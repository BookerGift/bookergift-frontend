import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#fafafa',
    },
    viewheadersms:{
        //borderWidth:2,
        height:hp('28%'),
        flexDirection:'column',
    },
    gobackview:{
        //borderWidth:3, 
        height:hp('11%'),
        width:wp('20%'), 
        justifyContent:'center', 
        alignItems:'center'
    },
    viewtxtconfirmsms:{
        //borderWidth:2,
        width:wp('90%'),
        alignSelf:'center',
        height:hp('15%'),
        flexDirection:'row',
    },
    confirmcodtxt:{
        //borderWidth:3,
        justifyContent:'center'
    },
    termos:{
        width:wp('90%'),
        //borderWidth:3,
        alignSelf:'center'
    },
    viewaceitetermos:{
        width:wp('90%'),
        //borderWidth:3,
        height:hp('20%'),
        alignSelf:'center',
        marginBottom:hp('3%'),
        marginTop:hp('2%')
    },
    viewaceitetermos2:{
        flexDirection:'row',
        //borderWidth:2,
    },
    checkboxtouchable:{
        width:'100%',
        height:'60%',
        borderWidth:4,
        //backgroundColor:'white',
        borderRadius:8,
        borderColor:'gray',
        justifyContent:'center',
        alignItems:'center'
    },
    checkboxtouchable2:{
        width:'100%',
        height:'60%',
        borderWidth:4,
        //backgroundColor:'white',
        borderRadius:8,
        borderColor:'#4eb03f',
        justifyContent:'center',
        alignItems:'center'
    },
    viewgo:{
        //borderWidth:5,
        marginTop:hp('1%'),
        flexDirection:'row-reverse',
        paddingLeft:20,
        height:'50%'
      },
});

export default styles;

