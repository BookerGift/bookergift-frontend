import React, { useState } from 'react';
import {Alert, View, Text, KeyboardAvoidingView, TouchableOpacity, ScrollView} from 'react-native';

//importando estilização da tela de TermsServices
import styles from './styles';

//importando componente GoBack
import GoBack from '../../Components/GoBack';
import TermosServicos from '../../Components/TermosServicos';
import GoNextTermos from '../../Components/GoNextTermos';

//Importação da biblioteca de icons do react-native-vector-icons
import Icon from 'react-native-vector-icons/Entypo';

const alerta = () => {
  Alert.alert('');
};

export default function TermsServices({route, navigation}) {
  
  // Pegando os parâmetros do componente GoNextConfirmEmail
  // Parâmetros esses: phone, nome, email e senha
  const {phone} = route.params;
  const {nome} = route.params;
  const {email} = route.params;
  const {senha} = route.params;

  //Criando constante de controle para o Checkbox de Aceite de
  //termos e condições
  const [checkHability, setCheckHability] = useState(false);
  const [aceiteTermos, setAceiteTermos] = useState(true);

  const habilitaCheck = () => {
    let controller = checkHability;

    if (checkHability === true) { 
      setCheckHability(false);
      return console.log(checkHability);
    } else {
      (checkHability === false)
      setCheckHability(true);
      return console.log(checkHability);
    }
  }

  const renderControleCheck = () => {
    let controllerCheck = checkHability;

    if ( checkHability === true ) {
      return <View style={styles.checkboxtouchable2}><Icon name="check" size={25} color={'#4eb03f'}/></View>;
    } else {   
      return <View style={styles.checkboxtouchable}/>;

    }
  }

  //Esta função faz com o que tenha o controle do botão
  //de avançar para a próxima tela, logo, o mesmo só estará
  //habilitado quando o telefone só tiver 11 caracteres
  const controleGoNextTermos = () => {
    
    
    if (checkHability === true) {

      return (
        <GoNextTermos
          telefone={phone}
          nome={nome}
          email={email}
          senha={senha}
          controle={false}
          aceitetermos={true}
          colorcontrole="#2cafd8"
        />
      );
    } else {
      
      return (
        <GoNextTermos
          telefone={phone}
          nome={nome}
          senha={senha}
          email={email}
          controle={true}
          aceitetermos={false}
          colorcontrole="gray"
        />
      );
    }
  };

  return (
    <KeyboardAvoidingView style={styles.container}>
      <View style={styles.viewheadersms}>

        {/* View do componente GoBack*/}
        <View style={styles.gobackview}>
          <GoBack irParaTela="Cadastro2" colorback="#545454" />
        </View>

        {/* View das labels de Termos e serviço*/}
        <View style={styles.viewtxtconfirmsms}>
          <View style={styles.confirmcodtxt}>
            <Text style={{color: 'black', fontSize: 22}}>
              Termos e Serviços.
            </Text>
            <Text style={{color: 'black', fontSize: 16, marginTop:10}}>
                Para concluirmos o cadastro leia e aceite nossos termos.
            </Text>
          </View>
        </View>
      </View>
      
      {/* ScroolView dos termos e serviçoes em si*/}
      <ScrollView style={styles.termos} persistentScrollbar={true}>
        <Text style={{fontSize:14}}>
            <TermosServicos></TermosServicos>
        </Text>
      </ScrollView>

      {/* View se checkbox para aceitar os termos*/}
      <View style={styles.viewaceitetermos}>
        <View style={styles.viewaceitetermos2}>
          <TouchableOpacity style={{width:'10%',height:'80%'}} onPress={() => habilitaCheck()}>
            {renderControleCheck()}
          </TouchableOpacity>
          <Text style={{width:'90%',paddingLeft:10, color:'black'}}> Declaro que li e concordo com os termos e condições do aplicativo. </Text>  
        </View>
        <View style={styles.viewgo}>
          {controleGoNextTermos()}
        </View> 
      </View>
    
    </KeyboardAvoidingView>
  );
}
