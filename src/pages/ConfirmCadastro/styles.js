import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        flex:1,
    },
    image: {
        flex: 2,
        resizeMode:"contain",
        backgroundColor:'#fff'
      },
      viewncelular:{
        flex:1,
        backgroundColor:'#fff',
        //borderWidth:4,
        alignItems:'center',
        justifyContent:'center'
      },
      text: {
        color: "grey",
        fontSize: 30,
        fontWeight: "bold"
      },
      viewimgprincipal:{
        //borderWidth: 5,
        alignSelf:'center',
        marginTop:hp('18%'),
        width:wp('40%'),
        height:hp('20%'),
        marginLeft:wp('9%'),
        //borderWidth:5
      },
      viewinformative1:{
        //borderWidth: 5,
        alignSelf:'center',
        width:wp('50%'),
        height:hp('7%'),
        marginLeft:wp('10%'),
        marginBottom:hp('4%')
      },
      viewinformative2:{
        //borderWidth: 5,
        alignSelf:'center',
        width:wp('85%'),
        height:hp('7%'),
        marginTop:hp('10%'),
        alignItems:'center',
        justifyContent:'center',
        //borderWidth:5
      },
      imgprincipal:{
        resizeMode:'contain',
        width:'100%',
        height:'100%',
      },
      imgsubprincipal:{
        resizeMode:'contain',
        width:'100%',
        height:'100%',
        alignSelf:'center'
      },
      textdescricao:{
          color:'white',
          fontSize: 18,
      },

      viewinferior:{
        flex:1,
        flexDirection:'column-reverse',
        //borderWidth:5,
      },
      containerinputs:{
        flex:1,
        width:wp('63%'),
        //borderWidth:5,
        alignSelf:'center',
        marginLeft:wp('23%')
      },
      txtlogin:{
        fontSize:22,
        fontWeight:'bold',
        color:'#FFDA87',
      },
      inputemail:{
        backgroundColor:'#55bede',
        //opacity:0.2,
        marginTop:hp('4%'),
        color:'#fff',
        height:hp('7%'),
        borderRadius:3,
        paddingLeft:15
      },
      inputsenha1:{
        justifyContent:'center',
        flexDirection:'row',
        height:hp('7%'),
        marginTop:hp('4%'),
        //borderWidth:5,
      },
      inputsenha2:{
        width:wp('50%'),
        backgroundColor:'#55bede',
        paddingLeft:15,
        height:hp('7%'),
        color:'#fff',
        //opacity:0.2,
        //marginTop:hp('4%'),
        //color:'#fff',
        //height:hp('7%'),
        //paddingLeft:15,
        //borderWidth:5,
      },
      iconeye:{
        width:wp('12%'),
        backgroundColor:'#55bede',
        borderTopRightRadius:3,
        borderBottomRightRadius:3,
        height:hp('7%'),
        //borderWidth:5,
        justifyContent:'center',
        alignItems:'center'
      },
      txtesquecesenha:{
        marginTop:hp('4%'),
        paddingLeft:2,
      },
      buttonentrar:{
        width:'100%',
        height:hp('7%'),
        //borderWidth:5,
        marginTop:hp('4%'),
        backgroundColor:'#14858E',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:3,
      },
      viewpais:{
        //borderWidth:5,
        height:'40%',
        borderBottomWidth:1,
        flexDirection:'row',
        
      },
      viewimgpais:{
        width:'15%',
        height:'100%',
      },
      imgpais:{
        resizeMode:'contain',
        width:'80%',
        height:'100%',
        alignSelf:'center'
      },
      viewbraziltxt:{
       // borderWidth:5, 
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        width:'15%',
      },
      viewinputcel:{
        //borderWidth:5,
        width:'70%',
        justifyContent:'center' 
      },
      viewgo:{
        //borderWidth:5,
        marginTop:hp('4%'),
      },

      viewsucess1:{
        width:wp('85%'),
        height:hp('10%'),
        //borderWidth:3,
        alignItems:'center'
      },

      viewsucess2:{
        width:wp('85%'),
        height:hp('10%'),
        //borderWidth:3,
        justifyContent:'center'
      },
      viewsucess3:{
        width:wp('85%'),
        height:hp('8%'),
        //borderWidth:3,
        justifyContent:'center',
        alignItems:'center'
      },
      buttonconfirm:{
        backgroundColor:'#349eeb',
        width:wp('83%'),
        height:hp('7%'),
        borderRadius:6,
        alignItems:'center',
        justifyContent:'center'
      }
});

export default styles;
