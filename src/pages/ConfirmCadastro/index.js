import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
} from 'react-native';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/FontAwesome5';

//importando componentes de GoBack e GoNext
import GoNext from '../../Components/GoNext';
import GoBack from '../../Components/GoBack';

//Importando a Estilização comum (Sem styled Components)
import styles from './styles';

import { useNavigation } from '@react-navigation/native';

//importando o asyncstorage
import AsyncStorage from '@react-native-async-storage/async-storage';

//importando o firebase
import firebase from '../../FirebaseConnection';


export default function ConfirmCadastro({ route }) {

  const navigation = useNavigation();

  const { email } = route.params;
  const { senha } = route.params;
  const { nome } = route.params;
  const { telefone } = route.params;

  const [keyUser, setMyKeyUser] = useState('');

  const finalizarCadastro = () => {
    navigation.navigate('Login');
  }

  function teste() {
    return console.log(`${email},${senha}`)
  }

  //Function para realizar login com as credenciais de login e senha
  //do usuário
  function login() {
    firebase.auth().signInWithEmailAndPassword(email, senha)
      .then((value) => {
        setMyKeyUser(value.user.uid);

        navigation.navigate('Home', {

          screen: 'Home',

        });
      })
      .catch((error) => {
        alert('Ops, este e-mail ou senha estão inválidos ou não estão na base de dados ! Tente novamente');
      })
  }


  const userStorage = async () => {
    if (keyUser != '') {

      let userobject = {
        nome: nome,
        email: email,
        telefone: telefone,
        senha: senha,
        chave: keyUser,
      };

      //Try catch para guardar objeto com dados do usuário logado 
      //no async storage
      try {

        await AsyncStorage.setItem('@objectuser', JSON.stringify(userobject));

      } catch (e) {

        alert(e);

      }
    }
  }
  useEffect(() => {
    userStorage();
    console.log(`EMAIL: ${email} E SENHA: ${senha}`);

  }, [keyUser]);

  return (
    <KeyboardAvoidingView style={styles.container}>

      {/*Primeira estrutura tela de cadastro */}
      <ImageBackground
        source={require('../../assets/confirm_cadastro.png')}
        style={styles.image}>

      </ImageBackground>

      <KeyboardAvoidingView style={styles.viewncelular}>
        <View style={styles.viewsucess1}>

          <Icon name="check-circle" size={65} color={'#4ddb65'} />
        </View>

        <View style={styles.viewsucess2}>
          <Text style={{ fontSize: 18, textAlign: 'center', color: '#4ddb65', marginBottom: 10, fontWeight: 'bold' }}>Cadastro realizado com sucesso !</Text>
          <Text style={{ fontSize: 14, textAlign: 'center' }}>Agora você pode Doar e Receber conhecimento !</Text>
        </View>

        <View style={styles.viewsucess3}>
          <TouchableOpacity style={styles.buttonconfirm} onPress={() => login()}>
            <Text style={{ fontWeight: 'bold', color: '#fff', fontSize: 16 }}>Finalizar</Text>
          </TouchableOpacity>
        </View>

      </KeyboardAvoidingView>
    </KeyboardAvoidingView>
  );
}
