import React, { useState } from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
} from 'react-native';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Importando biblioteca de mask para telefone ou máscaras no geral
import { TextInputMask } from 'react-native-masked-text';

//importando componentes de GoBack e GoNext
import GoNext from '../../Components/GoNext';
import GoBack from '../../Components/GoBack';

//Importando a Estilização comum (Sem styled Components)
import styles from './styles';
import { useNavigation } from '@react-navigation/native';

//Importandocomponente GoNext
import GoNextConfirmForgotPassword from '../../Components/GoNextConfirmForgotPassword';

export default function ForgotPassword() {

  const navigation = useNavigation();

  const finalizarCadastro = () => {
    navigation.navigate('Login');
  }

  const controlegonextemail = (email) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(email) === false) {
      return <GoNextConfirmForgotPassword controle={true} colorcontrole="gray" emailrecovery={recoveryEmail} />
    } else {
      return <GoNextConfirmForgotPassword controle={false} colorcontrole="#2cafd8" emailrecovery={recoveryEmail} />
    }
  }

  //Constante de estado do email a ser recuperado
  const [recoveryEmail, setMyRecoveryEmail] = useState('');

  return (
    <KeyboardAvoidingView style={styles.container}>

      {/*Primeira estrutura tela de cadastro */}
      <ImageBackground
        source={require('../../assets/forgot_password.png')}
        style={styles.image}>

        <View style={styles.gobackview}>
          <GoBack irParaTela="Login" colorback="#fff" />
        </View>

        <View style={styles.viewforgot}>
          <Text style={{ fontSize: 28, fontWeight: 'bold', color: '#fff' }}>
            Recuperar Senha
                </Text>
        </View>

        <View style={styles.viewforgot2}>
          <Text style={{ fontSize: 16, color: '#fff', lineHeight: 22 }}>
            Esqueceu sua senha ? coloque seu e-mail que foi cadastrado e deixa o resto com a gente !
                </Text>
        </View>

      </ImageBackground>

      <KeyboardAvoidingView style={styles.viewncelular}>

        {/*Seleção País*/}
        <View style={styles.viewpais}>

          <View style={{ ...styles.viewtxtforgotinput }}>
            <Text> Digite seu e-mail para recuperar sua senha !</Text>
          </View>

          <View style={{ flexDirection: 'row' }}>
            {/*Bandeira do País*/}
            <View style={styles.viewimgpais}>
              <Icon name="email-search" size={40} color={'#349eeb'} />
            </View>


            <View style={styles.viewinputcel}>
              {/* Input de E-mail com variavel de estado myEmail*/}
              <TextInput
                style={styles.inputcel}
                placeholderTextColor="#545454"
                placeholder="Digite o seu e-mail"
                onChangeText={(text) => setMyRecoveryEmail(text)}
                value={recoveryEmail}
              />
            </View>

            <View style={styles.viewgonext}>
              {controlegonextemail(recoveryEmail)}
            </View>

          </View>


        </View>

      </KeyboardAvoidingView>
    </KeyboardAvoidingView>
  );
}
