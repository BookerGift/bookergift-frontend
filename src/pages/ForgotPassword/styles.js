import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        flex:1,
    },
    image: {
        flex: 2,
        resizeMode:"contain",
        backgroundColor:'#fff'
      },
      viewncelular:{
        flex:1,
        backgroundColor:'#fafafa',
        //borderWidth:4,
        alignItems:'center',
        justifyContent:'center'
      },
      viewpais:{
        width:'90%',
        //borderWidth:3,
        height:'48%',
        //flexDirection:'row'
        borderBottomWidth:1,
      },
      viewtxtforgotinput:{
       // borderWidth:3,
      },
      viewimgpais:{
        width:'15%',
        height:'100%',
        //borderWidth:3,
        justifyContent:'center',
        alignItems:'center',
        
      },
      imgpais:{
        resizeMode:'contain',
        width:'80%',
        height:'100%',
        alignSelf:'center'
      },
      viewinputcel:{
        //borderWidth:3,
        width:'65%',
        justifyContent:'center',
         
      },
      inputcel:{
        fontSize: 16, 
       
        //backgroundColor:'#e2e2e2',
        color:'#545454',
        padding:20,
      },
      viewforgot:{
        width:wp('60%'),
        height:hp('7%'),
        //borderWidth:3,
        left:wp('8%'),
        top:hp('4%'),
        justifyContent:'center'
      },
      gobackview:{
        //borderWidth:3, 
        height:hp('11%'),
        width:wp('20%'), 
        justifyContent:'center', 
        alignItems:'center'
    },
      viewforgot2:{
        width:wp('85%'),
        height:hp('10%'),
        //borderWidth:3,
        left:wp('8%'),
        top:hp('4%'),
        justifyContent:'center'
      },
      viewgonext:{
        width:'20%',
        //borderWidth:2,
        justifyContent:'center',
        alignItems:'center'
      }
});

export default styles;
