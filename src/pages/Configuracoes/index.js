import React, { useState, useEffect } from 'react';
import { View, Text, KeyboardAvoidingView, Image, TouchableOpacity, TextInput, Alert, ImageBackground, Modal } from 'react-native';

//Importação do AsyncStorage para utilização de dados guardados do usuário
import AsyncStorage from '@react-native-async-storage/async-storage';

import { sendGridEmail } from 'react-native-sendgrid'

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

//Importando a Estilização StyleSheet
import styles from './styles';

//importando o firebase
import firebase from '../../FirebaseConnection';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';


export default function Configuracoes() {

  const SENDGRIDAPIKEY = "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";


  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  const [objetousuario, setMyObjetoUsuario] = useState('');
  const [isVisible, setIsVisible] = useState(false);
  const [isVisible2, setIsVisible2] = useState(false);

  const getDataObject = async () => {
    try {
      const pegarobjeto = await AsyncStorage.getItem('@objectuser')

      setMyObjetoUsuario(JSON.parse(pegarobjeto));

      if (objetousuario === null) {
        chaveusuario = ' DEU MERDA LEKAO '
      }
      //console.log(objetousuario);
    } catch (e) {
      Alert.alert(e);
    }
  }

  const gotoPerfil = () => {
    navigation.navigate('Perfil', {
      screen: 'Perfil',
    });
  }

  const loggout = () => {
    setIsVisible2(!isVisible2);

    navigation.navigate('Login', {
      screen: 'Login'
    });
  }

  const sendEmail = () => {
    const FROMEMAIL = "tccbookergift@gmail.com";
    const TOMEMAIL = "nielsantosrj@gmail.com";
    const SUBJECT = "Parabéns Pela Doação !";

    const ContactDetails = "Contact Data: " + "BookerGift" + " Mail: " + "tccbookergift@gmail.com" + " Phone: " + "21964814668"

    const sendRequest = sendGridEmail(SENDGRIDAPIKEY, TOMEMAIL, FROMEMAIL, SUBJECT, ContactDetails)
    sendRequest.then((response) => {
      console.log("Success")
    }).catch((error) => {
      console.log(error)
    });
  }

  useEffect(() => {
    getDataObject();
  }, [objetousuario.chave]);

  return (
    <ImageBackground source={require('../../assets/fundo-configuracoes.png')} style={styles.imagem}>

      <View style={styles.topo}>
        <Text style={{ fontWeight: 'bold', fontSize: 26, color: '#55bede' }}>Configurações</Text>
      </View>
      <View style={styles.container}>

        {/* View do TXT "Configurações Principais " */}
        <View style={styles.boxprincipaltxt}>
          <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Configurações Principais</Text>
        </View>

        {/* View do Box das configurações principais */}
        <View style={styles.boxprincipal}>

          <TouchableOpacity style={styles.boxitemprincipal1} onPress={() => gotoPerfil()}>
            <View style={styles.viewintemiconprincipal}>
              <Icon name="person-sharp" size={30} color={'#55bede'} />
            </View>

            <View style={styles.txt_item_principal}>
              <Text style={{ fontSize: 15, color: '#696969' }}>Meu Perfil</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.boxitemprincipal2} onPress={() => setIsVisible(!isVisible)}>
            <View style={styles.viewintemiconprincipal}>
              <Icon name="ios-information-circle-sharp" size={35} color={'#ebc034'} />
            </View>

            <View style={styles.txt_item_principal}>
              <Text style={{ fontSize: 15, color: '#696969' }}>Sobre o aplicativo</Text>
            </View>
          </TouchableOpacity>

        </View>

        {/* View do TXT "Configurações Principais " */}
        <View style={styles.boxprincipaltxt}>
          <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Sobre a conta</Text>
        </View>

        {/* View do Box do Suporte */}
        <View style={styles.boxprincipal2}>

          <TouchableOpacity style={styles.boxitemprincipal3} onPress={() => setIsVisible2(!isVisible2)}>
            <View style={styles.viewintemiconprincipal}>
              <Icon name="md-enter-sharp" size={30} color={'#b9b9b9'} />
            </View>

            <View style={styles.txt_item_principal}>
              <Text style={{ fontSize: 15, color: '#696969' }}>Sair do aplicativo</Text>
            </View>
          </TouchableOpacity>

        </View>

      </View>

      <Modal animationType="slide" transparent={true} visible={isVisible}
        onRequestClose={() => {
          setIsVisible(!isVisible);
        }}
      >

        <View style={styles.centeredView2}>
          <View style={styles.modalView3}>


            {/* View sobre o aplicativo */}
            <View style={styles.infoappview}>
              <Icon name="ios-information-circle-sharp" size={30} color={'#ebc034'} />
              <Text style={{ paddingLeft: 5 }}>Sobre o Aplicativo</Text>
            </View>

            <View style={styles.infoappview2}>
              <Text style={{ paddingLeft: 5, fontWeight: 'bold', fontSize: 16 }}>Desenvolvimento da Aplicação</Text>
            </View>

            {/* View das fotos minha e da Thassia */}
            <View style={styles.infoimgview}>
              <View style={{ width: '50%', height: '100%' }}>
                <Image source={require('../../assets/thassia.png')} style={styles.imgprincipal} ></Image>
              </View>
              <View style={{ width: '50%', height: '100%' }}>
                <Image source={require('../../assets/daniel.png')} style={styles.imgprincipal} ></Image>
              </View>
            </View>

            <View style={styles.viewnomes}>
              <View style={{ width: '50%', justifyContent: 'center', alignItems: 'center' }}>
                <Text>Thassia Ymara</Text>
              </View>

              <View style={{ width: '50%', justifyContent: 'center', alignItems: 'center' }}>
                <Text>Daniel Henrique</Text>
              </View>
            </View>

            <View style={styles.viewlinha}></View>
            <View style={styles.infoapp}>
              <Text style={{ paddingLeft: 5, fontWeight: 'bold', fontSize: 16 }}>Sobre a Aplicação</Text>
            </View>

            <View style={styles.infoappbooker}>
              <Text style={{ textAlign: 'justify', lineHeight: 20, fontSize: 14 }}>
                O BookerGift, consiste em uma aplicação onde é possível cadastrar aqueles seus livros que você não usa mais e poder doa-los para lguem que precise !
              </Text>
              <Text></Text>
              <Text style={{ textAlign: 'justify', lineHeight: 20, fontSize: 14 }}>
                Com o Código ISBN é possível cadastrar de forma rápida e prática, o que facilita a você desapegar dos seus livros que não usa !
              </Text>
            </View>

            <TouchableOpacity style={styles.fecharmodal} onPress={() => setIsVisible(!isVisible)}>
              <Text style={{ fontWeight: 'bold', color: '#fff' }}>FECHAR</Text>
            </TouchableOpacity>

          </View>
        </View>

      </Modal>


      <Modal
        animationType="slide"
        transparent={true}
        visible={isVisible2}
        onRequestClose={() => {
          setIsVisible2(!isVisible2);
        }}
        style={{ width: '50%' }}
      >
        <View style={styles.centeredView2}>
          <View style={styles.modalView2}>


            <Text style={{ fontWeight: 'bold' }}>Deseja realmente deslogar da aplicação ?</Text>

            <View style={{ flexDirection: 'row', marginTop: '10%' }}>
              <TouchableOpacity style={styles.buttonremover2} onPress={() => setIsVisible2(!isVisible2)}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>Cancelar</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.buttonpublicar2}>
                <Text style={{ color: 'white', fontWeight: 'bold' }} onPress={() => loggout()}>Sair</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>

    </ImageBackground>
  );
}