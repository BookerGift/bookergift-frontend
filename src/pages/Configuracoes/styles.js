import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  imagem: {
    flex: 1,
    resizeMode: "cover",
  },
  container: {
    flex: 1,
    //borderWidth: 1
  },
  centeredView2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(52, 52, 52, 0.8)'
  },
  modalView2: {
    width: wp('70%'),
    height: hp('20%'),
    //margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    //borderWidth: 1,
    padding: 25,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  buttonremover2: {
    width: '50%',
    height: '100%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#f54842',
    justifyContent: 'center',
    alignItems: 'center',
    //right: wp('2%')
  },
  buttonpublicar2: {
    width: '50%',
    height: '100%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#2ed936',
    justifyContent: 'center',
    alignItems: 'center',
    left: wp('3%')
  },
  infoappview: {
    width: '100%',
    height: '10%',
    //borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  infoapp: {
    width: '100%',
    //height: '10%',
    //borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  infoappview2: {
    width: '100%',
    height: '10%',
    //borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    //marginTop: hp('1%')
  },
  infoappbooker: {
    width: '100%',
    height: '35%',
    //borderWidth: 1,
    //alignItems: 'center',
    //justifyContent: 'center',
    //flexDirection: 'row',
    marginTop: hp('1%'),
    //backgroundColor: '#55bede'
  },
  viewimgprincipal: {
    //borderWidth: 5,
    alignSelf: 'center',

    width: wp('10%'),
    //height: hp('10%'),
    //borderWidth:5
  },
  fecharmodal: {
    width: '100%',
    height: '10%',
    backgroundColor: '#d44c4c',
    //borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    //flexDirection: 'row',
    //marginTop: hp('1%'),
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
  },
  viewnomes: {
    width: '100%',
    //borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    //marginTop: hp('1%'),
    flexDirection: 'row'
  },
  imgprincipal: {
    resizeMode: 'contain',
    width: '100%',
    height: '100%',
  },
  infoimgview: {
    width: '100%',
    height: '20%',
    //borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    //marginTop: hp('1%'),
    flexDirection: 'row'
  },
  viewlinha: {
    width: '100%',
    height: '3%',
    borderBottomWidth: 1,
    borderColor: '#d3d3d3',
    marginBottom: 10
  },
  topo: {
    width: wp('100%'),
    height: hp('18%'),
    //backgroundColor: '#fff',
    //padding: wp('5%'),
    justifyContent: 'center',
    paddingLeft: wp('35%'),
  },
  viewphoto: {
    //borderWidth: 3,
    borderColor: 'gray',
    width: '15%',
    height: '100%',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  dadosperfil: {
    width: '75%',
    height: '100%',
    //borderWidth: 3,
    justifyContent: 'center',
    paddingLeft: wp('15%')
  },
  dadosnome: {
    //borderWidth:3,
  },
  boxprincipaltxt: {
    width: wp('85%'),
    height: hp('5%'),
    //borderWidth: 1,
    alignSelf: 'center',
    marginTop: hp('4%')
  },
  boxprincipal: {
    width: wp('85%'),
    height: hp('20%'),
    //borderWidth: 1,
    alignSelf: 'center',
    marginTop: hp('1%'),
    borderRadius: 10,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    elevation: 5,
    padding: 15,
  },
  boxprincipal2: {
    width: wp('85%'),
    height: hp('10%'),
    //borderWidth: 1,
    alignSelf: 'center',
    marginTop: hp('1%'),
    borderRadius: 10,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    elevation: 5,
    padding: 15,
  },
  boxitemprincipal1: {
    width: '100%',
    height: '50%',
    //borderWidth: 1,
    flexDirection: 'row'
  },
  boxitemprincipal3: {
    width: '100%',
    height: '100%',
    //borderWidth: 1,
    flexDirection: 'row'
  },
  boxitemprincipal2: {
    width: '100%',
    height: '50%',
    borderTopWidth: 0.5,
    flexDirection: 'row',
    borderColor: '#dedede'
  },
  viewintemiconprincipal: {
    width: '20%',
    height: '100%',
    //borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  txt_item_principal: {
    width: '80%',
    height: '100%',
    //borderWidth: 1,
    justifyContent: 'center',
    paddingLeft: 5
  },
  modalView3: {
    width: wp('90%'),
    height: hp('85%'),
    //margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    //borderWidth: 1,
    padding: 25,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
});

export default styles;
