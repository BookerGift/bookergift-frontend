import React from 'react'
import { View, Text, ImageBackground, Image } from 'react-native'

//Importando a Estilização comum (Sem styled Components)
import styles from './styles';

//Importando Componente da barra inferior
import Downbarinformative from '../../Components/Downbarinformative'

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/MaterialIcons';


export default function Informative2({ navigation }) {
    return (
        <View style={styles.container}>

            <ImageBackground source={require('../../assets/fundo2.jpg')} style={styles.image}>

                {/* View de descrição inicial da aplicação */}
                <View style={styles.viewinformative21}>

                    {/* View geral do Título da informative Screen 2 */}
                    <View style={styles.viewtxttitle}>

                        <View style={styles.viewicontitle}>
                            <Icon name="fiber-manual-record" size={15} color={'#6bff84'} />
                        </View>

                        <View style={styles.viewtitle}>
                            <Text style={styles.texttitulo}>Compartilhe Conhecimento</Text>
                        </View>

                    </View>

                    {/* View geral da descrição do Título da informative Screen 2 */}
                    <View style={styles.viewsubtitulo}>
                        <Text style={styles.textdescricao}>Cadastre e publique seus livros na sua estante virtual para compartilhar conhecimento com a comunidade Booker Gift!</Text>
                    </View>
                </View>

                {/* View Inferior principal de manuseio das informatives screens */}
                <View style={styles.viewinferior}>

                    {/*Componente da barra inferior da tela */}
                    <Downbarinformative c1='white' c2='yellow' c3='white' c4='white' screenName='Informative3' />

                </View>

            </ImageBackground>

        </View>
    );
}