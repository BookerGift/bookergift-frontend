import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
  Alert
} from 'react-native';

//importando o asyncstorage
import AsyncStorage from '@react-native-async-storage/async-storage';

//Importando a Estilização StyleSheet
import styles from './styles';

//Importação da biblioteca de icons do react-native-vector-icons
import Icon from 'react-native-vector-icons/Entypo';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

//importando o firebase
import firebase from '../../FirebaseConnection';
import { useScreens } from 'react-native-screens';

export default function Login() {

  //Constante do Hook de estado para o e-mail, de senha, 
  //e controller para mostrar e ocultar a senha
  const [myNome, setMyNome] = useState('');
  const [myTelefone, setMyTelefone] = useState('');
  const [myEmail, setMyEmail] = useState('');
  const [mySenha, setMySenha] = useState('');
  const [myControler, setMyControler] = useState(true);
  const [keyUser, setMyKeyUser] = useState('');

  const [emailInput, setEmailInput] = useState('');
  const [senhaInput, setSenhaInput] = useState('');
  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  const userobject = {
    nome: myNome,
    email: emailInput,
    telefone: myTelefone,
    senha: senhaInput,
    chave: keyUser,
  };

  //A função validate faz com que o e-mail que é digital no input
  //de email seja validade em tempo real, ou seja, conforme o usuário
  //digita, a função vai fazendo a validação até que o mesmo esteja
  //correto.
  const validate = (text) => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      setMyEmail(text)
      return false;
    }
    else {
      setMyEmail(text)
      console.log("Email is Correct");
    }
  }

  //A função mostrar senha, faz o controle do button de mostrar,
  //ou ocultar senha
  const mostrarSenha = (controler) => {
    if (controler == true) {
      setMyControler(false);
    } else {
      if (controler == false) {
        setMyControler(true);
      }
    }
  }


  //Função que guarda todos os dados do usuário no asyncstorage para assim
  //após de acontecer a navegação ser possível pegar os dados do usuário
  //para dar tratamento ao mesmo.
  const userStorage = async () => {

    //Try catch para guardar objeto com dados do usuário logado 
    //no async storage
    try {

      await AsyncStorage.setItem('@objectuser', JSON.stringify(userobject));

    } catch (e) {

      alert(e);

    }
  }

  const getNome = async () => {
    await firebase.database().ref('usuario/' + keyUser + '/nome').on('value', (snapshot) => {
      return setMyNome(snapshot.val());
    })
  }

  const getTelefone = async () => {
    await firebase.database().ref('usuario/' + keyUser + '/contato/telefone').once('value', (snapshot) => {
      return setMyTelefone(snapshot.val());
    })
  }

  //Function para realizar login com as credenciais de login e senha
  //do usuário
  const login = async () => {
    await firebase.auth().signInWithEmailAndPassword(myEmail, mySenha)
      .then((value) => {
        setMyEmail(value.user.email);
        setMyKeyUser(value.user.uid);
        setEmailInput(myEmail);
        setSenhaInput(mySenha);

        navigation.navigate('Home', {
          screen: 'Home',
        });

        setMyEmail('');
        setMySenha('');
      })
      .catch((error) => {
        alert('Ops, este e-mail ou senha estão inválidos ou não estão na base de dados ! Tente novamente');
      })
  }

  //Useffect é o hook que substitui o ciclo de vida do react, logo
  //é através dele que se inicia uma determinada ação antes de ou depois
  //de renderizar o componente completo da tela.
  useEffect(() => {

    getNome();
    getTelefone();

  }, [keyUser]);

  useEffect(() => {

    userStorage();

  }, [userobject]);

  return (
    <KeyboardAvoidingView style={styles.container}>

      <ImageBackground source={require('../../assets/fundo5.png')} style={styles.image}>

        {/* Imagem da logo principal */}
        <KeyboardAvoidingView style={styles.viewimgprincipal}>
          <Image source={require('../../assets/logobranco.png')} style={styles.imgprincipal}></Image>
        </KeyboardAvoidingView>

        {/* Imagem da sublogo(título) principal */}
        <KeyboardAvoidingView style={styles.viewinformative1}>
          <Image source={require('../../assets/logoescrita.png')} style={styles.imgsubprincipal}></Image>
        </KeyboardAvoidingView>

        {/* Área dos Inputs e Buttons */}
        <KeyboardAvoidingView style={styles.containerinputs}>
          {/*Text Login */}
          <Text style={styles.txtlogin}>Login</Text>

          {/* Input de E-mail com variavel de estado myEmail*/}
          <TextInput style={styles.inputcel}
            placeholderTextColor='#fff'
            placeholder="Seu e-mail"
            onChangeText={text => setMyEmail(text)}
            value={myEmail}
          />

          {/* Input de senha com variavel de estado myEmail*/}
          <View style={{ flexDirection: 'row' }}>

            {/*
              Input de Senha, possui securityentry para ocultar os
              caracteres digitados "criptografando" o que o usuário
              digita
            */}
            <View>
              <TextInput style={styles.inputsenha2}
                placeholderTextColor='#fff'
                placeholder="Sua senha"
                secureTextEntry={myControler}
                onChangeText={text => setMySenha(text)}
                value={mySenha}
              />
            </View>

            {/* 
              Botão de mostrar a senha onde ao acionar chama a
              função que muda o estado co controler mostrando a senha
            */}
            <TouchableOpacity style={styles.iconeye} onPress={() => mostrarSenha(myControler)}>
              <Icon name="eye" color="#fff" size={20} />
            </TouchableOpacity>

          </View>

          {/* Button de "Não tem acesso? Cadastre-se !*/}
          <TouchableOpacity style={styles.txtesquecesenha} onPress={() => navigation.navigate('Cadastro')}>
            <Text style={{ color: '#FFDA87' }}>
              Não tem Acesso?
                <Text style={{ fontWeight: 'bold', color: '#fff' }}>
                Cadastre-se !
                </Text>
            </Text>
          </TouchableOpacity>

          {/* Button Touchable de Entrar */}
          <TouchableOpacity style={styles.buttonentrar}
            onPress={() => login()}
          >
            <Text style={{ fontWeight: 'bold', color: '#FFDA87' }}>ENTRAR</Text>
          </TouchableOpacity>

          {/* Button de "Esqueceu sua senha?*/}
          <TouchableOpacity style={styles.txtesquecesenha} onPress={() => navigation.navigate('ForgotPassword')}>
            <Text style={{ color: '#FFDA87' }}>
              Esqueceu sua senha ?
                <Text style={{ fontWeight: 'bold', color: '#fff' }}>
                Recupere !
                </Text>
            </Text>
          </TouchableOpacity>

        </KeyboardAvoidingView>

      </ImageBackground>

    </KeyboardAvoidingView>
  );
}
