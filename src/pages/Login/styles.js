import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        flex:1,
    },
    image: {
        flex: 1,
        resizeMode: "cover",
      },
      text: {
        color: "grey",
        fontSize: 30,
        fontWeight: "bold"
      },
      viewimgprincipal:{
        //borderWidth: 5,
        alignSelf:'center',
        marginTop:hp('8%'),
        width:wp('35%'),
        height:hp('15%'),
        marginLeft:wp('20%')
      },
      viewinformative1:{
        //borderWidth: 5,
        alignSelf:'center',
        width:wp('50%'),
        height:hp('7%'),
        marginLeft:wp('20%'),
        marginBottom:hp('4%')
      },
      viewinformative2:{
        //borderWidth: 5,
        alignSelf:'center',
        width:wp('85%'),
        height:hp('7%'),
        marginTop:hp('10%'),
        alignItems:'center',
        justifyContent:'center',
        //borderWidth:5
      },
      imgprincipal:{
        resizeMode:'contain',
        width:'100%',
        height:'100%',
      },
      imgsubprincipal:{
        resizeMode:'contain',
        width:'80%',
        height:'100%',
        alignSelf:'center'
      },
      textdescricao:{
          color:'white',
          fontSize: 18,
      },

      viewinferior:{
        flex:1,
        flexDirection:'column-reverse',
        //borderWidth:5,
      },
      containerinputs:{
        flex:1,
        width:wp('63%'),
        //borderWidth:5,
        alignSelf:'center',
        marginLeft:wp('23%')
      },
      txtlogin:{
        fontSize:22,
        fontWeight:'bold',
        color:'#FFDA87',
      },
      inputemail:{
        backgroundColor:'#55bede',
        //opacity:0.2,
        marginTop:hp('4%'),
        color:'#fff',
        height:hp('7%'),
        borderRadius:3,
        paddingLeft:15
      },
      inputsenha1:{
        justifyContent:'center',
        flexDirection:'row',
        height:hp('7%'),
        marginTop:hp('4%'),
        borderWidth:5,
        width:wp('14%'),
        backgroundColor:'#55bede',

      },
      inputsenha2:{
        width:wp('50%'),
        backgroundColor:'#55bede',
        paddingLeft:15,
        height:hp('7%'),
        color:'#fff',
        //opacity:0.2,
        marginTop:hp('4%'),
        //color:'#fff',
        //height:hp('7%'),
        //paddingLeft:15,
        //borderWidth:5,
      },
      iconeye:{
        width:wp('13%'),
        backgroundColor:'#55bede',
        borderTopRightRadius:3,
        borderBottomRightRadius:3,
        height:hp('7%'),
        //borderWidth:5,
        justifyContent:'center',
        alignItems:'center',
        marginTop:hp('4%'),
      },
      txtesquecesenha:{
        marginTop:hp('4%'),
        paddingLeft:2,
      },
      buttonentrar:{
        width:'100%',
        height:hp('7%'),
        //borderWidth:5,
        marginTop:hp('2%'),
        backgroundColor:'#14858E',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:3,
      },
      viewpais:{
        //borderWidth:5,
        height:'10%',
        marginTop:hp('4%'),
        flexDirection:'row'
      },
      viewimgpais:{
        width:'20%',
        height:'100%',
      },
      imgpais:{
        resizeMode:'contain',
        width:'80%',
        height:'100%',
        alignSelf:'center'
      },
      viewbraziltxt:{
        //borderWidth:5, 
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
      },
      inputcel:{
        fontSize: 16, 
        height:hp('7%'), 
        backgroundColor:'#54bddb',
        marginTop:'7%',
        paddingLeft:20,
        color:'#fff'
      },
      inputsenha:{
        fontSize: 16, 
        height:hp('7%'), 
        backgroundColor:'#54bddb',
        marginTop:'7%',
        paddingLeft:20,
        color:'#fff'
      }
});

export default styles;
