import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#f0f0f0',
        flex: 1,
        //borderWidth: 1
    },
    topo: {
        width: wp('100%'),
        height: hp('25%'),
        backgroundColor: '#f0f0f0',
        padding: wp('5%'),
        //borderWidth: 1
    },
    viewphoto: {
        //borderWidth:3,
        borderColor: 'gray',
        width: '17%',
        height: '100%',
    },
    viewphoto2: {
        //borderWidth: 1,
        borderColor: 'gray',

        justifyContent: 'center',
    },
    imagecircle: {
        width: 45,
        height: 45,
        borderRadius: 45 / 2,
        backgroundColor: '#55bede',

    },
    imagecircle2: {
        width: 35,
        height: 35,
        borderRadius: 35 / 2,
        backgroundColor: '#55bede',
        alignSelf: 'center'

    },
    imagecircle3: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        backgroundColor: '#55bede',
        alignSelf: 'center'

    },
    dadosperfil: {
        width: '75%',
        height: '100%',
        //borderWidth:1,

    },
    dadosnome: {
        marginBottom: 5
    },
    viewopcoesperfil: {
        flex: 2,
        //borderWidth:3,
    },
    infopessoais: {
        width: '95%',
        height: '30%',
        //borderWidth:2,
        borderBottomWidth: 0.2,
        borderColor: 'gray',
        alignSelf: 'center',
        justifyContent: 'center',
        padding: 20
    },
    cardpublicacao: {
        width: wp('90%'),
        height: hp('26%'),
        borderRadius: 10,
        //marginRight: wp('12%'),
        marginTop: hp('4%'),
        marginBottom: hp('3%'),
        //borderWidth: 1,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        alignSelf: 'center'
    },
    scrollpublicacoes: {

        width: wp('100%'),
        //height: hp('100%'),
        borderWidth: 1,
        alignSelf: 'center',
        //paddingTop: 0
        //justifyContent: 'center'

    },
    viewimgpublicacao: {
        width: '100%',
        height: '50%',
        borderColor: '#c3c3c3',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        justifyContent: 'center',
        //borderWidth: 2
    },
    imgpublicacao: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
        borderRadius: 1,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        position: 'absolute'
    },
    viewinfopublicacao: {
        width: '100%',
        height: '50%',
        //borderWidth: 1,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    viewtitulos: {
        width: '100%',
        height: '35%',
        //borderWidth: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    boxtitulo: {
        width: '50%',
        height: '100%',
        //borderWidth: 1,
        justifyContent: 'center',
    },
    viewinfospubli: {
        width: '100%',
        height: '60%',
        //borderWidth: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    boxtpublicador: {
        width: '50%',
        height: '100%',
        //borderWidth: 1,
        flexDirection: 'row',
    },
    boxlivro: {
        width: '50%',
        height: '100%',
        //borderWidth: 1
    },
    boxdata: {
        width: '33%',
        height: '100%',
        //borderWidth: 1
    },
    txtboxtitulo: {
        fontWeight: 'bold',
        //textAlign: 'center',
        fontSize: 15,
        paddingLeft: 15,
        color: '#55bede'
    },
    foto: {
        width: '35%',
        height: '100%',
        //borderWidth: 1,
        //justifyContent: 'center'
    },
    nomefoto: {
        width: '60%',
        //height: '100%',
        //borderWidth: 1
    },
    viewmypublicacao: {
        position: 'absolute',
        width: '50%',
        height: '40%',
        borderRadius: 10,
        backgroundColor: '#55bede',
        alignSelf: 'flex-end',
        //marginTop: 10,

        //justifyContent: 'center',
        flexDirection: 'row',
        marginTop: -13
    },
    viewmypublicacao2: {
        position: 'absolute',
        width: '55%',
        height: '30%',
        borderRadius: 10,
        backgroundColor: '#55bede',
        alignSelf: 'flex-end',
        //marginTop: 10,

        //justifyContent: 'center',
        flexDirection: 'row',
        marginTop: -13
    },
    viewmypublicacaoexit: {
        position: 'absolute',
        width: '15%',
        height: '30%',
        borderRadius: 10,
        backgroundColor: '#d44c4c',
        alignSelf: 'flex-start',
        //marginTop: 10,

        justifyContent: 'center',
        //flexDirection: 'row',
        marginTop: -13
    },
    centeredView2: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(52, 52, 52, 0.8)'
    },
    modalView2: {
        width: wp('90%'),
        height: hp('85%'),
        //margin: 20,
        backgroundColor: "white",
        borderRadius: 10,
        //borderWidth: 1,

        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    modalView3: {
        width: wp('80%'),
        height: hp('20%'),
        //margin: 20,
        backgroundColor: "white",
        borderRadius: 10,
        borderWidth: 1,

        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    modalView4: {
        width: wp('70%'),
        height: hp('20%'),
        //margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        //borderWidth: 1,
        padding: 25,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    buttonremover2: {
        width: '45%',
        height: '75%',
        //borderWidth: 1,
        borderRadius: 10,
        backgroundColor: '#f54842',
        justifyContent: 'center',
        alignItems: 'center',
        //right: wp('2%')
    },
    buttonpublicar2: {
        width: '50%',
        height: '75%',
        //borderWidth: 1,
        borderRadius: 10,
        backgroundColor: '#2ed936',
        justifyContent: 'center',
        alignItems: 'center',
        left: wp('3%')
    },
    buttonremover3: {
        width: '50%',
        height: '100%',
        //borderWidth: 1,
        borderRadius: 10,
        backgroundColor: '#f54842',
        justifyContent: 'center',
        alignItems: 'center',
        //right: wp('2%')
    },
    buttonpublicar3: {
        width: '50%',
        height: '100%',
        //borderWidth: 1,
        borderRadius: 10,
        backgroundColor: '#2ed936',
        justifyContent: 'center',
        alignItems: 'center',
        left: wp('3%')
    },
    imgmodalpublicacao: {
        width: '100%',
        height: '25%',
        //borderWidth: 1,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    fonttitle: {

        fontWeight: 'bold',
        fontSize: 16,

    },
    viewtitlepublicacao: {
        width: '100%',
        height: '8%',
        //borderWidth: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 5,
        marginTop: 5
        //paddingLeft: 20,
    },
    comment: {
        width: '90%',
        alignSelf: 'center',
        height: '100%',
        //borderWidth: 1,
        borderLeftWidth: 2,
        borderColor: '#ebebeb',
        paddingLeft: 10,
        justifyContent: 'center',
    },
    comment2: {
        width: '90%',
        alignSelf: 'center',
        height: '100%',
        //borderWidth: 1,
        borderLeftWidth: 5,
        borderColor: '#ebebeb',
        paddingLeft: 10,
        justifyContent: 'center',
    },
    commentstyle: {
        fontSize: 18,
        fontWeight: 'bold',
        fontStyle: 'italic',
        color: '#a3a3a3'
    },
    view_foto_publi: {
        width: '20%',
        height: '100%',
        //borderWidth: 1,
        justifyContent: 'center',
        //paddingLeft: 10
    },
    infofoto: {
        width: '40%',
        height: '100%',
        //borderWidth: 1,
        justifyContent: 'center',
        //alignItems: 'center'
    },
    publicador: {
        width: '30%',
        height: '100%',
        //borderWidth: 1,
        justifyContent: 'center',
        paddingLeft: 5
    },
    viewadocao: {
        width: '35%',
        height: '80%',
        //borderWidth: 1,
        borderRadius: 15,
        //alignItems: 'center',
        //paddingTop: 5,
        backgroundColor: '#f5f5f5',
        flexDirection: 'row',
        //justifyContent: 'center'
    },
    viewaceitarpublicacao: {
        width: '45%',
        height: '80%',
        //borderWidth: 1,
        borderRadius: 15,
        alignItems: 'center',
        //paddingTop: 5,
        backgroundColor: '#55bede',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(52, 52, 52, 0.8)'
    },
    modalView: {
        width: wp('80%'),
        height: hp('70%'),
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 30,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 10,
        //padding: 10,
        elevation: 2,
        width: '60%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
});

export default styles;
