import React, { useState, useEffect } from 'react';
import { View, Text, KeyboardAvoidingView, Image, TouchableOpacity, SnapshotViewIOS, Modal, Alert } from 'react-native';
//importando o firebase
import firebase from '../../FirebaseConnection';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

//Importando a Estilização StyleSheet
import styles from './styles';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { ScrollView } from 'react-native-gesture-handler';

import { sendGridEmail } from 'react-native-sendgrid'


export default function Home({ route }) {

    //Atribuindo a navegação para a constante navigantion, assim
    //a mesma tem acesso à nevegação entre as telas da aplicação
    const navigation = useNavigation();

    //const { keyusurario } = route.params;
    const [keyUser, setMyKeyUser] = useState('');
    const [nomeUsuario, setMyNomeUsuario] = useState('Carregando...');
    //const [myTelefone, setMyTelefone] = useState('');
    const [objetousuario, setMyObjetoUsuario] = useState('');
    const [publicacoes, setPublicacoes] = useState([]);
    const [arrayListPublicacao, setArrayListPublicacao] = useState('');
    const [isVisible, setIsVisible] = useState(false);
    const [isVisible2, setIsVisible2] = useState(false);
    const [isVisible3, setIsVisible3] = useState(false);
    const [saldoAdocao, setSaldoAdocao] = useState(0);
    const [notificacaoEmail, setNotificacaoEmail] = useState(true);
    const [saldoAdocaoDoador, setSaldoAdocaoDoador] = useState(0);
    const [qtdLivros, setQtdLivros] = useState(0);
    const [qtdPublicacoes, setQtdPublicacoes] = useState(0);
    const [isVisible4, setIsVisible4] = useState(false);

    const SENDGRIDAPIKEY = "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";

    const data = new Date();

    const dataAtual = {
        dia: data.getDate(),
        mes: data.getMonth(),
        ano: data.getFullYear(),
    }
    const mostrarData = `${dataAtual.dia}/${dataAtual.mes}/${dataAtual.ano}`;


    const pegarQtdLivrosDoador = async () => {
        //PEGANDO A QUANTIDADE DE LIVROS DO PUBLICADOR / DOADOR
        await firebase.database().ref(`usuario/${arrayListPublicacao.iduser}/livros_cadastrados`).once('value', (snapshot) => {
            setQtdLivros(snapshot.val());
        });
    }

    const pegarQtdPublcacoesDoador = async () => {
        //PEGANDO A QUANTIDADE DE PUBLICAÇÕES DO PUBLICADOR / DOADOR
        await firebase.database().ref(`usuario/${arrayListPublicacao.iduser}/publicacoes`).once('value', (snapshot) => {
            setQtdPublicacoes(snapshot.val());
        });
    }

    const pegarSaldoAdocaoDoador = async () => {
        //PEGANDO O SALDO ADOÇÃO DO USUÁRIO QUE PUBLICOU / DOADOR
        await firebase.database().ref(`usuario/${arrayListPublicacao.iduser}/saldo_adocao`).once('value', (snapshot) => {
            setSaldoAdocaoDoador(snapshot.val());
        });
    }

    const sairAplicativo = () => {
        return (
            <Deslogar />
        );

    };


    const getDataObject = async () => {
        try {
            const pegarobjeto = await AsyncStorage.getItem('@objectuser')

            setMyObjetoUsuario(JSON.parse(pegarobjeto));

        } catch (e) {
            Alert.alert(e);
        }
    }

    const getNomeUsuario = async () => {
        await firebase.database().ref(`usuario/${objetousuario.chave}/nome`).on('value', (snapshot) => {
            setMyNomeUsuario(snapshot.val());
        });
    }

    const getDataPublicacao = async () => {
        await firebase.database().ref('publicacao').on('value', (snapshot) => {

            setPublicacoes([]);

            snapshot.forEach((chilItem) => {

                const publicacaodata = {
                    key: chilItem.key,
                    titulo: chilItem.val().titulo,
                    descricao: chilItem.val().descricao,
                    iduser: chilItem.val().usuario.iduser,
                    nomeusuario: chilItem.val().usuario.nome,
                    telefoneusuario: chilItem.val().usuario.telefone,
                    emailusuario: chilItem.val().usuario.email,
                    idlivro: chilItem.val().livro.idlivro,
                    imagem: chilItem.val().livro.imagem,
                    nomelivro: chilItem.val().livro.titulo,
                    editoralivro: chilItem.val().livro.editora,
                    datapublicacao: chilItem.val().datapublicacao,
                    status: chilItem.val().status,
                };


                setPublicacoes(oldArray => [...oldArray, publicacaodata]);


            });
        })
    }

    const minhaPublicacao = (chavepublicador) => {

        if (objetousuario.chave == chavepublicador) {
            return (
                <View style={{ width: '95%', alignSelf: 'center', height: '100%' }}>
                    <View style={styles.viewmypublicacao}>
                        <View style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                            <Icon style={{ alignSelf: 'center' }} name="star" color="#f5e473" size={18} />
                        </View>
                        <View style={{ height: '100%', justifyContent: 'center' }}>
                            <Text style={{ fontWeight: 'bold', color: '#fff' }}>Minha Publicação !</Text>
                        </View>
                    </View>
                </View>
            );
        }
    }

    const minhaPublicacao2 = (chavepublicador) => {

        if (objetousuario.chave == chavepublicador) {
            return (
                <View style={{ width: '90%', alignSelf: 'center', height: '100%' }}>
                    <TouchableOpacity style={styles.viewmypublicacaoexit} onPress={() => setIsVisible(!isVisible)}>
                        <Icon style={{ alignSelf: 'center' }} name="ios-close" color="#fff" size={25} />
                    </TouchableOpacity>

                    <View style={styles.viewmypublicacao2}>
                        <View style={{ width: '25%', height: '100%', justifyContent: 'center' }}>
                            <Icon style={{ alignSelf: 'center' }} name="star" color="#f5e473" size={18} />
                        </View>
                        <View style={{ height: '100%', justifyContent: 'center' }}>
                            <Text style={{ fontWeight: 'bold', color: '#fff' }}>Minha Publicação !</Text>
                        </View>
                    </View>
                </View>
            );
        } else {
            return (
                <View style={{ width: '90%', alignSelf: 'center', height: '100%' }}>
                    <TouchableOpacity style={styles.viewmypublicacaoexit} onPress={() => setIsVisible(!isVisible)}>
                        <Icon style={{ alignSelf: 'center' }} name="ios-close" color="#fff" size={25} />
                    </TouchableOpacity>
                </View>
            );

        }
    }

    const buscarPublicacao = async (chavepublicacao) => {

        await firebase.database().ref(`publicacao/${chavepublicacao}`).once('value', (snapshot) => {

            setArrayListPublicacao({
                key: snapshot.key,
                titulo: snapshot.val().titulo,
                descricao: snapshot.val().descricao,
                datapublicacao: snapshot.val().datapublicacao,
                status: snapshot.val().status,
                iduser: snapshot.val().usuario.iduser,
                nomeusuario: snapshot.val().usuario.nome,
                emailusuario: snapshot.val().usuario.email,
                telefoneusuario: snapshot.val().usuario.telefone,
                idlivro: snapshot.val().livro.idlivro,
                titulolivro: snapshot.val().livro.titulo,
                editoralivro: snapshot.val().livro.editora,
                imagem: snapshot.val().livro.imagem,
                isbn: snapshot.val().livro.isbn,
                autor: snapshot.val().livro.autor
            });

        });

        //PEGANDO O SALDO ADOÇÃO DO USUÁRIO QUE ESTÁ LOGADO (RECEPTOR PERANTE A PUBLICAÇÃO)
        await firebase.database().ref(`usuario/${objetousuario.chave}/saldo_adocao`).once('value', (snapshot) => {
            setSaldoAdocao(snapshot.val());
        });


        setIsVisible(!isVisible);
    }

    const publicacaoStorage = async () => {

        //Try catch para guardar objeto com dados do livro
        //no async storage
        try {

            await AsyncStorage.setItem('@objectpublicacao', JSON.stringify(arrayListPublicacao));

        } catch (e) {

            alert(e);

        }

    }

    const confirmPublicacao = () => {
        setIsVisible3(!isVisible3);
    }

    const renderMinhaPublicacao = (chavepublicador) => {
        if (objetousuario.chave == chavepublicador) {
            return (
                <View style={{ width: '90%', height: '100%', alignSelf: 'center' }}>
                    <View><Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold' }}>Esta Publicação é sua !</Text></View>
                    <TouchableOpacity
                        style={{
                            marginTop: 10,
                            width: '100%',
                            alignSelf: 'center',
                            height: '45%',
                            justifyContent: 'center',
                            borderRadius: 5,
                            backgroundColor: '#55bede'
                        }}
                        onPress={() => gotoEditarPublicacao()}>
                        <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold', color: '#fff' }}>Ver Publicação !</Text>
                    </TouchableOpacity>
                </View>
            );
        } else {
            return (
                <View style={{ width: '90%', height: '100%', alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between', }}>
                    <View style={styles.viewadocao}>
                        <View style={{ width: '100%', height: '80%', marginTop: 5, }}>
                            <View style={{ width: '100%', height: '50%', justifyContent: 'center' }}>
                                <Text style={{ textAlign: 'center' }}>Saldo Adoção:</Text>
                            </View>

                            <View style={{ width: '100%', height: '50%', justifyContent: 'center' }}>
                                <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold' }}>{saldoAdocao}</Text>
                            </View>
                        </View>
                        <View style={{ width: '100%', height: '100%', justifyContent: 'flex-end', position: 'absolute' }}>
                            <TouchableOpacity style={{ position: 'absolute', alignSelf: 'flex-end', right: -20 }} onPress={() => alertSaldoAdocao()}>
                                <Icon name="ios-information-circle" color="#e0b94c" size={40} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <TouchableOpacity style={styles.viewaceitarpublicacao} onPress={() => confirmPublicacao()}>
                        <View>
                            <Text style={{ fontWeight: 'bold', color: '#fff', textAlign: 'center' }}>ACEITAR </Text>
                            <Text style={{ fontWeight: 'bold', color: '#fff' }}>PUBLICAÇÃO </Text>

                        </View>

                    </TouchableOpacity>
                </View>
            );
        }
    }

    const alertSaldoAdocao = () => {
        setIsVisible2(!isVisible2);
    }

    const gotoEditarPublicacao = async () => {
        navigation.navigate('EditarPublicacao', {
            screen: 'EditarPublicacao'
        })

        setIsVisible(!isVisible);
    }

    const criarDoacao = async (saldoadocao) => {
        if (saldoadocao > 0) {

            let aumentarSaldoPublicador = 0;
            let diminuirSaldoReceptor = 0;
            let diminuirQtdLivros = 0;
            let diminuirQtdPublicacoes = 0;

            let doacao = await firebase.database().ref('doacao');
            let iddoacao = doacao.push().key;

            doacao.child(iddoacao).set({
                doador: {
                    iduser: arrayListPublicacao.iduser,
                    nome: arrayListPublicacao.nomeusuario,
                    telefone: arrayListPublicacao.telefoneusuario,
                    email: arrayListPublicacao.emailusuario,
                },
                receptor: {
                    iduser: objetousuario.chave,
                    nome: objetousuario.nome,
                    telefone: objetousuario.telefone,
                    email: objetousuario.email,
                },
                publicacao: {
                    idpublicacao: arrayListPublicacao.key,
                    datapublicacao: arrayListPublicacao.datapublicacao,
                    titulo: arrayListPublicacao.titulo,
                    descricao: arrayListPublicacao.descricao
                },
                notificacaoemail: notificacaoEmail,
                livro: {
                    idlivro: arrayListPublicacao.idlivro,
                    imagem: arrayListPublicacao.imagem,
                    titulo: arrayListPublicacao.titulolivro,
                    editora: arrayListPublicacao.editoralivro,
                    isbn: arrayListPublicacao.isbn,
                    autor: arrayListPublicacao.autor
                },
                datadoacao: mostrarData,
                statusdoacao: true,
            })

            await firebase.database().ref(`publicacao/${arrayListPublicacao.key}`).remove();

            await firebase.database().ref(`livro/${arrayListPublicacao.idlivro}`).remove();

            diminuirQtdLivros = qtdLivros - 1;
            diminuirQtdPublicacoes = qtdPublicacoes - 1;

            await firebase.database().ref('usuario/' + arrayListPublicacao.iduser + '/livros_cadastrados').set(diminuirQtdLivros);
            await firebase.database().ref('usuario/' + arrayListPublicacao.iduser + '/publicacoes').set(diminuirQtdPublicacoes);


            aumentarSaldoPublicador = saldoAdocaoDoador + 1;
            diminuirSaldoReceptor = saldoAdocao - 1;

            await firebase.database().ref('usuario/' + arrayListPublicacao.iduser + '/saldo_adocao').set(aumentarSaldoPublicador);
            await firebase.database().ref('usuario/' + objetousuario.chave + '/saldo_adocao').set(diminuirSaldoReceptor);

            const FROMEMAIL = "tccbookergift@gmail.com";
            const TOEMAILDOADOR = arrayListPublicacao.emailusuario;
            const TOEMAILRECEPTOR = objetousuario.email;
            const SUBJECTDOADOR = "Parabéns ! Você acabou de Doar um livro !";
            const SUBJECTRECEPTOR = "Parabéns ! Você acabou de Adotar um livro !";

            const ContactDetailsDoador = `Olá, ${arrayListPublicacao.nomeusuario}, tudo bem ? ${'\n'} O usuário ${objetousuario.nome} aceitou uma publicação do seu livro: ${arrayListPublicacao.titulolivro}. ${'\n'} Para que vocês possam continuar com o andamento da doação segue os dados de contato do ${objetousuario.nome}. ${'\n'} Email: ${objetousuario.email} Telefone: ${objetousuario.telefone}. ${'\n'}Agradeçemos pela sua Doação !`
            const ContactDetailsReceptor = `Olá, ${objetousuario.nome}, tudo bem ? ${'\n'} Você acabou de aceitar uma publicação do livro: ${arrayListPublicacao.titulolivro} publicado pelo usuário: ${arrayListPublicacao.nomeusuario}${'\n'} Para que vocês possam continuar com o andamento da doação e você receba seu livro, segue os dados de contato do ${arrayListPublicacao.nomeusuario}. ${'\n'} Email: ${arrayListPublicacao.emailusuario} Telefone: ${arrayListPublicacao.telefoneusuario}. ${'\n'}Agradeçemos pela sua Doação !`

            const sendRequestDoador = sendGridEmail(SENDGRIDAPIKEY, TOEMAILDOADOR, FROMEMAIL, SUBJECTDOADOR, ContactDetailsDoador);
            const sendRequestReceptor = sendGridEmail(SENDGRIDAPIKEY, TOEMAILRECEPTOR, FROMEMAIL, SUBJECTRECEPTOR, ContactDetailsReceptor);

            sendRequestDoador.then((response) => {
                console.log("Success")
            }).catch((error) => {
                console.log(error)
            });

            sendRequestReceptor.then((response) => {
                console.log("Success")
            }).catch((error) => {
                console.log(error)
            });

            setIsVisible3(!isVisible3);
            setIsVisible(!isVisible);

            navigation.navigate('ConfirmDoacao', {
                screen: 'ConfirmDoacao'
            });

        } else {

            setIsVisible3(!isVisible3);
            setIsVisible(!isVisible);

            navigation.navigate('CancelDoacao', {
                screen: 'CancelDoacao'
            })
        }
    }


    const renderPublicacoesHome = (totalPublicacoes) => {
        if (totalPublicacoes > 0) {
            return (
                <ScrollView>
                    {publicacoes.map(publicacao => (

                        <TouchableOpacity style={styles.cardpublicacao} onPress={() => buscarPublicacao(publicacao.key)}>

                            <View style={styles.viewimgpublicacao}>
                                <Image style={styles.imgpublicacao} source={{ uri: publicacao.imagem }} />

                                {minhaPublicacao(publicacao.iduser)}

                            </View>

                            <View style={styles.viewinfopublicacao}>

                                {/* VIEW DOS TÍTULOS PUBLICADOR E LIVRO*/}
                                <View style={styles.viewtitulos}>
                                    <View style={styles.boxtitulo}>
                                        <Text style={styles.txtboxtitulo}>Publicador</Text>
                                    </View>

                                    <View style={styles.boxtitulo}>
                                        <Text style={styles.txtboxtitulo}>Livro</Text>
                                    </View>

                                </View>

                                <View style={styles.viewinfospubli}>

                                    {/* VIEW DO BOX PUBLICADOR COM A FOTO, NOME E DATA DE PUBLICAÇÃO*/}
                                    <View style={styles.boxtpublicador}>
                                        <View style={styles.foto}>
                                            <View style={styles.viewphoto2}>
                                                <Image
                                                    style={styles.imagecircle2}
                                                    source={require('../../assets/userphoto.png')}
                                                />
                                            </View>
                                        </View>

                                        <View style={styles.nomefoto}>
                                            <View style={{ width: '100%', height: '50%' }}>
                                                <Text style={{ fontWeight: 'bold' }} numberOfLines={1}>{publicacao.nomeusuario}</Text>
                                                <Text style={{ fontSize: 14, color: 'gray' }}>{publicacao.datapublicacao}</Text>
                                            </View>
                                        </View>
                                    </View>

                                    {/* VIEW DO NOME DO LIVRO E STATUS DA PUBLICAÇÃO*/}
                                    <View style={styles.boxlivro}>
                                        <View style={{ width: '100%', height: '50%', paddingLeft: 10, paddingRight: 20 }}>
                                            <Text numberOfLines={1}>{publicacao.nomelivro}</Text>
                                        </View>
                                        <View style={{ width: '100%', height: '50%', justifyContent: 'flex-start', paddingRight: 25 }}>
                                            <Text style={{ fontWeight: 'bold', color: '#11bd2e', textAlign: 'right' }}>  EM ABERTO</Text>
                                        </View>
                                    </View>
                                </View>

                            </View>
                        </TouchableOpacity>
                    ))}
                </ScrollView>

            );
        } else {
            return (
                <View style={{ width: wp('100%'), height: '71%', }}>
                    {/* VIEW do ICON da carinha triste */}
                    <View style={{ height: '50%', width: '100%', alignItems: 'center', justifyContent: 'flex-end' }}>
                        <Icon name="md-sad-outline" color="#c8c8c8" size={80} />
                    </View>

                    {/* VIEW do TXT: Ops não há nenhum livro aqui ! */}
                    <View style={{ height: '50%', width: '100%', alignItems: 'center' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#c3c3c3' }}>
                            OPS, Não há nada por aqui !
                            </Text>

                    </View>
                </View>
            );
        }
    }

    const loggout = () => {
        setIsVisible4(!isVisible4);

        navigation.navigate('Login', {
            screen: 'Login'
        });
    }

    useEffect(() => {

        console.log(objetousuario);
        getDataObject();
        getNomeUsuario();
        getDataPublicacao();

    }, [nomeUsuario, publicacoes.length]);

    useEffect(() => {
        publicacaoStorage();
        //console.log(arrayListLivro);
        pegarQtdLivrosDoador();
        pegarQtdPublcacoesDoador();
        pegarSaldoAdocaoDoador();
        //console.log(`Qtd de livros publicador:${qtdLivros} Qtd publicacoes Publicador${qtdPublicacoes} Saldo adocao publicador${saldoAdocaoDoador} chave publicador: ${arrayListPublicacao.iduser}`)

    }, [arrayListPublicacao, qtdLivros, qtdPublicacoes, saldoAdocaoDoador]);


    return (
        <KeyboardAvoidingView style={styles.container}>
            {/* Heade principal das informações do usuário, botão de sair e Estante Virtual */}
            <View style={{ overflow: 'hidden', paddingBottom: 5 }}>

                {/* View topo onde ficará as informações do perfil do usuário */}
                <View style={styles.topo}>

                    <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                        <View style={styles.viewphoto}>
                            <Image
                                style={styles.imagecircle}
                                source={require('../../assets/userphoto.png')}
                            />
                        </View>

                        <View style={styles.dadosperfil}>
                            <View style={styles.dadosnome}>
                                <Text style={{ fontSize: 17 }}>{nomeUsuario}</Text>
                            </View>
                            <View>
                                <Text style={{ color: 'gray' }}>{objetousuario.email}</Text>
                            </View>
                        </View>

                        <View style={{ alignSelf: 'center' }}>
                            <TouchableOpacity onPress={() => setIsVisible4(!isVisible4)}><Text>Sair</Text></TouchableOpacity>
                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', height: '25%', borderBottomWidth: 1, borderColor: '#c3c3c3' }}>
                        <View style={{ justifyContent: 'center' }}>
                            <Icon name="library" size={25} color={'#55bede'} />
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 18, paddingLeft: 10 }}>Estante Virtual</Text>
                        </View>
                    </View>

                    <View>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, marginTop: 20 }}>Publicações em Aberto: ({publicacoes.length})</Text>
                    </View>

                </View>
            </View>

            {/* Área de todas as publicações feitas */}

            {renderPublicacoesHome(publicacoes.length)}
            {/* Modal de informações da publicação que foi escolhida pelo usuário */}
            <Modal
                animationType="slide"
                transparent={true}
                visible={isVisible}
                onRequestClose={() => {
                    setIsVisible(!isVisible);
                }}
            >
                <View style={styles.centeredView2}>
                    <View style={styles.modalView2}>

                        {/*View da imagem da publicação*/}
                        <View style={styles.imgmodalpublicacao}>
                            <Image style={styles.imgpublicacao} source={{ uri: arrayListPublicacao.imagem }} />
                            {minhaPublicacao2(arrayListPublicacao.iduser)}
                        </View>

                        <View style={styles.viewtitlepublicacao}>
                            <View style={styles.view_foto_publi}>
                                <Image
                                    style={styles.imagecircle3}
                                    source={require('../../assets/userphoto.png')}
                                />
                            </View>

                            <View style={styles.infofoto}>
                                <Text style={{ fontWeight: 'bold', fontSize: 15 }} numberOfLines={1}>{arrayListPublicacao.nomeusuario}</Text>
                                <Text style={{ fontSize: 14, color: 'gray' }}>Publicação: {arrayListPublicacao.datapublicacao}</Text>
                            </View>

                            <View style={styles.publicador}>
                                <Text style={{ fontWeight: 'bold', color: '#55bede' }}>-  Publicador</Text>
                            </View>
                        </View>

                        {/* Views do título e descrição*/}
                        <View style={{ width: '100%', height: '8%', justifyContent: 'center', paddingLeft: 10, }}>
                            <View style={styles.comment}>
                                <Text style={{ fontWeight: 'bold', color: '#7a7a7a' }}>Título da publicação:</Text>
                                <Text style={{ fontSize: 16, fontStyle: 'italic', color: '#a3a3a3' }} numberOfLines={1}>{arrayListPublicacao.titulo}</Text>
                            </View>
                        </View>

                        <View style={{ width: '100%', height: '18%', justifyContent: 'center', paddingLeft: 10, paddingTop: 5, }}>
                            <View style={styles.comment2}>
                                <ScrollView style={{ width: '95%', height: '100%', alignSelf: 'flex-end', backgroundColor: '#ededed', padding: 20, borderRadius: 10, alignSelf: 'flex-start' }}>

                                    <Text style={{ fontWeight: 'bold', color: '#7a7a7a' }}>Descrição:</Text>
                                    <Text style={{ fontSize: 16, fontStyle: 'italic', color: '#a3a3a3' }}>
                                        <Text style={{ fontSize: 25, fontStyle: 'italic', fontWeight: 'bold', color: '#55bede' }}>"</Text>
                                        {arrayListPublicacao.descricao}
                                        <Text style={{ fontSize: 25, fontStyle: 'italic', fontWeight: 'bold', color: '#55bede' }}>"</Text>
                                    </Text>
                                </ScrollView>

                            </View>
                        </View>

                        <View style={{ width: '100%', height: '25%', justifyContent: 'center', paddingTop: 5 }}>
                            <View style={{ width: '85%', height: '100%', alignSelf: 'center' }}>
                                <View style={{ width: '100%', marginBottom: 10, marginTop: 5 }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Informações do Livro:</Text>
                                </View>

                                <View>
                                    <View style={{ width: '100%', marginBottom: 4 }}>
                                        <Text style={{ fontWeight: 'bold', color: '#7a7a7a' }} numberOfLines={3}>
                                            Título Livro:
                                             <Text style={{ fontWeight: 'normal' }}> {arrayListPublicacao.titulolivro}</Text>
                                        </Text>
                                    </View>

                                    <View style={{ width: '100%', marginBottom: 4 }}>
                                        <Text style={{ fontWeight: 'bold', color: '#7a7a7a' }}>
                                            Autor:
                                            <Text style={{ fontWeight: 'normal' }}>  {arrayListPublicacao.autor}</Text>
                                        </Text>
                                    </View>

                                    <View style={{ width: '100%', marginBottom: 4 }}>
                                        <Text style={{ fontWeight: 'bold', color: '#7a7a7a' }}>
                                            Editora:
                                            <Text style={{ fontWeight: 'normal' }}>  {arrayListPublicacao.editoralivro}</Text>
                                        </Text>
                                    </View>


                                    <View style={{ width: '100%', marginBottom: 4 }}>
                                        <Text style={{ fontWeight: 'bold', color: '#7a7a7a' }}>
                                            ISBN:
                                            <Text style={{ fontWeight: 'normal', color: '#7a7a7a' }}>  {arrayListPublicacao.isbn}</Text>
                                        </Text>
                                    </View>

                                </View>
                            </View>
                        </View>

                        <View style={{ width: '100%', height: '14%', justifyContent: 'center' }}>
                            {renderMinhaPublicacao(arrayListPublicacao.iduser)}
                        </View>


                    </View>
                </View>
            </Modal>

            {/* Modal informativo sobre o que é o SALDO ADOÇÃO */}
            <Modal
                animationType="slide"
                transparent={true}
                visible={isVisible2}
                onRequestClose={() => {
                    setIsVisible2(!isVisible2)
                }}
                style={{ width: '50%' }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Icon name="ios-information-circle" color="#e0b94c" size={50} />
                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>O que é o Saldo Adoção?</Text>

                        <View style={{ width: '100%', height: '70%' }}>
                            <View style={{ marginBottom: 15, marginTop: 15 }}><Text style={{ fontSize: 16, textAlign: 'justify' }}>O saldo adoção é um controle de livros que são adotados (Doados para você) por você.</Text></View>
                            <View style={{ marginBottom: 15 }}><Text style={{ fontSize: 16, textAlign: 'justify' }}>Você só poderá aceitar um livro/fazer uma adoção somente se este saldo estiver positivo.</Text></View>
                            <View style={{ marginBottom: 15 }}><Text style={{ fontSize: 16, textAlign: 'justify' }}>Para conseguir acumular o Saldo Adoção, é preciso publicar um livro e realizar uma doação que irá contabilizar no saldo.</Text></View>
                            <View style={{ marginBottom: 15 }}><Text style={{ fontSize: 16, textAlign: 'justify', fontWeight: 'bold' }}>Logo, se você doa, você tem direito de receber !</Text></View>
                        </View>

                        <View style={{ flexDirection: 'row', height: '12%' }}>

                            <TouchableOpacity
                                style={{ ...styles.openButton, backgroundColor: "#e8695a" }}
                                onPress={() => {
                                    setIsVisible2(!isVisible2);
                                }}
                            >
                                <Text style={{ fontSize: 16, color: '#fff', fontWeight: 'bold' }}>FECHAR</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            </Modal>

            <Modal
                animationType="slide"
                transparent={true}
                visible={isVisible3}
                onRequestClose={() => {
                    setIsVisible3(!isVisible3);
                }}
                style={{ width: '50%' }}>

                <View style={styles.centeredView2}>
                    <View style={styles.modalView3}>

                        <View style={{ width: '100%', height: '50%', justifyContent: 'center', alignSelf: 'center' }}>
                            <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>Você está prestes a realizar uma adoção, deseja continuar?</Text>
                        </View>

                        <View style={{ flexDirection: 'row', width: '80%' }}>
                            <TouchableOpacity style={styles.buttonremover2} onPress={() => setIsVisible3(!isVisible3)}>
                                <Text style={{ color: 'white', fontWeight: 'bold' }}>Não, Cancelar !</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.buttonpublicar2} onPress={() => criarDoacao(saldoAdocao)}>
                                <Text style={{ color: 'white', fontWeight: 'bold' }} >Sim, Continuar !</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>

            </Modal>

            <Modal
                animationType="slide"
                transparent={true}
                visible={isVisible4}
                onRequestClose={() => {
                    setIsVisible(!isVisible4);
                }}
                style={{ width: '50%' }}
            >
                <View style={styles.centeredView2}>
                    <View style={styles.modalView4}>


                        <Text style={{ fontWeight: 'bold' }}>Deseja realmente deslogar da aplicação ?</Text>

                        <View style={{ flexDirection: 'row', marginTop: '10%' }}>
                            <TouchableOpacity style={styles.buttonremover3} onPress={() => setIsVisible4(!isVisible4)}>
                                <Text style={{ color: 'white', fontWeight: 'bold' }}>Cancelar</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.buttonpublicar3}>
                                <Text style={{ color: 'white', fontWeight: 'bold' }} onPress={() => loggout()}>Sair</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </Modal>
        </KeyboardAvoidingView >
    );
}