import React from 'react'
import { View, Text, ImageBackground, Image } from 'react-native'

//Importando a Estilização comum (Sem styled Components)
import styles from './styles';

//Importando Componente da barra inferior
import Downbarinformative from '../../Components/Downbarinformative'

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/MaterialIcons';


export default function Informative4() {
    return (
        <View style={styles.container}>

            <ImageBackground source={require('../../assets/fundo4.png')} style={styles.image}>


                {/* View Inferior principal de manuseio das informatives screens */}
                <View style={styles.viewinferior}>

                    {/*Componente da barra inferior da tela */}
                    <Downbarinformative c1='white' c2='white' c3='white' c4='yellow' screenName='Login' />

                </View>

                {/* View geral da descrição do Título da informative Screen 4 */}
                <View style={styles.viewsubtitulo}>
                    <Text style={styles.textdescricao}>O ISBN, International Standard Book Number, trata-se de um sistema internacional de identificação dos livros e softwares que utilizam números para a classificação por título, autor, país, editora e edição.</Text>
                </View>

                {/* View geral do Título da informative Screen 4 */}
                <View style={styles.viewtxttitle}>

                    <View style={styles.viewicontitle}>
                        <Icon name="fiber-manual-record" size={15} color={'#6bff84'} />
                    </View>

                    <View style={styles.viewtitle}>
                        <Text style={styles.texttitulo}>Conhece o código ISBN?</Text>
                    </View>

                </View>
            </ImageBackground>

        </View>
    );
}