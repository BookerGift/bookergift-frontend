import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        width:wp('100%'),
        height:('100%'),
    
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        flexDirection:'column-reverse'
      },
      text: {
        color: "grey",
        fontSize: 30,
        fontWeight: "bold"
      },
      viewinformative21:{
        //borderWidth: 5,
        //alignSelf:'center',
        width:wp('100%'),
        height:hp('70%'),
        //marginTop:hp('30%'),
        //alignItems:'center',
        justifyContent:'flex-end'
      },
      imgprincipal:{
        resizeMode:'contain',
        width:wp('100%'),
        height:hp('100%'),
      },
      imgsubprincipal:{
        resizeMode:'contain',
        width:wp('80%'),
        height:hp('100%'),
        alignSelf:'center'
      },
      texttitulo:{
          color:'white',
          fontSize: 22,
          fontWeight:'bold',
          color:'#FFDA87'
      },

      viewinferior:{
        justifyContent:'center',
        //borderWidth:5,
        borderColor:'red',
        height:hp('10%'),
        marginTop:wp('8%'),
      },

      viewtxttitle:{
        //borderWidth:5,
        height:'10%',
        flexDirection:'row',
        paddingLeft:wp('5%'),
      },
      viewicontitle:{
        //borderWidth:5,
        justifyContent:'center',
      },
      viewtitle:{
        //borderWidth:5,
        justifyContent:'center',
        paddingLeft:wp('5%')
      },
      viewsubtitulo:{
        //borderWidth:5,
        height:'20%',
        paddingLeft:wp('5%'),  
        paddingRight:wp('5%')
    },
      textdescricao:{
        color:'white',
        fontSize:16,
        lineHeight: 30,
      }
});

export default styles;
