import React, { useEffect, useState } from 'react'
import { View, Text, ImageBackground, Image, Alert } from 'react-native'

//Importando a Estilização comum (Sem styled Components)
import styles from './styles';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

//Importando Componente da barra inferior
import Downbarinformative from '../../Components/Downbarinformative'

import AsyncStorage from '@react-native-async-storage/async-storage';

export default function Informative1() {

    //Atribuindo a navegação para a constante navigantion, assim
    //a mesma tem acesso à nevegação entre as telas da aplicação
    const navigation = useNavigation();

    return (
        <View style={styles.container}>

            <ImageBackground source={require('../../assets/fundo1.jpg')} style={styles.image}>

                {/* Imagem da logo principal */}
                <View style={styles.viewimgprincipal}>
                    <Image source={require('../../assets/logobranco.png')} style={styles.imgprincipal} ></Image>
                </View>

                {/* Imagem da sublogo(título) principal */}
                <View style={styles.viewinformative1}>
                    <Image source={require('../../assets/logoescrita.png')} style={styles.imgsubprincipal} ></Image>
                </View>

                {/* View de descrição inicial da aplicação */}
                <View style={styles.viewinformative2}>
                    <Text style={styles.textdescricao}>Um jeito diferente de trocar e doar livros !</Text>
                </View>

                {/* View Inferior principal de manuseio das informatives screens */}
                <View style={styles.viewinferior}>

                    {/*Componente da barra inferior da tela */}
                    <Downbarinformative c1='yellow' c2='white' c3='white' c4='white' screenName='Informative2' />

                </View>

            </ImageBackground>

        </View>
    );
}