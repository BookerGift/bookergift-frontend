import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        flex:1,
    },
    image: {
        flex: 1,
        resizeMode: "cover",
      },
      text: {
        color: "grey",
        fontSize: 30,
        fontWeight: "bold"
      },
      viewimgprincipal:{
        //borderWidth: 5,
        alignSelf:'center',
        marginTop:hp('15%'),
        width:wp('40%'),
        height:hp('20%'),
        //borderWidth:5
      },
      viewinformative1:{
        //borderWidth: 5,
        alignSelf:'center',
        width:wp('60%'),
        height:hp('10%'),
      },
      viewinformative2:{
        //borderWidth: 5,
        alignSelf:'center',
        width:wp('85%'),
        height:hp('7%'),
        marginTop:hp('10%'),
        alignItems:'center',
        justifyContent:'center',
        //borderWidth:5
      },
      imgprincipal:{
        resizeMode:'contain',
        width:'100%',
        height:'100%',
      },
      imgsubprincipal:{
        resizeMode:'contain',
        width:'80%',
        height:'100%',
        alignSelf:'center'
      },
      textdescricao:{
          color:'white',
          fontSize: 18,
      },

      viewinferior:{
        flex:1,
        flexDirection:'column-reverse',
        //borderWidth:5,
      },
});

export default styles;
