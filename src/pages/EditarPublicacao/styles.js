import { StyleSheet } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topo: {
    width: wp('100%'),
    height: hp('12%'),
    //backgroundColor: '#fff',
    paddingLeft: wp('3%'),
    justifyContent: 'center',
    //borderWidth:3
  },
  viewphoto: {
    //borderWidth:3,
    borderColor: 'gray',
    width: '15%',
    height: '100%',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  dadosperfil: {
    width: '70%',
    height: '100%',
    //borderWidth:3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewpublicacaogeral: {
    width: wp('90%'),
    height: hp('80%'),
    //borderWidth: 1,
    //justifyContent: 'center',
    //padding: wp('4%'),
    //flexDirection: 'row',
    alignSelf: 'center',
    borderRadius: 10,
    backgroundColor: '#fff',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  viewimgpub: {
    width: '90%',
    height: '25%',
    borderWidth: 2,
    alignSelf: 'center',
    marginTop: 15,
    borderRadius: 10,
    borderColor: '#2eb329'
    //borderTopRightRadius: 10,
  },
  viewinfopub: {
    width: '90%',
    height: '18%',
    //borderWidth: 1,
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  vieweditinputs: {
    width: '90%',
    height: '42%',
    //borderWidth: 1,
    alignSelf: 'center',
  },
  info1: {
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    //borderWidth: 1,
  },
  info2: {
    width: '50%',
    height: '100%',
    //borderWidth: 1,
    justifyContent: 'center',
  },
  viewbnt: {
    width: '90%',
    height: '100%',
    //borderWidth: 1,
    //alignSelf: 'center',
    backgroundColor: '#2eb329',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewbnt2: {
    width: '90%',
    height: '100%',
    //borderWidth: 1,
    //alignSelf: 'center',
    backgroundColor: '#55bede',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewboxtitulo: {
    width: '100%',
    height: '20%',
    flexDirection: 'row',
    //borderWidth: 1
  },
  viewboxdescricao: {
    width: '100%',
    height: '45%',
    //borderWidth: 1,
    flexDirection: 'row'
  },
  bntpublicar: {
    width: '100%',
    height: '25%',
    backgroundColor: '#55bede',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputtitulo: {
    width: '100%',
    backgroundColor: '#fff',
    paddingLeft: 15,
    height: '100%',
    color: 'gray',
    //opacity:0.2,
    //color:'#fff',
    //height:hp('7%'),
    //paddingLeft:15,
    borderWidth: 2,
    borderColor: '#55bede',
    borderRadius: 10,
  },
  inputdescricao: {
    width: '100%',
    backgroundColor: '#fff',
    paddingLeft: 15,
    height: '100%',
    color: 'gray',
    //opacity:0.2,
    //color:'#fff',
    //height:hp('7%'),
    //paddingLeft:15,
    borderWidth: 2,
    //borderBottomWidth: 2,
    borderLeftWidth: 2,
    borderColor: '#55bede',
    borderRadius: 10,


  },
  inputtitulo2: {
    width: '100%',
    backgroundColor: '#f2f2f2',
    paddingLeft: 15,
    height: '100%',
    color: 'gray',
    //opacity:0.2,
    //color:'#fff',
    //height:hp('7%'),
    //paddingLeft:15,
    borderTopWidth: 2,
    borderWidth: 2,
    borderColor: '#b3b3b3',
    borderRadius: 10,
  },
  inputdescricao2: {
    width: '100%',
    backgroundColor: '#f2f2f2',
    paddingLeft: 15,
    height: '100%',
    color: 'gray',
    //opacity:0.2,
    //color:'#fff',
    //height:hp('7%'),
    //paddingLeft:15,
    borderWidth: 2,
    borderLeftWidth: 2,
    borderColor: '#b3b3b3',
    borderRadius: 10,
    alignSelf: 'flex-end'

  },
  viewiconedit2: {
    width: '20%',
    height: '100%',
    //borderWidth: 1,
    borderTopRightRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#b3b3b3'
  },
  viewiconeditDescricao: {
    width: '20%',
    height: '100%',
    //borderWidth: 1,
    borderTopRightRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#55bede'
  },
  viewiconedit: {
    width: '20%',
    height: '75%',
    //borderWidth: 1,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#b3b3b3'
  },
  viewiconeditTitulo: {
    width: '20%',
    height: '75%',
    //borderWidth: 1,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#55bede'
  },
  areabuttons: {
    width: '90%',
    height: '10%',
    //borderWidth: 1,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  bntEditarSalvar: {
    width: '50%',
    height: '100%',
    //borderWidth: 1
  },
  bntremover: {
    width: '50%',
    height: '100%',
    //borderWidth: 1
  },
  bnttremover: {
    width: '90%',
    height: '100%',
    //borderWidth: 1,
    alignSelf: 'center',
    backgroundColor: '#d44c4c',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  centeredView2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(52, 52, 52, 0.8)'
  },
  modalView2: {
    width: wp('70%'),
    height: hp('20%'),
    //margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    //borderWidth: 1,
    padding: 25,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  buttonremover2: {
    width: '50%',
    height: '100%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#f54842',
    justifyContent: 'center',
    alignItems: 'center',
    //right: wp('2%')
  },
  buttonpublicar2: {
    width: '50%',
    height: '100%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#2ed936',
    justifyContent: 'center',
    alignItems: 'center',
    left: wp('3%')
  },
});

export default styles;