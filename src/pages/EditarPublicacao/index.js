import React, { useState, useEffect } from 'react';
import { View, Text, KeyboardAvoidingView, Image, TouchableOpacity, SnapshotViewIOS, ScrollView, TextInput, Alert, Modal } from 'react-native';
//importando o firebase
import firebase from '../../FirebaseConnection';

//Importando a Estilização StyleSheet
import styles from './styles';

//Importação da biblioteca de icons do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

import AsyncStorage from '@react-native-async-storage/async-storage';

export default function EditarPublicacao() {

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  const [objetousuario, setObjetoUsuario] = useState('');
  const [livro, setLivro] = useState([]);
  const [objetopublicacao, setObjetoPublicacao] = useState('');
  const [visibleEdit1, setVisibleEdit1] = useState(false);
  const [visibleEdit2, setVisibleEdit2] = useState(false);
  const [inputEdit1, setInputEdit1] = useState(false);
  const [inputEdit2, setInputEdit2] = useState(false);
  const [pegarTitulo, setPegarTitulo] = useState('');
  const [pegarDescricao, setPegarDescricao] = useState('');
  const [newTitulo, setNewTitulo] = useState('');
  const [newDescricao, setNewDescricao] = useState('');
  const [visibleModal, setVisibleModal] = useState(false);
  const [visibleModal2, setVisibleModal2] = useState(false);
  const [qtdPublicacao, setQtdPublicacao] = useState(0);

  const getDataObjects = async () => {
    try {
      const pegarobjetouser = await AsyncStorage.getItem('@objectuser');
      const pegarobjetopublicacao = await AsyncStorage.getItem('@objectpublicacao');

      setObjetoUsuario(JSON.parse(pegarobjetouser));
      setObjetoPublicacao(JSON.parse(pegarobjetopublicacao));

    } catch (e) {
      Alert.alert(e);
    }
  }

  const cancelarAlteracoes = () => {
    setVisibleModal(!visibleModal);
    setVisibleEdit1(!visibleEdit1);
    setInputEdit1(!inputEdit1);
    setVisibleEdit2(!visibleEdit2);
    setInputEdit2(!inputEdit2);
  }

  //Renderização condicional para o input de título
  const renderInputTitulo = () => {
    if (!visibleEdit1) {
      return (
        <TextInput style={styles.inputtitulo2}
          placeholderTextColor='gray'
          placeholder="Digite um título para a publicação !"
          onChangeText={text => setNewTitulo(text)}
          editable={inputEdit1}
          value={pegarTitulo}
          maxLength={40}
        />
      );
    } else {
      return (
        <TextInput style={styles.inputtitulo}
          placeholderTextColor='gray'
          placeholder="Digite um título para a publicação !"
          onChangeText={text => setNewTitulo(text)}
          editable={inputEdit1}
          value={newTitulo}
          maxLength={40}
        />
      );
    }
  }

  //Renderização condicional para o input de descrição 
  const renderInputDescricao = () => {
    if (!visibleEdit2) {
      return (
        <TextInput style={styles.inputdescricao2}
          multiline={true}
          numberOfLines={4}
          placeholderTextColor='gray'
          placeholder="Digite uma descrição para sua publicação !"
          onChangeText={text => setNewDescricao(text)}
          editable={inputEdit2}
          value={pegarDescricao}
          maxLength={200}
        />
      );
    } else {
      return (
        <TextInput style={styles.inputdescricao}
          multiline={true}
          numberOfLines={4}
          placeholderTextColor='gray'
          placeholder="Digite uma descrição para sua publicação !"
          onChangeText={text => setNewDescricao(text)}
          editable={inputEdit2}
          value={newDescricao}
          maxLength={200}
        />
      );
    }
  }

  //Controle de hibilitação dos inputs ao acionar o botão
  //"EDITAR DADOS"
  const habilitarInputs = () => {
    setVisibleEdit1(!visibleEdit1);
    setInputEdit1(!inputEdit1);
    setVisibleEdit2(!visibleEdit2);
    setInputEdit2(!inputEdit2);
  }


  const pegarTituloDB = async () => {
    await firebase.database().ref(`publicacao/${objetopublicacao.key}/titulo`).once('value', (snapshot) => {
      setPegarTitulo(snapshot.val());
    });

  };

  const pegarDescricaoDB = async () => {
    await firebase.database().ref(`publicacao/${objetopublicacao.key}/descricao`).once('value', (snapshot) => {
      setPegarDescricao(snapshot.val());
    });

  };

  const renderbntSalvar = () => {
    if (((inputEdit1) && (inputEdit2))) {
      return (
        <TouchableOpacity style={styles.viewbnt} onPress={() => setVisibleModal(!visibleModal)}>
          <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#fff' }}>SALVAR</Text>
        </TouchableOpacity>
      );
    } else {
      if (((!inputEdit1) && (!inputEdit2))) {
        return (
          <TouchableOpacity enabled={false} style={styles.viewbnt2} onPress={() => habilitarInputs()}>
            <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#fff' }}>EDITAR DADOS</Text>
          </TouchableOpacity>
        );
      }

    }
  }

  const renderStatus = (status) => {
    if (status) {
      return (
        <Text>Em Aberto</Text>
      );
    } else {
      return (
        <Text>Fechado</Text>
      );
    }
  }


  const alterarDados = async () => {
    if ((newTitulo != "") && (newDescricao != "")) {
      await firebase.database().ref('publicacao/' + objetopublicacao.key + '/titulo').set(newTitulo);
      await firebase.database().ref('publicacao/' + objetopublicacao.key + '/descricao').set(newDescricao);

      navigation.navigate('Home', {
        screen: 'Biblioteca'
      });

      Alert.alert('Dados da publicação alterados com sucesso !');
    } else {
      setVisibleModal(!visibleModal);
      Alert.alert("Algum ou todos os campos estão em branco, preencha algum dado!");

    }

  }

  const removerPublicacao = async (chavepublicacao) => {

    //1º Remover publicação
    let valueAdd = 0;
    let falso = false;
    valueAdd = qtdPublicacao - 1;

    await firebase.database().ref(`publicacao/${chavepublicacao}`).remove();
    setVisibleModal2(!visibleModal2);

    await firebase.database().ref('usuario/' + objetousuario.chave + '/publicacoes').set(valueAdd);
    //console.log(`Valor qtdLivros: ${qtdLivros}`);

    await firebase.database().ref('livro/' + objetopublicacao.idlivro + '/publicado').set(falso);
    //console.log(`Valor qtdLivros: ${qtdLivros}`);

    await firebase.database().ref(`livro/${objetopublicacao.idlivro}/publicacao`).remove();

    navigation.navigate('Home', {
      screen: 'Biblioteca',
    });

    Alert.alert('Publicação excluída/removida com sucesso !')

    //2º Diminuir contador de publicacao

    //3º Alterar status de publicado do Livro
  }

  const getQtdPublicacoes = async () => {
    await firebase.database().ref(`usuario/${objetousuario.chave}/publicacoes`).on('value', (snapshot) => {
      setQtdPublicacao(snapshot.val());
    });

    console.log(`QUANTIDADE: ${qtdPublicacao}`)
  }

  useEffect(() => {
    getDataObjects();
    pegarTituloDB();
    pegarDescricaoDB();
    getQtdPublicacoes();

  }, [objetopublicacao.key, objetousuario.chave, qtdPublicacao]);

  return (
    <View style={{ flex: 1 }}>
      {/* View topo onde ficará as informações do perfil do usuário */}
      <View style={styles.topo}>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity style={styles.viewphoto} onPress={() => navigation.goBack()}>
            <Icon name="arrow-back-outline" color="black" size={40} />
          </TouchableOpacity>

          <View style={styles.dadosperfil}>
            <View style={styles.dadosnome}>
              <Text style={{ fontSize: 17, fontWeight: 'bold' }}>EDITAR PUBLICAÇÃO</Text>
            </View>

          </View>
        </View>
      </View>

      <View style={styles.viewpublicacaogeral}>
        <View style={styles.viewimgpub}>
          <Image style={{
            width: '100%',
            height: '100%',
            resizeMode: 'cover',
            borderRadius: 10,
          }} source={{ uri: objetopublicacao.imagem }} />
        </View>

        <View style={styles.viewinfopub}>
          <View style={styles.info1}>
            <View>
              <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Livro:</Text>
              <Text numberOfLines={1}>{objetopublicacao.titulolivro}</Text>
            </View>

            <View style={{ marginTop: 13 }}>
              <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Publicador:</Text>
              <Text numberOfLines={1}>{objetopublicacao.nomeusuario}</Text>
            </View>
          </View>

          <View style={styles.info2}>
            <View>
              <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Data Publicação:</Text>
              <Text>{objetopublicacao.datapublicacao}</Text>
            </View>

            <View style={{ marginTop: 13 }}>
              <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Status Publicação:</Text>
              <Text>{renderStatus(objetopublicacao.status)}</Text>
            </View>
          </View>
        </View>

        {/* VIEW dos Imputs que serão editados */}
        <View style={styles.vieweditinputs}>
          <View style={{ width: '100%', height: '15%', justifyContent: 'center' }}>
            <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#575757' }}>Editar Título:</Text>
          </View>
          <View style={styles.viewboxtitulo}>
            {renderInputTitulo()}
          </View>


          <View style={{ width: '100%', height: '15%', justifyContent: 'center' }}>
            <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#575757' }}>Editar Descrição:</Text>
          </View>
          <View style={styles.viewboxdescricao}>
            {renderInputDescricao()}
          </View>
        </View>


        <View style={styles.areabuttons}>
          <View style={styles.bntEditarSalvar}>
            {renderbntSalvar()}
          </View>
          <View style={styles.bntremover}>
            <TouchableOpacity style={styles.bnttremover} onPress={() => setVisibleModal2(!visibleModal2)}>
              <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#fff' }}>REMOVER</Text>
            </TouchableOpacity>
          </View>
        </View>

        {/* */}

      </View>

      <Modal
        animationType="slide"
        transparent={true}
        visible={visibleModal}
        onRequestClose={() => {
          setVisibleModal(!visibleModal);
        }}
        style={{ width: '50%' }}
      >
        <View style={styles.centeredView2}>
          <View style={styles.modalView2}>


            <Text style={{ fontWeight: 'bold' }}>Deseja realmente salvar as alterações ?</Text>

            <View style={{ flexDirection: 'row', marginTop: '10%' }}>
              <TouchableOpacity style={styles.buttonremover2} onPress={() => cancelarAlteracoes()}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>Não, Cancelar !</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.buttonpublicar2} onPress={() => alterarDados()}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>Sim, salvar!</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>

      <Modal
        animationType="slide"
        transparent={true}
        visible={visibleModal2}
        onRequestClose={() => {
          setVisibleModal2(!visibleModal2);
        }}
        style={{ width: '50%' }}
      >
        <View style={styles.centeredView2}>
          <View style={styles.modalView2}>


            <Text style={{ fontWeight: 'bold' }}>Deseja realmente excluir/remover essa publicação?</Text>

            <View style={{ flexDirection: 'row', marginTop: '10%' }}>
              <TouchableOpacity style={styles.buttonremover2} onPress={() => setVisibleModal2(!visibleModal2)}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>Não, Cancelar !</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.buttonpublicar2} onPress={() => removerPublicacao(objetopublicacao.key)}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>Sim, excluir!</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>

    </View>
  );
}