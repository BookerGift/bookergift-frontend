import React, { useState } from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
} from 'react-native';

//importando componentes de GoBack e GoNext
import GoNext from '../../Components/GoNext';
import GoBack from '../../Components/GoBack';

//Importando biblioteca de mask para telefone ou máscaras no geral
import { TextInputMask } from 'react-native-masked-text';

//Importando a Estilização comum (Sem styled Components)
import styles from './styles';

//Importando biblioteca react-native-vector-icons
import Icon from 'react-native-vector-icons/AntDesign';

export default function Cadastro({ navigation }) {

  //Constante de estado do telefone que o usuário digitar
  //O estado está sendo feito com o hook useState, pois
  //se trata de com componente Functional 
  const [myTelefone, setMyTelefone] = useState('')

  //Esta função faz com o que tenha o controle do botão
  //de avançar para a próxima tela, logo, o mesmo só estará
  //habilitado quando o telefone só tiver 11 caracteres
  const controleGoNext = () => {
    let telefonecontrole = myTelefone;

    if (telefonecontrole.length < 11) {
      return <GoNext telefone={myTelefone} controle='true' colorcontrole='gray' />
    } else {
      if (telefonecontrole.length >= 11) {
        return <GoNext telefone={myTelefone} controle='false' colorcontrole='#2cafd8' />
      }
    }
  };

  return (
    <KeyboardAvoidingView style={styles.container}>



      {/*Primeira estrutura tela de cadastro */}
      <ImageBackground
        source={require('../../assets/fundo5-1.png')}
        style={styles.image}>

        {/* Componente do goBack para voltar à tela anterior */}
        <View style={styles.viewgo}>
          <GoBack irParaTela='Login' colorback='#fff' />
        </View>

        {/* Imagem da logo principal */}
        <KeyboardAvoidingView style={styles.viewimgprincipal}>
          <Image
            source={require('../../assets/logobranco.png')}
            style={styles.imgprincipal}></Image>
        </KeyboardAvoidingView>

        {/* Imagem da sublogo(título) principal */}
        <KeyboardAvoidingView style={styles.viewinformative1}>
          <Image
            source={require('../../assets/logoescrita.png')}
            style={styles.imgsubprincipal}></Image>
        </KeyboardAvoidingView>


      </ImageBackground>

      <KeyboardAvoidingView style={styles.viewncelular}>
        <Text style={{ fontSize: 16 }}>Vamos começar seu cadastro?</Text>

        {/*Seleção País*/}
        <View style={styles.viewpais}>
          {/*Bandeira do País*/}
          <View style={styles.viewimgpais}>
            <Image
              source={require('../../assets/brazil.png')}
              style={styles.imgpais}></Image>
          </View>

          {/*Seta e TXT Brazil*/}
          <TouchableOpacity style={styles.viewbraziltxt}>
            <Text style={{ fontSize: 16, color: 'black', fontWeight: 'bold' }}>
              + 55
              </Text>
          </TouchableOpacity>

          <View style={styles.viewinputcel}>
            <TextInputMask
              type={'cel-phone'}
              options={{
                maskType: 'BRL',
                withDDD: true,
              }}
              placeholder="Digite seu telefone com DDD"
              value={myTelefone}
              onChangeText={text => {
                setMyTelefone(text.replace(/\D+/g, ""));
              }}
              style={{ fontSize: 16, height: '70%' }}
            />
          </View>
        </View>


        <View style={styles.viewgo}>
          {controleGoNext()}
        </View>

      </KeyboardAvoidingView>
    </KeyboardAvoidingView>
  );
}
