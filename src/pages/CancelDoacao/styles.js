import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //borderWidth: 1,
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  containerInfo: {
    width: '95%',
    height: '90%',
    //borderWidth: 1,
    alignSelf: 'center',
    borderRadius: 10,
    backgroundColor: '#fff',
    padding: 20,
    justifyContent: 'center'
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: 'center'
  },
  viewtitleinfo: {
    width: '100%',
    height: '12%',
    //borderWidth: 1,
    justifyContent: 'center'
  },
  viewiconinfo: {
    width: '100%',
    height: '20%',
    //borderWidth: 1,
    //justifyContent: 'center',
    flexDirection: 'column-reverse'
  },
  infosubtitle: {
    width: '100%',
    height: '40%',
    //borderWidth: 1,
    //justifyContent: 'center',
    //flexDirection: 'column-reverse'
  },
  viewbuttonexit: {
    width: '100%',
    height: '10%',
    //borderWidth: 1,
    justifyContent: 'center',
    //flexDirection: 'column-reverse'
  }
});

export default styles;