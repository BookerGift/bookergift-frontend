import React from 'react';
import { View, Text, ImageBackground } from 'react-native';

//Importando a Estilização StyleSheet
import styles from './styles';

//Importando biblioteca react-native-vector-icons
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

import { TouchableOpacity } from 'react-native-gesture-handler';

export default function CancelDoacao() {

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  const gotoHome = () => {
    navigation.navigate('Home', {
      screen: 'Home'
    })
  }

  return (
    <View style={styles.container}>


      <View style={styles.containerInfo}>
        <View style={styles.viewiconinfo}>
          <Icon style={{ alignSelf: 'center' }} name="emoticon-sad" size={100} color="#d44c4c" />
        </View>

        <View style={styles.viewtitleinfo}>
          <Text style={{ textAlign: 'center', fontSize: 40, fontWeight: 'bold' }}>
            OPS...
            </Text>
        </View>

        <View style={styles.infosubtitle}>
          <Text style={{ fontSize: 16, lineHeight: 30 }}>Verificamos que seu <Text style={{ fontWeight: 'bold' }}>Saldo Adoção</Text> é insuficiente para que você possa fazer essa adoção !</Text>
          <View style={{ marginTop: 20 }}>
            <Text style={{ fontSize: 16, lineHeight: 30 }}> Para que você possa aceitar uma publicação é necessário ter o saldo adoção positivo, logo é necessário realizar uma doação, assim para cada doação feita, você tem direito a fazer uma adoção !</Text>
          </View>
        </View>

        <View style={styles.viewbuttonexit}>
          <TouchableOpacity
            style={{
              width: '40%',
              borderRadius: 10,
              backgroundColor: '#d44c4c',
              height: '100%',
              alignSelf: 'center',
              //borderWidth: 1,
              justifyContent: 'center'
            }} onPress={() => gotoHome()}>

            <Text style={{ textAlign: 'center', fontWeight: "bold", color: '#fff' }}>FECHAR</Text>

          </TouchableOpacity>
        </View>
      </View>


    </View>
  );
}