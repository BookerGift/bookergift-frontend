import React, {useState} from 'react';
import {
  View,
  Image,
  Text,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
} from 'react-native';

import AnimatedInput from 'react-native-animated-input';

//importando componente de validação de e-mail e goBack
import GoNextConfirmEmail from '../../Components/GoNextConfirmEmail';
import GoBack from '../../Components/GoBack';

//Importando a Estilização comum (Sem styled Components)
import styles from './styles';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Feather';

export default function Cadastro2({route, navigation}) {
  // Recebendo o phone por parâmetro de outra tela anterior
  const {phone} = route.params;

  // Declarando as constantes de estado
  const [myNome, setMyNome] = useState('');
  const [myEmail, setMyEmail] = useState('');
  const [myPassword, setMyPassword] = useState('');

  //Função serve para fazer a vilidação do campo nome
  const checkNome = () => {
    let nome_usuario = myNome;

    if (nome_usuario.length >= 3) {
      return <Icon name="check" size={25} color={'#1bba3a'} />;
    } else {
      return <Icon name="x" size={25} color={'gray'} />;
    }
  };

  //Função serve para fazer a vilidação do campo de E=mail
  const checkEmail = () => {
    let email_user = myEmail

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (reg.test(email_user) === true) {
      return <Icon name="check" size={25} color={'#1bba3a'} />;
    } else {
      return <Icon name="x" size={25} color={'gray'} />;
    }
  };

  const checkPassword = () => {
    let password_user = myPassword;

    if (password_user.length >= 8) {
      return <Icon name="check" size={25} color={'#1bba3a'} />;
    } else {
      return <Icon name="x" size={25} color={'gray'} />;
    }
  };

  //Esta função faz com o que tenha o controle do botão
  //de avançar para a próxima tela, logo, o mesmo só estará
  //habilitado quando o telefone só tiver 11 caracteres
  const controleGoNextEmail = () => {
    let nome_usuario = myNome;
    let email = myEmail;
    let senha = myPassword;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    
    if ((nome_usuario.length >= 3) && (reg.test(email) === true) && (senha.length >= 8)) {
      return (
        <GoNextConfirmEmail
          telefone={phone}
          nome={myNome}
          email={myEmail}
          senha={myPassword}
          controle={false}
          colorcontrole="#2cafd8"
        />
      );
    } else {
      return (
        <GoNextConfirmEmail
          telefone={phone}
          nome={myNome}
          email={myEmail}
          senha={myPassword}
          controle={true}
          colorcontrole="gray"
        />
      );
    }
  };

  return (
    <KeyboardAvoidingView style={styles.container}>
      <View style={styles.viewheadersms}>
        <View style={styles.gobackview}>
          <GoBack irParaTela="Cadastro" colorback="#fff" />
        </View>

        <View style={styles.viewtxtconfirmsms}>
          <View style={styles.confirmcodtxt}>
            <Text style={{color: '#fff', fontSize: 17}}>
              {' '}
              Falta só mais alguns dados !
            </Text>
          </View>
        </View>
      </View>

      {/* Área de cadastro do nome, E-mail e a confirmação do E-mail*/}
      <View style={styles.viewtxtcodigo}>
        <View style={styles.txtenviamos}>
          <Text style={{fontSize: 16, color: '#2b2b2b'}}>
            Agora, insira seu nome, e-mail e senha para se cadastrar:
          </Text>
        </View>

        {/* Input deNome com variavel de estado myNome*/}
        <View style={{width: '100%', flexDirection: 'row'}}>
          <View style={styles.iconname}>
            <Icon name="user-check" size={25} color={'#545454'} />
          </View>

          <TextInput
            style={styles.inputcel}
            placeholderTextColor="#545454"
            placeholder="Nome"
            onChangeText={(text) => setMyNome(text)}
            value={myNome}
          />
        </View>

        <View style={{width: '100%', flexDirection: 'row'}}>
          <View style={styles.iconname}>
            <Icon name="mail" size={25} color={'#545454'} />
          </View>
          {/* Input de E-mail com variavel de estado myEmail*/}
          <TextInput
            style={styles.inputcel}
            placeholderTextColor="#545454"
            placeholder="Seu e-mail"
            onChangeText={(text) => setMyEmail(text)}
            value={myEmail}
          />
        </View>

        <View style={{width: '100%', flexDirection: 'row'}}>
          <View style={styles.iconname}>
            <Icon name="lock" size={25} color={'#545454'} />
          </View>
          {/* Input de vonfirmar E-mail com variavel de estado myCinfirmEmail*/}
          <TextInput
            style={styles.inputcel}
            placeholderTextColor="#545454"
            placeholder="Digite uma senha"
            secureTextEntry={true}
            onChangeText={(text) => setMyPassword(text)}
            value={myPassword}
          />
        </View>

        <View style={styles.txtenviamos2}>
          {/* View dos checks referente à validação dos campos */}
          <View style={styles.viewchecks}>
            <View style={styles.check}>{checkNome()}</View>

            <View style={styles.check}>{checkEmail()}</View>

            <View style={styles.check}>{checkPassword()}</View>
          </View>

          {/* View das descrições dos checks referente à validação dos 
          campos */}
          <View style={styles.viewdescriptionchecks}>
            <View style={styles.check}>
              <Text style={{textAlign: 'center'}}>
                {' '}
                Seu nome deve ter no mínimo 3 letras.
              </Text>
            </View>

            <View style={styles.check}>
              <Text style={{textAlign: 'center'}}>
                {' '}
                O e-mail deve estar digitado corretamente.
              </Text>
            </View>

            <View style={styles.check}>
              <Text style={{textAlign: 'center'}}>
                {' '}
                Sua senha deve ter no mínimo 8 caracteres.
              </Text>
            </View>
          </View>
        </View>
      </View>

      <View style={styles.viewgo}>
          {controleGoNextEmail()}
      </View> 
    </KeyboardAvoidingView>
  );
}
