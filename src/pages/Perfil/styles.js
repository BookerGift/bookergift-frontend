import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    containerimg: {
        backgroundColor: '#55bede',
        flex: 1,
        //borderWidth: 2,
    },
    containerperfil: {
        //ackgroundColor: '#ff2',
        //position: 'absolute',
        flex: 1,
        //borderTopWidth: 1

    },
    boxperfiltxt: {
        width: wp('37%'),
        height: wp('10%'),
        alignSelf: 'center',
        //borderWidth: 1,
        marginTop: hp('20%')
    },
    containerinfoperfil: {
        borderTopRightRadius: 45,
        borderTopLeftRadius: 45,
        //borderWidth: 1,
        flex: 2,
        backgroundColor: '#f0f0f0',
        paddingTop: hp('4%')

    },
    boxinfouser: {
        width: '85%',
        //borderWidth: 2,
        height: '20%',
        alignSelf: 'center',
        flexDirection: 'row',
        marginBottom: hp('2%'),
    },
    boxinfonome1: {
        width: '85%',
        //borderWidth: 2,
        height: '20%',
        alignSelf: 'center',
        flexDirection: 'row',
        borderTopWidth: 0.7,
        borderColor: '#d9d9d9'
    },
    boxinfonome: {
        width: '85%',
        //borderWidth: 2,
        height: '20%',
        alignSelf: 'center',
        flexDirection: 'row',

    },
    boxloggout: {
        width: '85%',
        //borderWidth: 2,
        height: '10%',
        flexDirection: 'row',
        borderRadius: 10,
        backgroundColor: '#24a1d6',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: hp('1%')
    },
    topo: {
        width: wp('100%'),
        height: hp('15%'),
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 5,
        padding: wp('5%'),
    },
    viewphoto: {
        //borderWidth: 3,
        borderColor: 'gray',
        width: '25%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    imagecircle: {
        width: 70,
        height: 70,
        borderRadius: 70 / 2,
        backgroundColor: '#55bede',
        alignItems: 'center',
        justifyContent: 'center'

    },
    viewcirclesinfo: {
        width: 60,
        height: 60,
        borderRadius: 60 / 2,
        backgroundColor: '#55bede',
        alignItems: 'center',
        justifyContent: 'center'
    },
    viewcirclesinfo2: {
        width: 60,
        height: 60,
        borderRadius: 60 / 2,
        backgroundColor: '#d9d9d9',
        alignItems: 'center',
        justifyContent: 'center'
    },
    dadosperfil: {
        width: '60%',
        height: '100%',
        //borderWidth: 3,
        padding: 12,
        justifyContent: 'center'
    },
    dadosnome: {
        marginBottom: 5,
        //borderWidth: 1
    },
    viewopcoesperfil: {
        flex: 2,
        //borderWidth:3,
    },
    infopessoais: {
        width: '95%',
        height: '30%',
        //borderWidth:2,
        borderBottomWidth: 0.2,
        borderColor: 'gray',
        alignSelf: 'center',
        justifyContent: 'center',
        padding: 20
    },
    iconsettings: {
        //borderWidth: 1,
        width: '15%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    centeredView2: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(52, 52, 52, 0.8)'
    },
    modalView2: {
        width: wp('70%'),
        height: hp('20%'),
        //margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        //borderWidth: 1,
        padding: 25,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    buttonremover2: {
        width: '50%',
        height: '100%',
        //borderWidth: 1,
        borderRadius: 10,
        backgroundColor: '#f54842',
        justifyContent: 'center',
        alignItems: 'center',
        //right: wp('2%')
    },
    buttonpublicar2: {
        width: '50%',
        height: '100%',
        //borderWidth: 1,
        borderRadius: 10,
        backgroundColor: '#2ed936',
        justifyContent: 'center',
        alignItems: 'center',
        left: wp('3%')
    },

});

export default styles;
