import React, { useState, useEffect } from 'react';
import { View, Text, KeyboardAvoidingView, Image, TouchableOpacity, SnapshotViewIOS, Alert, ImageBackground, Modal } from 'react-native';

//Importando navegação do React Navigation para navegação entre telas
//import { useNavigation } from '@react-navigation/native';

import AsyncStorage from '@react-native-async-storage/async-storage';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

//importando o firebase
import firebase from '../../FirebaseConnection';

//Importando a Estilização StyleSheet
import styles from './styles';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';
import { isEnabled } from 'react-native/Libraries/Performance/Systrace';

export default function Perfil({ route }) {

    //Atribuindo a navegação para a constante navigantion, assim
    //a mesma tem acesso à nevegação entre as telas da aplicação
    const navigation = useNavigation();

    const [objetousuario, setMyObjetoUsuario] = useState('');
    const [nomeUsuario, setMyNomeUsuario] = useState('Carregando...');
    const [isVisible, setIsVisible] = useState(false);

    const getDataObject = async () => {
        try {
            const pegarobjeto = await AsyncStorage.getItem('@objectuser')

            setMyObjetoUsuario(JSON.parse(pegarobjeto));

        } catch (e) {
            Alert.alert(e);
        }
    }

    const getNomeUsuario = async () => {
        await firebase.database().ref(`usuario/${objetousuario.chave}/nome`).on('value', (snapshot) => {
            setMyNomeUsuario(snapshot.val());
        });
    }
    gotoperfilinfonome = () => {
        navigation.navigate('PerfilInfoNome', {
            screen: 'PerfilInfoNome',
        });
    }

    gotoConfiguracoes = () => {
        navigation.navigate('Configuracoes', {
            screen: 'Configuracoes',
        });
    }


    infoEdit = () => {
        Alert.alert("Desculpe, no momento esse dado não pode ser alterado.")
    }

    useEffect(() => {

        console.log(nomeUsuario);
        getDataObject();
        getNomeUsuario();
    }, [objetousuario.chave, nomeUsuario]);

    return (
        <ImageBackground source={require('../../assets/fundo_perfil_screen.png')} style={styles.containerimg}>

            <View style={styles.containerperfil}>
                <View style={styles.boxperfiltxt}>
                    <Text style={{ fontSize: 34, fontWeight: 'bold', color: '#fff' }}>Perfil</Text>
                </View>
            </View>

            <View style={styles.containerinfoperfil}>

                {/* View da foto e nome do usuário */}
                <View style={styles.boxinfouser}>

                    {/* View da foto */}
                    <View style={styles.viewphoto}>
                        <Image
                            style={styles.imagecircle}
                            source={require('../../assets/userphoto.png')}
                        />
                    </View>

                    {/* View info do nome e email do usuario*/}
                    <View style={styles.dadosperfil}>
                        <View style={styles.dadosnome}>
                            <Text style={{ fontSize: 17 }}>{nomeUsuario} </Text>
                        </View>
                        <Text>
                            <Text style={{ color: 'gray' }}>Usuário BookerGift</Text>
                        </Text>
                    </View>

                    {/* View do icon de settings (opções)*/}
                    <View style={styles.iconsettings}>
                        <TouchableOpacity onPress={() => gotoConfiguracoes()}>
                            <Icon name="settings-sharp" size={30} color={'#b2b2b2'} />
                        </TouchableOpacity>
                    </View>
                </View>

                {/* View da foto e nome do usuário (para editar)*/}
                <View style={styles.boxinfonome1}>
                    {/* View do icon user */}
                    <View style={styles.viewphoto}>
                        <View style={styles.viewcirclesinfo}>
                            <Icon name="person-sharp" size={25} color={'white'} />
                        </View>
                    </View>

                    {/* View info do nome e email do usuario*/}
                    <View style={styles.dadosperfil}>
                        <View style={styles.dadosnome}>
                            <Text style={{ fontSize: 17, fontWeight: 'bold' }}>Nome</Text>
                        </View>
                        <Text>
                            <Text style={{ color: 'gray' }}>{nomeUsuario}</Text>
                        </Text>
                    </View>

                    {/* View do icon de Edit (Edição)*/}
                    <View style={styles.iconsettings}>
                        <TouchableOpacity onPress={() => gotoperfilinfonome()}>
                            <Icon name="pencil-sharp" size={30} color={'#696969'} />
                        </TouchableOpacity>
                    </View>
                </View>

                {/* View do icon e dados do Email */}
                <View style={styles.boxinfonome}>
                    {/* View do icon user */}
                    <View style={styles.viewphoto}>
                        <View style={styles.viewcirclesinfo2}>
                            <Icon name="mail" size={25} color={'#b2b2b2'} />
                        </View>
                    </View>

                    {/* View info do nome e email do usuario*/}
                    <View style={styles.dadosperfil}>
                        <View style={styles.dadosnome}>
                            <Text style={{ fontSize: 17, fontWeight: 'bold' }}>E-mail</Text>
                        </View>
                        <Text>
                            <Text style={{ color: 'gray' }}>{objetousuario.email}</Text>
                        </Text>
                    </View>

                    {/* View do icon de Edit (Edição)*/}
                    <View style={styles.iconsettings}>
                        <TouchableOpacity onPress={() => infoEdit()}>
                            <Icon name="alert-circle-outline" size={35} color={'#55bede'} />
                        </TouchableOpacity>
                    </View>
                </View>

                {/* View do icon e dados do Telefone */}
                <View style={styles.boxinfonome}>
                    {/* View do icon user */}
                    <View style={styles.viewphoto}>
                        <View style={styles.viewcirclesinfo2}>
                            <Icon name="ios-call" size={25} color={'#b2b2b2'} />
                        </View>
                    </View>

                    {/* View info do nome e email do usuario*/}
                    <View style={styles.dadosperfil}>
                        <View style={styles.dadosnome}>
                            <Text style={{ fontSize: 17, fontWeight: 'bold' }}>Telefone</Text>
                        </View>
                        <Text>
                            <Text style={{ color: 'gray' }}>{objetousuario.telefone}</Text>
                        </Text>
                    </View>

                    {/* View do icon de Edit (Edição)*/}
                    <View style={styles.iconsettings}>
                        <TouchableOpacity onPress={() => infoEdit()}>
                            <Icon name="alert-circle-outline" size={35} color={'#55bede'} />
                        </TouchableOpacity>
                    </View>
                </View>

                {/* View do button logout*/}
                <TouchableOpacity style={styles.boxloggout} onPress={() => setIsVisible(!isVisible)}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#fff', letterSpacing: 3 }}>LOGGOUT</Text>
                </TouchableOpacity>
            </View>

            <Modal
                animationType="slide"
                transparent={true}
                visible={isVisible}
                onRequestClose={() => {
                    setIsVisible(!isVisible);
                }}
                style={{ width: '50%' }}
            >
                <View style={styles.centeredView2}>
                    <View style={styles.modalView2}>


                        <Text style={{ fontWeight: 'bold' }}>Deseja realmente deslogar da aplicação ?</Text>

                        <View style={{ flexDirection: 'row', marginTop: '10%' }}>
                            <TouchableOpacity style={styles.buttonremover2} onPress={() => setIsVisible(!isVisible)}>
                                <Text style={{ color: 'white', fontWeight: 'bold' }}>Cancelar</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.buttonpublicar2}>
                                <Text style={{ color: 'white', fontWeight: 'bold' }} onPress={() => loggout()}>Sair</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </Modal>
        </ImageBackground >
    );
}