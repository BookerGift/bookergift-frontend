import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
      container: {
        backgroundColor: 'black',
        flex:1 
      },
      image: {
        flex:1,
        resizeMode: "cover",
      },
      text: {
        color: "grey",
        fontSize: 30,
        fontWeight: "bold"
      },
      viewinformative21:{
        //borderWidth: 5,
        borderColor:'red',
        //alignSelf:'center',
        flex:1,
        //marginTop:hp('30%'),
        //alignItems:'center',
        //justifyContent:'center'
      },
      texttitulo:{
          color:'white',
          fontSize: 22,
          fontWeight:'bold',
          color:'#FFDA87'
      },
      viewinferior:{
        justifyContent:'center',
        //borderWidth:5,
        borderColor:'red',
        height:hp('10%'),
      },

      viewtxttitle:{
        //borderWidth:5,
        height:'15%',
        flexDirection:'row',
        paddingLeft:wp('5%'),
      },
      viewicontitle:{
        //borderWidth:5,
        justifyContent:'center',
      },
      viewtitle:{
        //borderWidth:5,
        justifyContent:'center',
        paddingLeft:wp('5%')
      },
      viewsubtitulo:{
        //borderWidth:5,
        height:'30%',
        paddingLeft:wp('5%'),  
        paddingRight:wp('5%')
    },
      textdescricao:{
        color:'white',
        fontSize:16,
        lineHeight: 30,
      }
});

export default styles;
