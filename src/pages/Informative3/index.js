import React from 'react'
import { View, Text, ImageBackground, Image } from 'react-native'

//Importando a Estilização comum (Sem styled Components)
import styles from './styles';

//Importando Componente da barra inferior
import Downbarinformative from '../../Components/Downbarinformative';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/MaterialIcons';


export default function Informative3() {
    return (
        <View style={styles.container}>

            <ImageBackground source={require('../../assets/fundo3.png')} style={styles.image}>

                {/* View de descrição inicial da aplicação */}
                <View style={styles.viewinformative21}>

                    {/* View geral do Título da informative Screen 3 */}
                    <View style={styles.viewtxttitle}>

                        <View style={styles.viewicontitle}>
                            <Icon name="fiber-manual-record" size={15} color={'#6bff84'} />
                        </View>

                        <View style={styles.viewtitle}>
                            <Text style={styles.texttitulo}>Doe seus livros !</Text>
                        </View>

                    </View>

                    {/* View geral da descrição do Título da informative Screen 3 */}
                    <View style={styles.viewsubtitulo}>
                        <Text style={styles.textdescricao}>Já pensou em doar seus livros que não utiliza para alguém que realmente precisa e não tem condiçoes de ter um?{'\n\n'} Doe e Ajude a disseminar o conhecimento !</Text>
                    </View>
                </View>

                {/* View Inferior principal de manuseio das informatives screens */}
                <View style={styles.viewinferior}>

                    {/*Componente da barra inferior da tela */}
                    <Downbarinformative c1='white' c2='white' c3='yellow' c4='white' screenName='Informative4' />

                </View>

            </ImageBackground>

        </View>
    );
}