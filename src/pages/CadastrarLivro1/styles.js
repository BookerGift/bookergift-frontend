import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  containerimg: {
    backgroundColor: '#55bede',
    flex: 1,
    //borderWidth: 2,
  },
  containercadlivro: {
    //ackgroundColor: '#ff2',
    //position: 'absolute',
    flex: 1,
    //borderTopWidth: 1

  },
  boxviewbackscreen: {
    width: wp('100%'),
    height: hp('10%'),
    //borderWidth: 1,
    position: 'absolute'
  },
  boxbackbutton: {
    width: '20%',
    height: '100%',
    //borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerinfocadlivro: {
    borderTopRightRadius: 45,
    borderTopLeftRadius: 45,
    //borderWidth: 1,
    flex: 2 / 1.8,
    backgroundColor: '#f0f0f0',
    paddingTop: hp('4%')

  },
  boxcadlivrotxt: {
    width: wp('80%'),
    alignSelf: 'center',
    //borderWidth: 1,
    marginTop: hp('10%')
  },
  boxcadlivrotxt2: {
    width: wp('80%'),
    alignSelf: 'center',
    //borderWidth: 1,
    marginTop: '20%'
  },
  boxtxtitlecad: {
    width: wp('80%'),
    //height: hp('5%'),
    //borderWidth: 1,
    alignSelf: 'center',

  },
  boxtxtinfoisbn: {
    width: wp('80%'),
    //height: hp('5%'),
    //borderWidth: 1,
    alignSelf: 'center',
    marginTop: hp('3%')
  },
  boxtxtinfoisbn2: {
    //width: wp('80%'),
    //height: hp('5%'),
    //borderWidth: 1,
    alignSelf: 'center',
    marginTop: hp('4%'),
    flexDirection: 'row'
  },
  viewinputisbn: {
    width: wp('80%'),
    height: hp('7%'),
    //borderWidth: 1,
    alignSelf: 'center',
    marginTop: hp('3%'),
    flexDirection: 'row'
  },
  viewinputisbn2: {
    width: '80%',
  },
  inputisbn: {
    width: '100%',
    height: '100%',
    borderWidth: 3,
    borderColor: '#55bede',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    paddingLeft: 20
  },
  viewserachicon: {
    width: '20%',
    height: '100%',
    //borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#55bede',
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    //borderColor: 'gray',
    //borderRadius: 10,
    //paddingLeft: 20
  },
  buttoninfoisbnmodal: {
    //borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10
  }

});

export default styles;
