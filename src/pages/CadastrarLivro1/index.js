import React, { useState, useEffect } from 'react';
import { View, Text, KeyboardAvoidingView, Image, TouchableOpacity, TextInput, SnapshotViewIOS, Alert, ImageBackground } from 'react-native';

//Importando Axios para comunicação com a API do ISBNdb
import axios from 'axios';

import AsyncStorage from '@react-native-async-storage/async-storage';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

//importando o firebase
import firebase from '../../FirebaseConnection';

//Importando a Estilização StyleSheet
import styles from './styles';

//importando componentes de GoBack e GoNext
import ModalInfoISBN from '../../Components/ModalInfoISBN';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

export default function CadastrarLivro1() {
  //Área de variáveis e functions

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  //Constando do código ISBN que o usuário irá digitar
  const [codIsbn, setMyCodIsbn] = useState('');

  const txtisbn = 'Com o código ISBN, podemos rastrear todas as informações do seu livro que deseja cadastrar, o que te poupa todo o trabalho de digitar dado por dado do livro ! Mas lembre-se, sem esse código não podemos cadastra-lo e nem todo livro possui um código ISBN rastreável !'

  // Const da chave de acesso para a AP
  const apikey = '45781_fc9f95ae66896e97945a850755c0193c'

  const [title, setTitle] = useState('');
  const [author, setAuthor] = useState('');
  const [publisher, setPublisher] = useState('');
  const [language, setLanguage] = useState('');
  const [image, setImage] = useState('');
  const [datepublished, setDatePublished] = useState('');
  const [objetousuario, setMyObjetoUsuario] = useState('');

  //Objeto livro que recebe todos os dados do data request
  //da API ISBNdb recebido pelo GET do isbn digitado
  const livroobject = {
    title: title,
    author: author,
    publisher: publisher,
    language: language,
    image: image,
    datepublished: datepublished,
    isbn: codIsbn,
    iduser: objetousuario.chave,
    publicado: false
  };

  const getDataObject = async () => {
    try {
      const pegarobjeto = await AsyncStorage.getItem('@objectuser')

      setMyObjetoUsuario(JSON.parse(pegarobjeto));

      if (objetousuario === null) {
        chaveusuario = ' DEU MERDA LEKAO '
      }

    } catch (e) {
      Alert.alert(e);
    }
  }

  //Função que guarda todos os dados do livro no asyncstorage para assim
  //após ser possível pegar os dados do livro para dar tratamento do mesmo.
  const livroStorage = async () => {

    //Try catch para guardar objeto com dados do livro
    //no async storage
    try {

      await AsyncStorage.setItem('@objectlivro', JSON.stringify(livroobject));

      console.log(JSON.stringify(livroobject));

    } catch (e) {

      alert(e);

    }

  }

  //Função que faz a requisição na API com o isbn digitado e retorna o data
  //do livro em caso de sucesso guaradando no objeto livroobject
  const requestIsbnDB = async () => {
    try {
      const response = await axios.get(`https://api2.isbndb.com/book/${codIsbn}`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `${apikey}`
        }
      });

      setTitle(response.data.book.title);
      setAuthor(response.data.book.authors);
      setPublisher(response.data.book.publisher);

      if (response.data.book.language) {
        setLanguage(response.data.book.language);
      } else {
        setLanguage('--');
      }

      setImage(response.data.book.image);
      setDatePublished(response.data.book.date_published);

      livroStorage();
      gotoInfoLivro();

      // alert(JSON.stringify(response.data));
    } catch (error) {
      if (error.response) {
        alert(error.response.data);
      }
      alert(error);
    }
  }

  //Função que realiza o direcionamento para a tema de Confirm livro, para
  //os dados da requisição são pegados com sucesso.
  const gotoInfoLivro = () => {
    navigation.navigate('ConfirmLivro', {
      screen: 'ConfirmLivro',
    });

    setMyCodIsbn('');
  }

  const abrirmodal = () => {
    navigation.navigate('ModalInfoISBN', {
      screen: 'ModalInfoISBN',
    });
  }

  const gotoBiblioteca = () => {
    navigation.navigate('Biblioteca', {
      screen: 'Biblioteca',
    });
  }

  const zerarInput = () => {
    setMyCodIsbn('');
  }

  useEffect(() => {

    livroStorage();
    getDataObject();
    console.log(objetousuario.chave);
  }, [livroobject.datepublished]);

  return (

    <ImageBackground source={require('../../assets/fundo_sccren_cadastro_livro.png')} style={styles.containerimg} >

      <ModalInfoISBN visible={true}></ModalInfoISBN>

      {/* Container do header e títulos da tela de Cadastrar Livro */}
      <View style={styles.containercadlivro}>

        {/* View do button de voltar tela (Back Button) */}
        <View style={styles.boxviewbackscreen}>
          <View style={styles.boxbackbutton}>
            <TouchableOpacity onPress={() => gotoBiblioteca()}>
              <Icon name="ios-arrow-back-outline" color="white" size={40} />
            </TouchableOpacity>
          </View>
        </View>

        {/* View do TXT título "Cadastrar Livro" */}
        <View style={styles.boxcadlivrotxt}>
          <Text style={{ fontSize: 34, fontWeight: 'bold', color: '#fff' }}>Cadastrar</Text>
          <Text style={{ fontSize: 34, fontWeight: 'bold', color: '#fff' }}>Livro</Text>
        </View>

        {/* View do Subtítulo "1º Etapa, Vamos começar com o ISBN" */}
        <View style={styles.boxcadlivrotxt2}>
          <Text style={{ fontSize: 26, fontWeight: 'bold', color: '#ff2' }}>Cadastro por ISBN</Text>
          <Text style={{ fontSize: 18, color: '#fff' }}>Vamos começar com o ISBN !</Text>
        </View>

      </View>

      {/* Container do input do ISBN e conteúdo da tela de Cadastrar Livro */}
      <View style={styles.containerinfocadlivro}>

        <View style={styles.boxtxtitlecad}>
          <Text style={{ fontSize: 16 }}>Insira o código ISBN do livro a cadastrar:</Text>
        </View>

        {/* View principal do Input do ISBN */}
        <View style={styles.viewinputisbn}>

          <View style={styles.viewinputisbn2}>
            <TextInput style={styles.inputisbn}
              keyboardType={'number-pad'}
              maxLength={13}
              onChangeText={text => setMyCodIsbn(text)}
              placeholder={'Digite o código ISBN'}
              editable={true}
              autoFocus={false}
              value={codIsbn}
            />
          </View>

          <TouchableOpacity style={styles.viewserachicon}
            onPress={() => requestIsbnDB()}
          >

            <Icon name="search" color="white" size={30} />

          </TouchableOpacity>

        </View>

        <View style={styles.boxtxtinfoisbn}>
          <Text style={{ fontSize: 15, lineHeight: 24 }}>
            {txtisbn}
          </Text>
        </View>

        <View style={styles.boxtxtinfoisbn2}>
          <Icon name="md-information-circle-sharp" color="#e0b94c" size={30} />
          <TouchableOpacity style={styles.buttoninfoisbnmodal} onPress={() => abrirmodal()}>
            <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'gray' }}>O que é o ISBN e como usar-lo? </Text>
          </TouchableOpacity>
        </View>

      </View>

    </ImageBackground >
  );
}