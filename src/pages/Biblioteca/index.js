import React, { useState, useEffect } from 'react';
import { View, Text, KeyboardAvoidingView, Image, TouchableOpacity, SnapshotViewIOS, ScrollView, Modal } from 'react-native';
//importando o firebase
import firebase from '../../FirebaseConnection';

//Importando a Estilização StyleSheet
import styles from './styles';

//Importação da biblioteca de icons do react-native-vector-icons
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import ListarLivrosUser from '../../Components/ListarLivrosUser';

export default function Biblioteca() {

    //Atribuindo a navegação para a constante navigantion, assim
    //a mesma tem acesso à nevegação entre as telas da aplicação
    const navigation = useNavigation();

    const [objetousuario, setMyObjetoUsuario] = useState('');
    const [qtdLivros, setMyQtdLivros] = useState('Carregando...');
    const [qtdPublicacoes, setMyQtdPublicacoes] = useState('Carregando...');
    const [nomeUser, setMyNomeUser] = useState('Carregando...');
    const [livro, setLivro] = useState([]);
    const [imgLivro, setImgLivro] = useState('');
    const [publicacoes, setPublicacoes] = useState([]);
    const [arrayListLivro, setArrayListLivro] = useState('');
    const [arrayListPublicacao, setArrayListPublicacao] = useState('');
    const [arrayDoacoes, setArrayDoacoes] = useState([]);
    const [isVisible, setIsVisible] = useState(false);
    const [arrayDoador, setArrayDoador] = useState([]);
    const [arrayReceptor, setArrayReceptor] = useState([]);

    const getDataObject = async () => {
        try {
            const pegarobjeto = await AsyncStorage.getItem('@objectuser')

            setMyObjetoUsuario(JSON.parse(pegarobjeto));

            if (objetousuario === null) {
                chaveusuario = ' DEU MERDA LEKAO '
            }

        } catch (e) {
            Alert.alert(e);
        }
    }

    const getNomeUsuario = async () => {
        await firebase.database().ref(`usuario/${objetousuario.chave}/nome`).on('value', (snapshot) => {
            setMyNomeUser(snapshot.val());
        });
    }

    //request para pegar qtd de livros do usuario
    async function getQtdLivros() {
        await firebase.database().ref(`usuario/${objetousuario.chave}/livros_cadastrados`).on('value', (snapshot) => {
            return setMyQtdLivros(snapshot.val());
        });
    }

    //request para pegar qtd de publicações do usuário
    async function getQtdPublicacoes() {
        await firebase.database().ref(`usuario/${objetousuario.chave}/publicacoes`).on('value', (snapshot) => {
            return setMyQtdPublicacoes(snapshot.val());
        });
    }

    const gotoCadastroLivro = () => {
        navigation.navigate('CadastrarLivro1', {
            screen: 'CadastrarLivro1',
        });
    }

    const goToCadastroPublicacao = () => {
        navigation.navigate('CadastroPublicacao', {
            screen: 'CadastroPublicacao',
        });
    }

    const getDataPublicacao = async () => {
        await firebase.database().ref('publicacao').on('value', (snapshot) => {

            setPublicacoes([]);

            snapshot.forEach((chilItem) => {

                const publicacaodata = {
                    key: chilItem.key,
                    titulo: chilItem.val().titulo,
                    descricao: chilItem.val().descricao,
                    iduser: chilItem.val().usuario.iduser,
                    nomeusuario: chilItem.val().usuario.nome,
                    telefoneusuario: chilItem.val().usuario.telefone,
                    emailusuario: chilItem.val().usuario.email,
                    idlivro: chilItem.val().livro.idlivro,
                    imagem: chilItem.val().livro.imagem,
                    nomelivro: chilItem.val().livro.titulo,
                    editoralivro: chilItem.val().livro.editora,
                    datapublicacao: chilItem.val().datapublicacao,
                    status: chilItem.val().status,
                };

                if (objetousuario.chave == publicacaodata.iduser) {

                    setPublicacoes(oldArray => [...oldArray, publicacaodata]);
                }

            });
        })
    }

    const getDataDoacao = async () => {
        await firebase.database().ref('doacao').on('value', (snapshot) => {

            setArrayDoacoes([]);
            setArrayReceptor([]);
            setArrayDoador([]);

            snapshot.forEach((chilItem) => {

                let doacaodata = {
                    key: chilItem.key,
                    datadoacao: chilItem.val().datadoacao,
                    iddoador: chilItem.val().doador.iduser,
                    emaildoador: chilItem.val().doador.email,
                    nomedoador: chilItem.val().doador.nome,
                    telefonedoador: chilItem.val().doador.telefone,
                    idlivrodoado: chilItem.val().livro.idlivro,
                    livroeditora: chilItem.val().livro.editora,
                    livroimagem: chilItem.val().livro.imagem,
                    livrotitulo: chilItem.val().livro.titulo,
                    livroisbn: chilItem.val().livro.isbn,
                    livroautor: chilItem.val().livro.autor,
                    notificacaoemail: chilItem.val().notificacaoemail,
                    idpublicacao: chilItem.val().publicacao.idpublicacao,
                    datapublicacao: chilItem.val().publicacao.datapublicacao,
                    idreceptor: chilItem.val().receptor.iduser,
                    emailreceptor: chilItem.val().receptor.email,
                    nomereceptor: chilItem.val().receptor.nome,
                    telefonereceptor: chilItem.val().receptor.telefone,
                    statusdoacao: chilItem.val().statusdoacao,
                };

                if (objetousuario.chave == doacaodata.iddoador) {

                    setArrayDoador(oldArray => [...oldArray, doacaodata]);
                } else {
                    setArrayReceptor(oldArray => [...oldArray, doacaodata]);
                }

            });
        })
    }

    const getDataLivros = async () => {
        await firebase.database().ref('livro').on('value', (snapshot) => {

            setLivro([]);

            snapshot.forEach((chilItem) => {
                let livrodata = {
                    key: chilItem.key,
                    autor: chilItem.val().autor,
                    imagem: chilItem.val().imagem,
                    titulo: chilItem.val().titulo,
                    iduser: chilItem.val().usuario.iduser,
                    publicado: chilItem.val().publicado,
                };

                if (objetousuario.chave == livrodata.iduser) {
                    setLivro(oldArray => [...oldArray, livrodata]);
                }

            });
        })
    }

    const buscarLivro = async (chave) => {

        await firebase.database().ref(`livro/${chave}`).once('value', (snapshot) => {

            if (snapshot.val().publicado == true) {
                setArrayListLivro({
                    key: snapshot.key,
                    titulo: snapshot.val().titulo,
                    autor: snapshot.val().autor,
                    editora: snapshot.val().editora,
                    imagem: snapshot.val().imagem,
                    linguagem: snapshot.val().linguagem,
                    datapublicado: snapshot.val().datapublicado,
                    isbn: snapshot.val().isbn,
                    publicado: snapshot.val().publicado,
                    idpublicacao: snapshot.val().publicacao.idpublicacao,
                });

            } else {
                setArrayListLivro({
                    key: snapshot.key,
                    titulo: snapshot.val().titulo,
                    autor: snapshot.val().autor,
                    editora: snapshot.val().editora,
                    imagem: snapshot.val().imagem,
                    linguagem: snapshot.val().linguagem,
                    datapublicado: snapshot.val().datapublicado,
                    isbn: snapshot.val().isbn,
                    publicado: snapshot.val().publicado,
                });
            }

        });

        navigation.navigate('MostrarLivroBiblioteca', {
            screen: 'MostrarLivroBiblioteca',
        });
    }

    const buscarPublicacao = async (chavepublicacao) => {

        await firebase.database().ref(`publicacao/${chavepublicacao}`).once('value', (snapshot) => {

            setArrayListPublicacao({
                key: snapshot.key,
                titulo: snapshot.val().titulo,
                descricao: snapshot.val().descricao,
                datapublicacao: snapshot.val().datapublicacao,
                status: snapshot.val().status,
                iduser: snapshot.val().usuario.iduser,
                nomeusuario: snapshot.val().usuario.nome,
                emailusuario: snapshot.val().usuario.email,
                telefoneusuario: snapshot.val().usuario.telefone,
                idlivro: snapshot.val().livro.idlivro,
                titulolivro: snapshot.val().livro.titulo,
                editoralivro: snapshot.val().livro.editora,
                imagem: snapshot.val().livro.imagem,

            });

        });

        navigation.navigate('EditarPublicacao', {
            screen: 'EditarPublicacao',
        });
    }

    const LivroStorage = async () => {

        //Try catch para guardar objeto com dados do livro
        //no async storage
        try {

            await AsyncStorage.setItem('@objectmostrarlivro', JSON.stringify(arrayListLivro));

        } catch (e) {

            alert(e);

        }

    }

    const publicacaoStorage = async () => {

        //Try catch para guardar objeto com dados do livro
        //no async storage
        try {

            await AsyncStorage.setItem('@objectpublicacao', JSON.stringify(arrayListPublicacao));

        } catch (e) {

            alert(e);

        }

    }

    //Renderizar "Livro Já Publicado" na tela de BIBLIOTECA quando na listagem
    //dos livros tenha algum livro que já esteja publicado
    const renderPublicado = (boleano) => {
        if (boleano) {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <Icon name="check-circle" color="#11bd2e" size={25} />
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ color: "#11bd2e", fontWeight: 'bold' }}>Livro já Publicado !</Text>
                    </View>
                </View>
            )
        }
    }

    const renderLivroBiblioteca = (qtdlivros) => {

        if (qtdlivros == 0) {
            return (

                <View style={styles.boxofflivro}>
                    <View>
                        {/* VIEW do ICON da carinha triste */}
                        <View style={{ height: '40%', width: '100%', alignItems: 'center', justifyContent: 'flex-end' }}>
                            <Icon name="emoticon-sad-outline" color="#c8c8c8" size={50} />
                        </View>

                        {/* VIEW do TXT: Ops não há nenhum livro aqui ! */}
                        <View style={{ justifyContent: 'center', height: '20%', width: '100%', alignItems: 'center' }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 14, color: '#c3c3c3' }}>
                                OPS, Não há nenhum livro por aqui !
                            </Text>
                        </View>

                        {/* VIEW do botão cadastrar um livro  */}
                        <View style={{ height: '30%', justifyContent: 'center' }}>
                            <TouchableOpacity style={styles.cadumlivro}>
                                <Text style={{ fontWeight: 'bold', color: '#c3c3c3' }}>Cadastrar um livro !</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View >
            );
        } else {
            if (qtdlivros > 0) {
                return (

                    <View style={{ flexDirection: 'row' }}>
                        {livro.map(datalivro => (

                            <TouchableOpacity style={{
                                width: wp('40%'),
                                height: hp('30%'),
                                borderRadius: 10,
                                marginRight: wp('15%'),
                                marginTop: hp('2%')
                                //alignSelf: 'center',

                            }} onPress={() => buscarLivro(datalivro.key)}>

                                <Image style={{
                                    width: '100%',
                                    height: '100%',
                                    resizeMode: 'contain',
                                }} source={{ uri: datalivro.imagem }} />


                                <Text numberOfLines={2} style={{ fontWeight: 'bold', textAlign: 'center', marginTop: '5%' }}>{datalivro.titulo}</Text>
                                {renderPublicado(datalivro.publicado)}
                            </TouchableOpacity>
                        ))}
                    </View>

                );
            }

        }
    }

    const renderPublicacoesBiblioteca = (qtdpublicacoes) => {

        if (qtdpublicacoes == 0) {
            return (

                <View style={styles.boxoffpublicacao}>
                    <View>
                        {/* VIEW do ICON da carinha triste */}
                        <View style={{ height: '50%', width: '100%', alignItems: 'center', justifyContent: 'flex-end' }}>
                            <Icon name="emoticon-sad-outline" color="#c8c8c8" size={45} />
                        </View>

                        {/* VIEW do TXT: Ops não há nenhum livro aqui ! */}
                        <View style={{ justifyContent: 'center', height: '35%', width: '100%', alignItems: 'center' }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 14, color: '#c3c3c3' }}>
                                OPS, Não há Publicações por aqui !
                            </Text>

                        </View>
                    </View>
                </View >
            );
        } else {
            if (qtdpublicacoes > 0) {
                return (

                    <View style={{ flexDirection: 'row' }}>
                        {publicacoes.map(datapublicacao => (

                            <View style={{
                                width: wp('65%'),
                                height: hp('20%'),
                                borderRadius: 10,
                                marginRight: wp('12%'),
                                marginTop: hp('3%'),
                                //borderWidth: 1,
                                backgroundColor: '#a2a2a2',
                                flexDirection: 'row'
                                //alignSelf: 'center',

                            }}>



                                <TouchableOpacity style={{
                                    borderWidth: 2,
                                    width: wp('65%'),
                                    borderColor: '#55bede',
                                    height: hp('20%'),
                                    borderRadius: 10,

                                }}
                                    onPress={() => buscarPublicacao(datapublicacao.key)}>
                                    <Image style={{
                                        width: '100%',
                                        height: '100%',
                                        resizeMode: 'cover',
                                        opacity: 0.5,
                                        borderRadius: 10,
                                    }} source={{ uri: datapublicacao.imagem }} />
                                    <View style={{ position: 'absolute', width: '26%', height: '40%', paddingLeft: 20, justifyContent: 'center', alignSelf: 'flex-end' }}>
                                        <Icon name="comment-edit" color="#5c5c5c" size={25} />
                                    </View>

                                </TouchableOpacity>
                            </View>
                        ))
                        }
                    </View >

                );
            }

        }
    }

    const renderAdocoesBiblioteca = (qtdAdocoes) => {

        if (qtdAdocoes == 0) {
            return (

                <View style={styles.boxoffpublicacao}>
                    <View>
                        {/* VIEW do ICON da carinha triste */}
                        <View style={{ height: '50%', width: '100%', alignItems: 'center', justifyContent: 'flex-end' }}>
                            <Icon name="emoticon-sad-outline" color="#c8c8c8" size={45} />
                        </View>

                        {/* VIEW do TXT: Ops não há nenhum livro aqui ! */}
                        <View style={{ justifyContent: 'center', height: '35%', width: '100%', alignItems: 'center' }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 14, color: '#c3c3c3' }}>
                                OPS, Não há Adoções por aqui !
                            </Text>

                        </View>
                    </View>
                </View >
            );
        } else {
            if (qtdAdocoes > 0) {
                return (

                    <View style={{ flexDirection: 'row', marginTop: hp('3%') }}>
                        {arrayReceptor.map(adocao => (

                            <View style={{
                                width: wp('60%'),
                                height: hp('35%'),
                                borderRadius: 10,
                                marginRight: wp('12%'),
                                //marginTop: hp('3%'),
                                //borderWidth: 1,
                                backgroundColor: '#fff',
                                flexDirection: 'row',
                                //alignSelf: 'center',
                                shadowColor: "#c8c8c8",
                                shadowOffset: {
                                    width: 0,
                                    height: 1
                                },
                                shadowOpacity: 0.5,
                                shadowRadius: 2,
                                elevation: 5,

                            }}>



                                <TouchableOpacity style={{
                                    //borderWidth: 2,
                                    width: '100%',
                                    //borderColor: '#55bede',
                                    height: '100%',
                                    borderRadius: 10,
                                }}>

                                    <View style={{ width: '100%', height: '40%' }}>

                                        <Image style={{
                                            width: '100%',
                                            height: '100%',
                                            resizeMode: 'cover',
                                            //opacity: 0.5,
                                            borderTopLeftRadius: 10,
                                            borderTopRightRadius: 10,
                                        }} source={{ uri: adocao.livroimagem }} />
                                    </View>

                                    <View style={{ width: '100%', height: '35%', paddingLeft: 20, paddingRight: 15, justifyContent: 'center' }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#55bede' }}>Livro Recebido</Text>
                                        <Text numberOfLines={2}>{adocao.livrotitulo}</Text>
                                    </View>

                                    <View style={{ width: '100%', height: '25%', flexDirection: 'row' }}>
                                        <View style={{ width: '45%', height: '100%', paddingLeft: 20 }}>
                                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Doador</Text>
                                            <Text>{adocao.nomedoador}</Text>
                                        </View>

                                        <View style={{ width: '55%', height: '100%', paddingLeft: 15 }}>
                                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Data Adoção</Text>
                                            <Text>{adocao.datadoacao}</Text>
                                        </View>
                                    </View>

                                    <View style={{ position: 'absolute', width: '20%', height: '100%', alignSelf: 'flex-end', top: -hp('1%'), right: wp('-6%'), justifyContent: 'flex-end' }}>
                                        <Icon name="check-circle" color="#2eb329" size={40} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        ))
                        }
                    </View >

                );
            }

        }
    }

    const renderDoacoesBiblioteca = (qtddoacoes) => {

        if (qtddoacoes == 0) {
            return (

                <View style={styles.boxoffpublicacao}>
                    <View>
                        {/* VIEW do ICON da carinha triste */}
                        <View style={{ height: '50%', width: '100%', alignItems: 'center', justifyContent: 'flex-end' }}>
                            <Icon name="emoticon-sad-outline" color="#c8c8c8" size={45} />
                        </View>

                        {/* VIEW do TXT: Ops não há nenhum livro aqui ! */}
                        <View style={{ justifyContent: 'center', height: '35%', width: '100%', alignItems: 'center' }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 14, color: '#c3c3c3' }}>
                                OPS, Não há Doações por aqui !
                            </Text>

                        </View>
                    </View>
                </View >
            );
        } else {
            if (qtddoacoes > 0) {
                return (

                    <View style={{ flexDirection: 'row', marginTop: hp('3%') }}>
                        {arrayDoador.map(doacao => (

                            <View style={{
                                width: wp('60%'),
                                height: hp('35%'),
                                borderRadius: 10,
                                marginRight: wp('12%'),
                                //marginTop: hp('3%'),
                                //borderWidth: 1,
                                backgroundColor: '#fff',
                                flexDirection: 'row',
                                //alignSelf: 'center',
                                shadowColor: "#c8c8c8",
                                shadowOffset: {
                                    width: 0,
                                    height: 1
                                },
                                shadowOpacity: 0.5,
                                shadowRadius: 2,
                                elevation: 5,

                            }}>



                                <TouchableOpacity style={{
                                    //borderWidth: 2,
                                    width: '100%',
                                    //borderColor: '#55bede',
                                    height: '100%',
                                    borderRadius: 10,
                                }}>

                                    <View style={{ width: '100%', height: '40%' }}>

                                        <Image style={{
                                            width: '100%',
                                            height: '100%',
                                            resizeMode: 'cover',
                                            //opacity: 0.5,
                                            borderTopLeftRadius: 10,
                                            borderTopRightRadius: 10,
                                        }} source={{ uri: doacao.livroimagem }} />
                                    </View>

                                    <View style={{ width: '100%', height: '35%', paddingLeft: 20, paddingRight: 15, justifyContent: 'center' }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#55bede' }}>Livro Doado</Text>
                                        <Text numberOfLines={2}>{doacao.livrotitulo}</Text>
                                    </View>

                                    <View style={{ width: '100%', height: '25%', flexDirection: 'row' }}>
                                        <View style={{ width: '45%', height: '100%', paddingLeft: 20 }}>
                                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Receptor</Text>
                                            <Text>{doacao.nomereceptor}</Text>
                                        </View>

                                        <View style={{ width: '55%', height: '100%', paddingLeft: 15 }}>
                                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Data Doação</Text>
                                            <Text>{doacao.datadoacao}</Text>
                                        </View>
                                    </View>

                                    <View style={{ position: 'absolute', width: '20%', height: '100%', alignSelf: 'flex-end', top: -hp('1%'), right: wp('-6%'), justifyContent: 'flex-end' }}>
                                        <Icon name="check-circle" color="#2eb329" size={40} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        ))
                        }
                    </View >

                );
            }

        }
    }

    const loggout = () => {
        setIsVisible(!isVisible);

        navigation.navigate('Login', {
            screen: 'Login'
        });
    }

    useEffect(() => {

        getDataObject(); //pegando o objeto livro
        getNomeUsuario(); //Pegando o nome do usuário
        getQtdLivros(); //olhando pro banco (on) para pegar a qtd de livros cadastrados do usuario
        getQtdPublicacoes(); //olhando pro banco (on) para pegar a qtd de publicações do usuario

        getDataLivros();
        getDataPublicacao();
        getDataDoacao();

    }, [livro.length, qtdLivros]);

    useEffect(() => {
        LivroStorage();
        //console.log(arrayListLivro);

    }, [arrayListLivro]);

    useEffect(() => {
        publicacaoStorage();
        //console.log(arrayListLivro);

    }, [arrayListPublicacao]);

    return (

        <ScrollView style={{ width: '100%', height: '100%' }}>

            <View style={{ overflow: 'hidden', paddingBottom: 5 }}>
                {/* View topo onde ficará as informações do perfil do usuário */}
                <View style={styles.topo}>


                    <View style={{ flexDirection: 'row' }}>
                        <View style={styles.viewphoto}>
                            <Image
                                style={styles.imagecircle}
                                source={require('../../assets/userphoto.png')}
                            />
                        </View>

                        <View style={styles.dadosperfil}>
                            <View style={styles.dadosnome}>
                                <Text style={{ fontSize: 17 }}>{nomeUser} </Text>
                            </View>

                        </View>
                        <View style={{ alignSelf: 'center' }}>

                            <TouchableOpacity onPress={() => setIsVisible(!isVisible)}><Text>Sair</Text></TouchableOpacity>

                        </View>
                    </View>


                </View>
            </View>

            {/* Box do "Olá, usuário" */}
            <View style={styles.boxolauser}>
                <Text style={{ fontSize: 26 }}>Olá, <Text style={{ fontSize: 22, fontWeight: 'bold' }}>{nomeUser}</Text></Text>
            </View>

            {/* Box do "Meus Livros" */}
            <View style={styles.boxmeuslivros}>
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ fontSize: 18 }}>Meus Livros <Text style={{ fontSize: 18, fontWeight: 'bold' }}>({livro.length})</Text></Text>
                </View>
                <TouchableOpacity style={{ justifyContent: 'center' }} onPress={() => gotoCadastroLivro()}>
                    <Icon name="plus-circle" color="#2eb329" size={35} />
                </TouchableOpacity>
            </View>


            {/* Box de Licros do "Meus Livros" */}
            <ScrollView horizontal={true} style={styles.boxscrolview}>

                {renderLivroBiblioteca(livro.length)}

            </ScrollView>

            {/* Box do "Minhas Publicações" */}
            <View style={styles.boxmeuslivros}>
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ fontSize: 18 }}>Minhas Publicações<Text style={{ fontSize: 18, fontWeight: 'bold' }}>({publicacoes.length})</Text></Text>
                </View>
                <TouchableOpacity style={{ justifyContent: 'center' }} onPress={() => goToCadastroPublicacao()}>
                    <Icon name="plus-circle" color="#2eb329" size={35} />
                </TouchableOpacity>
            </View>

            {/* Box de Publicações de "Minhas Publicações" */}
            <ScrollView horizontal={true} style={styles.boxscrolview2}>
                <View>
                    {renderPublicacoesBiblioteca(publicacoes.length)}
                </View>
            </ScrollView>

            {/* Box do "Minhas Publicações" */}
            <View style={styles.boxmeuslivros}>
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ fontSize: 18 }}>Minhas Doações<Text style={{ fontSize: 18, fontWeight: 'bold' }}>({arrayDoador.length})</Text></Text>
                </View>
            </View>

            {/* Box de Publicações de "Minhas Publicações" */}
            <ScrollView horizontal={true} style={styles.boxscrolview3}>
                <View>
                    {renderDoacoesBiblioteca(arrayDoador.length)}
                </View>
            </ScrollView>


            {/* Box do "Minhas Publicações" */}
            <View style={{ ...styles.boxmeuslivros, marginTop: hp('3%') }}>
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ fontSize: 18 }}>Minhas Adoções<Text style={{ fontSize: 18, fontWeight: 'bold' }}>({arrayReceptor.length})</Text></Text>
                </View>
            </View>

            {/* Box de Publicações de "Minhas Publicações" */}
            <ScrollView horizontal={true} style={{ ...styles.boxscrolview3, marginBottom: wp('15%') }}>
                <View>
                    {renderAdocoesBiblioteca(arrayReceptor.length)}
                </View>
            </ScrollView>

            <Modal
                animationType="slide"
                transparent={true}
                visible={isVisible}
                onRequestClose={() => {
                    setIsVisible(!isVisible);
                }}
                style={{ width: '50%' }}
            >
                <View style={styles.centeredView2}>
                    <View style={styles.modalView2}>


                        <Text style={{ fontWeight: 'bold' }}>Deseja realmente deslogar da aplicação ?</Text>

                        <View style={{ flexDirection: 'row', marginTop: '10%' }}>
                            <TouchableOpacity style={styles.buttonremover2} onPress={() => setIsVisible(!isVisible)}>
                                <Text style={{ color: 'white', fontWeight: 'bold' }}>Cancelar</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.buttonpublicar2}>
                                <Text style={{ color: 'white', fontWeight: 'bold' }} onPress={() => loggout()}>Sair</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </Modal>
        </ScrollView>
    );
}