import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    //borderWidth:1
  },
  topo: {
    width: wp('100%'),
    height: hp('15%'),
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    elevation: 5,
    //padding: wp('5%'),
    justifyContent: 'center'
  },
  viewphoto: {
    //borderWidth: 3,
    borderColor: 'gray',
    width: '15%',
    height: '100%',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  imagecircle: {
    width: '24%',
    height: 70,
    borderRadius: 70 / 2,
    backgroundColor: '#55bede',

  },
  dadosperfil: {
    width: '75%',
    height: '100%',
    //borderWidth: 3,
    justifyContent: 'center',
    paddingLeft: wp('10%')
  },
  dadosnome: {
    //borderWidth:3,
  },
  viewopcoesperfil: {
    flex: 2,
    //borderWidth:3,
  },
  infopessoais: {
    width: '95%',
    height: '30%',
    //borderWidth:2,
    borderBottomWidth: 0.2,
    borderColor: 'gray',
    alignSelf: 'center',
    justifyContent: 'center',
    padding: 20
  },
  inputsenha2: {
    width: '100%',
    paddingLeft: 15,
    height: hp('7%'),
    color: 'gray',
    //opacity:0.2,
    marginTop: hp('2%'),
    //color:'#fff',
    //height:hp('7%'),
    //paddingLeft:15,
    borderWidth: 1,
    //justifyContent:'center',
    borderRadius: 5,
    borderColor: 'gray'
  },
  inputsenha3: {
    width: '100%',
    paddingLeft: 15,
    height: hp('7%'),
    color: 'black',
    //opacity:0.2,
    marginTop: hp('2%'),
    //color:'#fff',
    //height:hp('7%'),
    //paddingLeft:15,
    borderWidth: 1,
    //justifyContent:'center',
    borderRadius: 5,
    borderColor: '#55bede'
  },
  viewinputname: {
    //borderWidth: 2,
    width: wp('85%'),
    //justifyContent:'center',

    alignSelf: 'center',
    height: hp('16%'),
    borderBottomWidth: 0.5,
    borderColor: '#d2d2d2'

  },
  cardinfonome: {
    width: wp('85%'),
    height: hp('23%'),
    //borderWidth: 2,
    alignSelf: 'center',
    marginTop: hp('4%'),
    backgroundColor: '#fff',
    padding: 20,
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    elevation: 5,
  },
  iconinfo: {
    // borderWidth: 1,
    width: '20%',
    height: '40%',
    justifyContent: 'center',

  },
  cautioninfonome: {
    width: '100%',
    height: '60%',
    //borderWidth: 1
  },
  viewinputname2: {
    //borderWidth:2,
    width: wp('85%'),
    //justifyContent: 'center',
    //alignItems:'center'
    alignSelf: 'center',
    height: hp('20%'),
    //borderWidth: 2,
    marginTop: hp('5%'),
  },
  viewatualizar: {
    width: wp('100%'),
    //borderWidth: 2,
    height: hp('8%'),
    flex: 1 / 8,
  },
  buttonatualizar: {
    flexDirection: 'column-reverse',
    width: '100%',
    height: '100%',
    backgroundColor: '#55bede',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonconcluir: {
    flexDirection: 'column-reverse',
    width: '100%',
    height: '100%',
    backgroundColor: '#32a852',
    justifyContent: 'center',
    alignItems: 'center'
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    //alignItems: "center",
  },
  modalView: {
    width: wp('100%'),
    height: hp('82%'),
    //margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  openButton2: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    fontSize: 16,
    marginBottom: 25,
    marginTop: 20,
    textAlign: "center",
    fontWeight: 'bold'
  },
  boxtexteditnome: {
    width: wp('85%'),
    //borderWidth: 1
  },
  texteditnome: {
    fontSize: 18,

  }
});

export default styles;
