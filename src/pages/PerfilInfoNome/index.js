import React, { useState, useEffect } from 'react';
import { View, Text, KeyboardAvoidingView, Image, TouchableOpacity, TextInput, Modal, Alert } from 'react-native';

//Importação do AsyncStorage para utilização de dados guardados do usuário
import AsyncStorage from '@react-native-async-storage/async-storage';

//Importação da biblioteca de icons do react-native-vector-icons
import Icon from 'react-native-vector-icons/Entypo';

//Importando a Estilização StyleSheet
import styles from './styles';

//importando o firebase
import firebase from '../../FirebaseConnection';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

export default function PerfilInfoNome() {

    //Atribuindo a navegação para a constante navigantion, assim
    //a mesma tem acesso à nevegação entre as telas da aplicação
    const navigation = useNavigation();

    //Atribuição e definição de variáveis de estado.
    const [objetousuario, setMyObjetoUsuario] = useState('');
    const [teste, setMyTeste] = useState('');
    const [estadoedicao, setMyEstadoedicao] = useState(false);
    const [estadofocus, setMyEstadofocus] = useState(false);

    //Função que pegas todos os dados do usuário do objeto que 
    //foi guardado no asyncstorage nas teslas anteriores 
    const getDataObject = async () => {
        try {
            const pegarobjeto = await AsyncStorage.getItem('@objectuser')

            setMyObjetoUsuario(JSON.parse(pegarobjeto));

        } catch (e) {
            Alert.alert(e);
        }
    }

    //Função que faz o controle do atributo "editable" do 
    //TextInput do nome, assim ele é desabilitado antes de
    //clicar no botão "ATUALIZAR" e é habilitado depois do
    //botão "ATUALIZAR" ser clicado
    const controledeedicao = () => {
        if (estadoedicao == false) {
            setMyEstadoedicao(!estadoedicao);
            setMyEstadofocus(!estadofocus);
            setMyTeste(objetousuario.nome);
        }
    }

    //Função assíncrona que faz a requisição de update do nome
    //do usuário, logo ao digitar um nome seja ele igual ou 
    //diferente do atual e concluir, o mesmo é atualizado na
    //base de dados do firebase.
    async function updateNome() {
        await firebase.database().ref('usuario/' + objetousuario.chave + '/nome').set(teste);

        navigation.navigate('Home', {
            screen: 'Home'
        })
    }

    //Função que faz a validação do nome que o usuário digita
    //no input de nome, é validada com uma expressão regular
    //(regex), logo se estiver verdadeira é executada a função
    //que faz a requisição de update de nome no firebase
    const validaNome = (nome) => {

        var reg = /^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/

        if (reg.test(nome) == true) {

            if (nome != objetousuario.nome) {
                console.log(nome + ' é somente palavras');
                updateNome();
                Alert.alert("Nome atualizado com sucesso !");
            } else {
                Alert.alert("O nome atual e o nome digitado são iguais, tente novamente !");
            }

        } else {
            Alert.alert("O nome contém carcteres imprópios, tente de novo !");
        }

    }

    //Função de Renderização Condicional referente ao botão
    //de "ATUALIZAR" e o botão "CONCLUIR", e ambos agem em
    //função do estado "estadoedicao" que controle o atributo
    //editable do TextInput
    const controlebuttoneditable = () => {
        if (estadoedicao == false) {
            return (<TouchableOpacity style={styles.buttonatualizar} onPress={() => controledeedicao()}>
                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 16, letterSpacing: 1 }}>ATUALIZAR</Text>
            </TouchableOpacity>);
        } else {
            return (<TouchableOpacity style={styles.buttonconcluir} onPress={() => validaNome(teste)}>
                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 16, letterSpacing: 1 }}>CONCLUIR</Text>
            </TouchableOpacity>);
        }
    }

    //Função de Renderização Condicional referente ao TextInput
    //do nome que será utilizado para edição e atualização do
    //nome.
    const controletextinputnome = () => {
        if (estadoedicao == false) {
            return (<TextInput style={styles.inputsenha2}
                onChangeText={text => setMyTeste(text)}
                placeholder={objetousuario.nome}
                editable={estadoedicao}
                autoFocus={estadofocus}
                value={teste}
            />);
        } else {
            return (<TextInput style={styles.inputsenha3}
                onChangeText={text => setMyTeste(text)}
                placeholder={objetousuario.nome}
                editable={estadoedicao}
                autoFocus={estadofocus}
                value={teste}
            />);
        }
    }

    const controletexteditnome = () => {
        if (estadoedicao == false) {
            return (
                <Text style={styles.texteditnome}>Nome atual:</Text>
            );
        } else {
            return (
                <Text style={styles.texteditnome}>Digite seu novo nome:</Text>
            );
        }
    }

    const gotoPerfil = () => {
        navigation.navigate('Perfil', {
            screen: 'Perfil',
        });
    }
    //Useeffect é o ciclo de vida do Componente(da tela)
    useEffect(() => {
        //console.log(typeof teste);
        getDataObject();
        console.log(objetousuario.chave)

    }, [objetousuario.chave]);

    return (

        <KeyboardAvoidingView style={styles.container}>

            {/* 
                View pai comum com overflow: 'hidden' pois só com esse atributo
                é possível aplicar a sombra da view filha somente no bottom
                dela.
            */}
            <View style={{ flex: 1 }}>

                {/* View topo onde ficará BOTÃO DE VOLTAR E "*/}
                <View style={styles.topo}>
                    <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                        <TouchableOpacity style={styles.viewphoto} onPress={() => gotoPerfil()}>
                            <Icon name="chevron-left" color="black" size={40} />
                        </TouchableOpacity>

                        <View style={styles.dadosperfil}>
                            <View style={styles.dadosnome}>
                                <Text style={{ fontSize: 17 }}>ALTERAÇÃO DE NOME</Text>
                            </View>

                        </View>
                    </View>
                </View>

                {/* VIEW DO "ALTERAR NOME" */}
                <View style={styles.viewinputname2}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ paddingRight: 5 }}>
                            <Icon name="pencil" color="#4a4a4a" size={32} />
                        </View>
                        <Text style={{ fontSize: 28, marginBottom: 10 }}>
                            Alterar Nome
                        </Text>
                    </View>

                    <View style={styles.boxtexteditnome}>
                        <Text style={{ lineHeight: 25, fontSize: 14 }}>
                            Para alteração de nome, utilize o botão <Text style={{ fontWeight: 'bold' }}>"ATUALIZAR"</Text> para habilitar a edição da caixa de texto abaixo, assim podendo mudar seu nome !
                        </Text>
                    </View>

                </View>


                {/* INPUT NOME (RENDERIZAÇÃO CONDICIONAL DO INPUT NOME) */}
                <View style={styles.viewinputname}>
                    {controletexteditnome()}
                    {controletextinputnome()}
                </View>

                {/* CARD QUE ESPECIFICA INFORMAÇÕES DO USO DO NOME */}
                <View style={styles.cardinfonome}>
                    <View style={styles.iconinfo}>
                        <Icon name="info-with-circle" color="#55bede" size={30} />
                    </View>
                    <View style={styles.cautioninfonome}>
                        <Text style={{ lineHeight: 25 }}>
                            Para preservar a segurança e por medidas de políticas do aplicativo<Text style={{ fontWeight: 'bold' }}> evite usar nomes ofensivos ou termos inapropriados </Text>no seu nome.
                        </Text>
                    </View>
                </View>


            </View>


            <View style={styles.viewatualizar}>
                {controlebuttoneditable()}
            </View>

        </KeyboardAvoidingView>
    );
}