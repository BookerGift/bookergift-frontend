import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: wp('4%')
  },
  containerconteudo: {
    //width: '100%',
    //borderWidth: 1,
    flex: 4,
    alignSelf: 'center',
    alignItems: 'center'
    //height: '100%'

  },
  boxiconsucess: {
    width: '100%',
    //height: '100%',
    //borderWidth: 1,
    alignItems: 'center'
  },
  boxtxtsucesstitle: {
    width: '100%',
    //height: '100%',
    //borderWidth: 1,
    alignItems: 'center'
  },
  boxtxtsucesstitle2: {
    width: '100%',
    //height: '100%',
    //borderWidth: 1,
    alignItems: 'center',
    marginTop: hp('2%'),
  },
  boxinfolivro: {
    width: '100%',
    //borderWidth: 1,
    marginTop: hp('3%'),
    //borderRadius: 20
  },
  imgsubprincipal: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    position: 'relative'
  },
  viewimg: {
    width: wp('47%'),
    height: hp('31%'),
    borderColor: '#55bede',
    //height: '70%',
    //borderWidth: 2,
    alignItems: 'center',
    //justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: hp('2%')
  },
  viewtitle: {
    flexDirection: 'row',
    //borderWidth: 1,
    width: '60%',
    justifyContent: 'center',
    marginTop: hp('1%'),
    //alignSelf: 'center',
    textAlign: 'center'
  },
  containerbuttons: {
    //borderWidth: 1,
    flex: 1 / 2,
    flexDirection: 'row'
  },
  buttoncancel: {
    width: '50%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    //borderWidth: 1,
    backgroundColor: '#d44c4c',
    flexDirection: 'row'
  },
  buttonconfirm: {
    width: '50%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    //borderWidth: 1,
    backgroundColor: '#11bd2e',
    flexDirection: 'row'
  }
});

export default styles;