import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, Alert } from 'react-native';

//Importando Axios para comunicação com a API do ISBNdb
import axios from 'axios';

import AsyncStorage from '@react-native-async-storage/async-storage';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

//Importando a Estilização StyleSheet
import styles from './styles';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

//importando o firebase
import firebase from '../../FirebaseConnection';

import { LogBox } from 'react-native';
LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
LogBox.ignoreAllLogs();//Ignore all log notifications

export default function ConfirmLivro() {

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  const [objetousuario, setMyObjetoUsuario] = useState('');
  const [objetoLivro, setMyObjetoLivro] = useState('');
  const [qtdLivros, setMyQtdLivros] = useState(0);
  //const [valueAdd, SetMyValueAdd] = useState(0);
  const [arrayListLivro, setArrayListLivro] = useState('');


  const getDataObject = async () => {
    try {
      const pegarobjetouser = await AsyncStorage.getItem('@objectuser')
      const pegarobjetolivro = await AsyncStorage.getItem('@objectlivro')

      setMyObjetoUsuario(JSON.parse(pegarobjetouser));
      setMyObjetoLivro(JSON.parse(pegarobjetolivro));

      //console.log(objetousuario);
    } catch (e) {
      Alert.alert(e);
    }
  }

  const goToConfirmCadLivro = async () => {

    let valueAdd = 0;
    let livro = await firebase.database().ref('livro');
    let idlivro = livro.push().key;

    livro.child(idlivro).set({
      titulo: objetoLivro.title,
      autor: objetoLivro.author,
      editora: objetoLivro.publisher,
      linguagem: objetoLivro.language,
      imagem: objetoLivro.image,
      datapublicado: objetoLivro.datepublished,
      isbn: objetoLivro.isbn,
      usuario: {
        iduser: objetoLivro.iduser,
      },
      publicado: objetoLivro.publicado,
    })



    valueAdd = qtdLivros + 1;


    await firebase.database().ref('usuario/' + objetousuario.chave + '/livros_cadastrados').set(valueAdd);
    console.log(`Valor valueAdd: ${valueAdd}`);
    console.log(`Valor qtdLivros: ${qtdLivros}`);


    setArrayListLivro({
      key: idlivro,
      titulo: objetoLivro.title,
      autor: objetoLivro.author,
      editora: objetoLivro.publisher,
      imagem: objetoLivro.image,
      linguagem: objetoLivro.language,
      datapublicado: objetoLivro.datepublished,
      isbn: objetoLivro.isbn,
      publicado: objetoLivro.publicado,
    });

    navigation.navigate('ConfirmCadastroLivro', {
      screen: 'ConfirmCadastroLivro',
    });

  }

  const LivroStorage = async () => {

    //Try catch para guardar objeto com dados do livro
    //no async storage
    try {

      await AsyncStorage.setItem('@objetolivropublicar', JSON.stringify(arrayListLivro));

    } catch (e) {

      alert(e);

    }

  }

  useEffect(() => {
    const getQtdLivros = async () => {
      await firebase.database().ref(`usuario/${objetousuario.chave}/livros_cadastrados`).once('value', (snapshot) => {
        setMyQtdLivros(snapshot.val());
      });
    }

    getDataObject();
    getQtdLivros();

    console.log(`Essa é a QUANTIDADE DE LIVROSSSSSSSSSSSSSSSSSS ==== ${qtdLivros}`);

  }, [objetoLivro.isbn]);


  useEffect(() => {
    LivroStorage();
    //console.log(arrayListLivro);

  }, [arrayListLivro]);
  return (

    < View style={styles.container} >

      <View style={styles.containerconteudo}>

        {/* Icon de Check Verde */}
        <View style={styles.boxiconsucess}>
          <Icon name="checkmark-circle" color="#11bd2e" size={80} />
        </View>

        {/* TXT: Encontramos seu Livro !*/}
        <View style={styles.boxtxtsucesstitle}>
          <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#11bd2e' }}>Encontramos seu Livro !</Text>
        </View>

        {/* TXT: Confira as informações do Livro */}
        <View style={styles.boxtxtsucesstitle2}>
          <Text style={{ fontSize: 14, color: '#55bede' }}>Confira as informações dele abaixo:</Text>
        </View>

        {/* VIEW Geral das informações do Livro */}
        <View style={styles.boxinfolivro}>

          {/* Imagem do Livro */}
          <View style={styles.viewimg}>
            <Image style={styles.imgsubprincipal} source={{ uri: objetoLivro.image }}>
            </Image>
          </View>

          {/* Título do Livro */}
          <View style={styles.viewtitle}>
            <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>Título:</Text>
            <Text numberOfLines={1}> {objetoLivro.title}</Text>
          </View>

          {/* Autor do Livro */}
          <View style={styles.viewtitle}>
            <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>Autor:</Text>
            <Text> {objetoLivro.author}</Text>
          </View>

          {/* Editora do Livro */}
          <View style={styles.viewtitle}>
            <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>Editora:</Text>
            <Text> {objetoLivro.publisher}</Text>
          </View>

          {/* Languagem do Livro */}
          <View style={styles.viewtitle}>
            <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>Linguagem (Idioma):</Text>
            <Text> {objetoLivro.language}</Text>
          </View>

          {/* Data de publicação do Livro */}
          <View style={styles.viewtitle}>
            <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>Ano de publicação:</Text>
            <Text> {objetoLivro.datepublished}</Text>
          </View>

          {/* ISBN do Livro */}
          <View style={styles.viewtitle}>
            <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>Código ISBN:</Text>
            <Text> {objetoLivro.isbn}</Text>
          </View>

        </View>

      </View>

      {/* VIEW dos buttons de Cancel e Confirm */}
      <View style={styles.containerbuttons}>

        <TouchableOpacity style={styles.buttoncancel} onPress={() => navigation.goBack()}>
          <Icon name="md-close-circle" color="white" size={20} />
          <Text style={{ color: 'white', fontWeight: 'bold', padding: 5 }}>
            Cancelar
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttonconfirm} onPress={() => goToConfirmCadLivro()}>
          <Icon name="checkmark-circle" color="white" size={20} />
          <Text style={{ color: 'white', fontWeight: 'bold', padding: 5 }}>
            Tudo certo, Finalizar !
          </Text>
        </TouchableOpacity>

      </View>

    </View >
  );
}