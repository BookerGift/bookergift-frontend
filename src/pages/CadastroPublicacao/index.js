import React, { useState, useEffect } from 'react';
import { View, Text, KeyboardAvoidingView, Image, TouchableOpacity, SnapshotViewIOS, ScrollView } from 'react-native';
//importando o firebase
import firebase from '../../FirebaseConnection';

//Importando a Estilização StyleSheet
import styles from './styles';

//Importação da biblioteca de icons do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

import AsyncStorage from '@react-native-async-storage/async-storage';

export default function CadastroPublicacao() {

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  const [objetousuario, setMyObjetoUsuario] = useState('');
  const [livro, setLivro] = useState([]);
  const [arrayListLivro, setArrayListLivro] = useState('');

  const getDataObject = async () => {
    try {
      const pegarobjeto = await AsyncStorage.getItem('@objectuser')

      setMyObjetoUsuario(JSON.parse(pegarobjeto));

      if (objetousuario === null) {
        chaveusuario = ' DEU MERDA LEKAO '
      }

    } catch (e) {
      Alert.alert(e);
    }
  }

  const getDataLivros = async () => {
    await firebase.database().ref('livro').on('value', (snapshot) => {

      setLivro([]);

      snapshot.forEach((chilItem) => {
        let livrodata = {
          key: chilItem.key,
          autor: chilItem.val().autor,
          editora: chilItem.val().editora,
          datapublicado: chilItem.val().datapublicado,
          imagem: chilItem.val().imagem,
          titulo: chilItem.val().titulo,
          iduser: chilItem.val().usuario.iduser,
          isbn: chilItem.val().isbn,
          publicado: chilItem.val().publicado,
        };

        if (objetousuario.chave == livrodata.iduser) {
          if (livrodata.publicado == false) {
            setLivro(oldArray => [...oldArray, livrodata]);
          }
        }

      });
    })


  }


  const buscarLivro = async (chave) => {

    let controller = true;

    await firebase.database().ref(`livro/${chave}`).once('value', (snapshot) => {

      setArrayListLivro({
        key: snapshot.key,
        titulo: snapshot.val().titulo,
        autor: snapshot.val().autor,
        editora: snapshot.val().editora,
        imagem: snapshot.val().imagem,
        linguagem: snapshot.val().linguagem,
        datapublicado: snapshot.val().datapublicado,
        isbn: snapshot.val().isbn,
        publicado: snapshot.val().publicado,
      });

    });

    navigation.navigate('MostrarLivroPublicar', {
      screen: 'MostrarLivroPublicar',
    });
  }

  const LivroStorage = async () => {

    //Try catch para guardar objeto com dados do livro
    //no async storage
    try {

      await AsyncStorage.setItem('@objetolivropublicar', JSON.stringify(arrayListLivro));

    } catch (e) {

      alert(e);

    }

  }

  const gotoBiblioteca = () => {
    navigation.navigate('Biblioteca', {
      screen: 'Biblioteca',
    });
  }

  const renderLivrosPublicar = (totalLivros) => {
    if (totalLivros > 0) {
      return (
        <ScrollView>
          {livro.map(datalivro => (

            <TouchableOpacity style={{
              width: wp('100%'),
              height: hp('40%'),
              //borderRadius: 10,
              marginRight: wp('15%'),
              //marginBottom: hp('2%'),
              borderBottomWidth: 1,
              borderColor: '#c8c8c8',
              paddingTop: 20,
              paddingLeft: 20,
              flexDirection: 'row'
              //alignSelf: 'center',

            }} onPress={() => buscarLivro(datalivro.key)}>

              <View style={{ width: '40%', height: '100%' }}>
                <Image style={{
                  width: '100%',
                  height: '80%',
                  resizeMode: 'contain',
                }} source={{ uri: datalivro.imagem }} />
                <View style={{ width: '100%', height: '20%', }}>
                  <Text numberOfLines={2} style={{ fontWeight: 'bold', textAlign: 'center', marginTop: '5%' }}>{datalivro.titulo}</Text>
                </View>
              </View>

              <View style={{ width: '60%', height: '100%', alignItems: 'center' }}>
                <Text numberOfLines={1} style={{ fontWeight: 'bold', fontSize: 16, marginBottom: hp('5%') }}>Mais informações:</Text>
                <Text numberOfLines={1} style={{ fontWeight: 'bold', marginBottom: hp('1%'), color: '#55bede' }}>Autor:  <Text style={{ fontWeight: 'normal', color: 'black' }}>{datalivro.autor}</Text></Text>
                <Text numberOfLines={1} style={{ fontWeight: 'bold', marginBottom: hp('1%'), color: '#55bede' }}>Editora:  <Text style={{ fontWeight: 'normal', color: 'black' }}>{datalivro.editora}</Text></Text>
                <Text numberOfLines={1} style={{ fontWeight: 'bold', marginBottom: hp('1%'), color: '#55bede' }}>Data Publicação:  <Text style={{ fontWeight: 'normal', color: 'black' }}>{datalivro.datapublicado}</Text></Text>
                <Text numberOfLines={1} style={{ fontWeight: 'bold', marginBottom: hp('1%'), color: '#55bede' }}>Cod ISBN:  <Text style={{ fontWeight: 'normal', color: 'black' }}>{datalivro.isbn}</Text></Text>
                <View numberOfLines={1} style={{ width: '80%', alignItems: 'center', height: '30%', justifyContent: 'center' }}><Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 16 }}>Este livro ainda não foi Publicado !</Text></View>
              </View>

            </TouchableOpacity>
          ))}
        </ScrollView>
      );
    } else {
      return (
        <View style={{ width: wp('100%'), height: '71%', }}>
          {/* VIEW do ICON da carinha triste */}
          <View style={{ height: '50%', width: '100%', alignItems: 'center', justifyContent: 'flex-end' }}>
            <Icon name="md-sad-outline" color="#c8c8c8" size={80} />
          </View>

          {/* VIEW do TXT: Ops não há nenhum livro aqui ! */}
          <View style={{ height: '50%', width: '100%', alignItems: 'center' }}>
            <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#c3c3c3' }}>
              OPS, Não há nenhum livro
            </Text>
            <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#c3c3c3' }}>que possa ser publicado</Text>
            <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#c3c3c3' }}> em sua biblioteca</Text>
          </View>
        </View>
      );
    }
  }

  useEffect(() => {

    getDataObject();
    getDataLivros();

    console.log(livro);

  }, [livro.length, objetousuario.chave]);

  useEffect(() => {
    LivroStorage();

  }, [arrayListLivro]);

  return (
    <View style={{ flex: 1 }}>
      {/* View topo onde ficará as informações do perfil do usuário */}
      <View style={styles.topo}>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity style={styles.viewphoto} onPress={() => gotoBiblioteca()}>
            <Icon name="arrow-back-outline" color="black" size={40} />
          </TouchableOpacity>

          <View style={styles.dadosperfil}>
            <View style={styles.dadosnome}>
              <Text style={{ fontSize: 17, fontWeight: 'bold' }}>PUBLICAR LIVRO</Text>
            </View>

          </View>
        </View>
      </View>

      <View style={{ width: wp('100%'), alignSelf: 'center', height: hp('8%'), borderBottomWidth: 2, borderColor: '#c8c8c8', }}>
        <View style={{ width: '90%', alignSelf: 'center' }}>
          <Text>Escolha um de seus livros que ainda <Text style={{ fontWeight: 'bold' }}>não foram publicados</Text> para publicá-lo !</Text>
        </View>

      </View>



      {renderLivrosPublicar(livro.length)}

    </View>
  );
}