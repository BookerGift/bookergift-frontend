import { StyleSheet } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topo: {
    width: wp('100%'),
    height: hp('12%'),
    //backgroundColor: '#fff',
    paddingLeft: wp('3%'),
    justifyContent: 'center',
    //borderWidth:3
  },
  viewphoto: {
    //borderWidth:3,
    borderColor: 'gray',
    width: '15%',
    height: '100%',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  dadosperfil: {
    width: '70%',
    height: '100%',
    //borderWidth:3,
    justifyContent: 'center',
    alignItems: 'center'
  },
});

export default styles;