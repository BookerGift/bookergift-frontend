import React from 'react';
import { View, Text, ImageBackground } from 'react-native';

//Importando a Estilização StyleSheet
import styles from './styles';

//Importando biblioteca react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

import { TouchableOpacity } from 'react-native-gesture-handler';

export default function ConfirmDoacao() {

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  const gotoHome = () => {
    navigation.navigate('Home', {
      screen: 'Home'
    })
  }

  return (
    <View style={styles.container}>
      <ImageBackground source={require('../../assets/adocao.png')} style={styles.image}>


        <View style={styles.containerconteudo}>
          <View style={styles.viewiconinfo}>
            <Icon style={{ alignSelf: 'center' }} name="checkmark-circle" size={100} color="#11bd2e" />
          </View>

          <View style={styles.viewtitleinfo}>
            <Text style={{ textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>
              Parabéns Pela Adoção !
            </Text>
          </View>

          <View style={styles.infosubtitle}>
            <View style={{ marginTop: 10, width: '70%', alignSelf: 'center' }}>
              <Text style={{ fontSize: 17, lineHeight: 30, textAlign: 'center' }}> Será enviado um e-mail para <Text style={{ fontWeight: 'bold' }}>Você </Text> e para o Doador do livro para que continuem o processo de doação !</Text>
            </View>
          </View>

          <View style={styles.viewbuttonexit}>
            <TouchableOpacity
              style={{
                width: '40%',
                borderRadius: 10,
                backgroundColor: '#55bede',
                height: '100%',
                alignSelf: 'center',
                //borderWidth: 1,
                justifyContent: 'center'
              }} onPress={() => gotoHome()}>

              <Text style={{ textAlign: 'center', fontWeight: "bold", color: '#fff' }}>Finalizar</Text>

            </TouchableOpacity>
          </View>

        </View>

      </ImageBackground>

    </View>
  );
}