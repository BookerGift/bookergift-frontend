import React, { useState, useEffect } from 'react';
import { View, Text, KeyboardAvoidingView, Image, TouchableOpacity, TextInput, Modal, Alert } from 'react-native';

//Importação do AsyncStorage para utilização de dados guardados do usuário
import AsyncStorage from '@react-native-async-storage/async-storage';

//Importação da biblioteca de icons do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

//Importando a Estilização StyleSheet da Tela PerfilInfoTelefone
import styles from './styles';

//Importando biblioteca de mask para telefone ou máscaras no geral
import { TextInputMask } from 'react-native-masked-text';

//importando o firebase
import firebase from '../../FirebaseConnection';

export default function PerfilInfoTelefone() {

    //Atribuição e definição de variáveis de estado.
    const [objetousuario, setMyObjetoUsuario] = useState('');
    const [teste, setMyTeste] = useState('');
    const [estadoedicao, setMyEstadoedicao] = useState(false);
    const [estadofocus, setMyEstadofocus] = useState(false);

    //Função que pegas todos os dados do usuário do objeto que 
    //foi guardado no asyncstorage nas teslas anteriores 
    const getDataObject = async () => {
        try {
            const pegarobjeto = await AsyncStorage.getItem('@objectuser')

            setMyObjetoUsuario(JSON.parse(pegarobjeto));

        } catch (e) {
            Alert.alert(e);
        }
    }

    //Função que faz o controle do atributo "editable" do 
    //TextInput do nome, assim ele é desabilitado antes de
    //clicar no botão "ATUALIZAR" e é habilitado depois do
    //botão "ATUALIZAR" ser clicado
    const controledeedicao = () => {
        if (estadoedicao == false) {
            setMyEstadoedicao(!estadoedicao);
            setMyEstadofocus(!estadofocus);
            setMyTeste(objetousuario.telefone);
        }
    }

    //Função assíncrona que faz a requisição de update do nome
    //do usuário, logo ao digitar um nome seja ele igual ou 
    //diferente do atual e concluir, o mesmo é atualizado na
    //base de dados do firebase.
    async function updateNome() {
        await firebase.database().ref('usuario/' + objetousuario.chave + '/nome').set(teste);
    }

    //Função que faz a validação do nome que o usuário digita
    //no input de nome, é validada com uma expressão regular
    //(regex), logo se estiver verdadeira é executada a função
    //que faz a requisição de update de nome no firebase
    const validaNome = (nome) => {

        var reg = /^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/

        if (reg.test(nome) == true) {
            console.log(nome + ' é somente palavras');
            updateNome();
            Alert.alert("Nome atualizado com sucesso !");
        } else {
            console.log(nome + 'testou negativo e tem números');
        }

    }

    //Função de Renderização Condicional referente ao botão
    //de "ATUALIZAR" e o botão "CONCLUIR", e ambos agem em
    //função do estado "estadoedicao" que controle o atributo
    //editable do TextInput
    const controlebuttoneditable = () => {
        if (estadoedicao == false) {
            return (<TouchableOpacity style={styles.buttonatualizar} onPress={() => controledeedicao()}>
                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 16, letterSpacing: 1 }}>ATUALIZAR</Text>
            </TouchableOpacity>);
        } else {
            return (<TouchableOpacity style={styles.buttonconcluir} onPress={() => validaNome(teste)}>
                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 16, letterSpacing: 1 }}>CONCLUIR</Text>
            </TouchableOpacity>);
        }
    }

    //Função de Renderização Condicional referente ao TextInput
    //do nome que será utilizado para edição e atualização do
    //nome.
    const controletextinputtelefone = () => {
        if (estadoedicao == false) {
            return (<TextInputMask type={'cel-phone'}
                options={{ maskType: 'BRL', withDDD: true, }}
                placeholder={objetousuario.telefone}
                editable={estadoedicao}
                autoFocus={estadofocus}
                value={teste}
                onChangeText={text => { setMyTeste(text.replace(/\D+/g, "")); }}
                style={styles.inputsenha2}
            />);
        } else {
            return (<TextInputMask type={'cel-phone'}
                options={{ maskType: 'BRL', withDDD: true, }}
                placeholder={objetousuario.telefone}
                editable={estadoedicao}
                autoFocus={estadofocus}
                value={teste}
                onChangeText={text => { setMyTeste(text.replace(/\D+/g, "")); }}
                style={styles.inputsenha3}
            />);
        }
    }

    //Useeffect é o ciclo de vida do Componente(da tela)
    useEffect(() => {
        //console.log(typeof teste);
        getDataObject();


    });


    return (
        <KeyboardAvoidingView style={styles.container}>
            {/* 
                View pai comum com overflow: 'hidden' pois só com esse atributo
                é possível aplicar a sombra da view filha somente no bottom
                dela.
            */}
            <View style={{ flex: 2 }}>

                {/* View topo onde ficará as informações do perfil do usuário */}
                <View style={styles.topo}>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={styles.viewphoto}>
                            <Icon name="arrow-back-outline" color="black" size={40} />
                        </TouchableOpacity>

                        <View style={styles.dadosperfil}>
                            <View style={styles.dadosnome}>
                                <Text style={{ fontSize: 17 }}>INFORMAÇÕES PESSOAIS</Text>
                            </View>

                        </View>
                    </View>
                </View>

                <View style={styles.viewinputname2}>
                    <Text style={{ fontSize: 22, fontWeight: 'bold', marginBottom: 10 }}>
                        Alteração dados de contato
                    </Text>
                    <Text>
                        Atualize seus dados de contato !
                    </Text>
                </View>

                <View style={styles.viewinputname}>
                    <View style={styles.viewlabeltelefone}>

                        <Text style={{ fontSize: 16, color: 'gray', fontWeight: 'bold' }}>
                            Telefone
                        </Text>
                    </View>

                    {/* Estrutua da bandeira do país e o input de telefone
                        que é uma renderização condicional controlada por função
                        */}
                    <View style={styles.telinput}>
                        <View style={styles.boxviewpais}>
                            <Image
                                source={require('../../assets/brazil.png')}
                                style={styles.imgpais}
                            >
                            </Image>
                            <Text style={{ alignSelf: 'center' }}>
                                +55
                                </Text>
                        </View>
                        {controletextinputtelefone()}
                    </View>


                </View>

                <TouchableOpacity style={styles.viewinputemail} onPress={() => Alert.alert('O campo de e-mail ainda não pode ser editado !')}>
                    <View style={{ width: '100%', justifyContent: 'center', height: '80%' }}>
                        <View style={styles.viewlabeltelefone}>

                            <Text style={{ fontSize: 16, color: 'gray', fontWeight: 'bold' }}>
                                E-mail
                            </Text>
                        </View>
                        {/*<TextInput style={styles.inputsenhemail}
                            onChangeText={text => setMyTeste(text)}
                            placeholder={objetousuario.email}
                            editable={false}
                            value={objetousuario.email}                        
                        />*/}
                        <View style={styles.inputsenhemail}>
                            <Text>{objetousuario.email}</Text>
                        </View>

                        <View style={styles.view_table_info_email}>
                            <View style={styles.viewlabeltelefone2}>
                                <Icon name="information-circle" color="gray" size={30} />
                            </View>
                            <View>
                                <Text>
                                    O dado "E-mail ainda não está disponível {'\n'}para edição no momento."
                                </Text>
                            </View>
                        </View>

                    </View>
                </TouchableOpacity>

            </View>

            <View style={styles.viewatualizar}>
                {controlebuttoneditable()}
            </View>
        </KeyboardAvoidingView>
    );
}