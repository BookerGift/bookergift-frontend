import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#fff',
        //borderWidth:1
    },
    topo:{
        width:wp('100%'),
        height:hp('12%'),
        backgroundColor: '#fff',
        paddingLeft:wp('3%'),
        justifyContent:'center',
        //borderWidth:3
    },
    viewphoto:{
        //borderWidth:3,
        borderColor:'gray',
        width: '15%', 
        height: '100%',
        alignSelf:'center',
        justifyContent:'center',
        alignItems:'center'
    },
    imagecircle:{
        width:'24%', 
        height: 70, 
        borderRadius: 70/ 2, 
        backgroundColor:'#55bede',
       
    },
    dadosperfil:{
        width:'75%',
        height:'100%',
        //borderWidth:3,
        justifyContent:'center',
        alignItems:'center'
    },
    dadosnome:{
        //borderWidth:3,
    },
    viewopcoesperfil:{
        flex:2,
        //borderWidth:3,
    },
    infopessoais:{
        width:'95%',
        height:'30%',
        //borderWidth:2,
        borderBottomWidth:0.2,
        borderColor:'gray',
        alignSelf:'center',
        justifyContent:'center',
        padding:20
    },
    inputsenhemail:{
        width:'100%',
        
        height:hp('7%'),
        color:'gray',
        //opacity:0.2,
        backgroundColor:'#f2f2f2',        
        marginTop:hp('1%'),
        //color:'#fff',
        //height:hp('7%'),
        paddingLeft:5,
        borderBottomWidth:0.3,
        borderRadius:5,
        borderColor:'gray',
        marginBottom:hp('2%'),
        justifyContent:'center'
      },
      inputsenha2:{
        width:'80%',
        paddingLeft:15,
        height:hp('7%'),
        color:'gray',
        //opacity:0.2,
        marginTop:hp('2%'),
        //color:'#fff',
        //height:hp('7%'),
        //paddingLeft:15,
        borderWidth:1,
        //justifyContent:'center',
        borderRadius:5,
        borderColor:'gray',
      },
      inputsenha3:{
        width:'80%',
        paddingLeft:15,
        height:hp('7%'),
        color:'black',
        //opacity:0.2,
        marginTop:hp('2%'),
        //color:'#fff',
        //height:hp('7%'),
        //paddingLeft:15,
        borderWidth:1,
        //justifyContent:'center',
        borderRadius:5,
        borderColor:'#55bede'
      },
      viewinputname:{
        //borderWidth:2,
        width:wp('85%'),
        //justifyContent:'center',
        alignItems:'center',
        height:hp('16%'), 
        alignSelf:'center'
          
      },
      viewinputemail:{
        borderTopWidth:1,
        borderWidth:2,
        width:wp('85%'),
        justifyContent:'center',
        alignItems:'center',
        height:hp('22%'), 
        alignSelf:'center',
        borderColor:'#d6d6d6',
        borderRadius:5,
        padding:15,
        backgroundColor:'#f2f2f2',  
    },
      viewinputname2:{
        //borderWidth:2,
        width:wp('100%'),
        justifyContent:'center',
        //alignItems:'center'
        paddingLeft:wp('8%'),height:hp('15%'),//borderWidth:2
    },
    viewatualizar:{
        width:wp('100%'),
        //borderWidth:2,
        height:hp('8%'), 
    },
    buttonatualizar:{
        flexDirection:'column-reverse',
        width:'100%',
        height:'100%',
        backgroundColor:'#55bede',
        justifyContent:'center',
        alignItems:'center'
    },
    buttonconcluir:{
      flexDirection:'column-reverse',
      width:'100%',
      height:'100%',
      backgroundColor:'#32a852',
      justifyContent:'center',
      alignItems:'center'
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
       //alignItems: "center",
      },
      modalView: {
        width:wp('100%'),
        height:hp('82%'),
        //margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
      },
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2,
      },
      openButton2: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        fontSize:16,
        marginBottom: 25,
        marginTop: 20,
        textAlign: "center",
        fontWeight:'bold'
      },
      viewlabeltelefone:{
        //borderWidth:1,
        //paddingLeft:wp('8%'),
        width:'100%',
        //borderWidth:1
    },
      viewlabeltelefone2:{
          //borderWidth:1,
          //paddingLeft:wp('8%'),
          width:'11%'
      },
      view_table_info_email:{
        width:'100%',
        //borderWidth:1,
        flexDirection:'row'
      },
      imgpais:{
        resizeMode:'contain',
        width:wp('10%'),
        height:hp('5%'),
        alignSelf:'center'
      },
      telinput:{
        width:wp('80%'),
        flexDirection:'row'
      },
      boxviewpais:{
        width:wp('20%'),
        height:hp('7%'),
        //borderWidth:1,
        marginTop:hp('2%'),
        marginLeft:wp('-2%'),
        paddingLeft:wp('1%'),
        flexDirection:'row',
        //backgroundColor:'#55bede',
      }
});

export default styles;
