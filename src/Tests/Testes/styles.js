import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f0f0f0',
    flex: 1,
  },
  topo: {
    width: wp('100%'),
    height: hp('12%'),
    backgroundColor: '#f0f0f0',
    padding: wp('5%'),
    //borderWidth: 1
  },
  viewphoto: {
    //borderWidth:3,
    borderColor: 'gray',
    width: '17%',
    height: '100%',
  },
  imagecircle: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    backgroundColor: '#55bede',

  },
  dadosperfil: {
    width: '75%',
    height: '100%',
    //borderWidth: 1,
    justifyContent: 'center'

  },
  dadosnome: {
    marginBottom: 5,
    //borderWidth: 1
  },
  viewopcoesperfil: {
    flex: 2,
    //borderWidth:3,
  },
  infopessoais: {
    width: '95%',
    height: '30%',
    //borderWidth:2,
    borderBottomWidth: 0.2,
    borderColor: 'gray',
    alignSelf: 'center',
    justifyContent: 'center',
    padding: 20
  },
  boxolauser: {
    width: wp('85%'),
    height: hp('8%'),
    //borderWidth: 1,
    alignSelf: 'center'
  },
  boxmeuslivros: {
    width: wp('85%'),
    height: hp('6%'),
    //borderWidth: 1,
    alignSelf: 'center',
    flexDirection: 'row'
  },
  boxscrolview: {
    width: wp('85%'),
    height: hp('100%'),
    borderWidth: 1,
    alignSelf: 'center',
    //flexDirection: 'row',
    //paddingTop: 0
    //justifyContent: 'center'
  },
  boxofflivro: {
    width: wp('85%'),
    height: hp('40%'),
    borderWidth: 3,
    //alignSelf: 'center'
    marginTop: hp('2%'),
    borderRadius: 10,
    borderColor: '#c8c8c8',
    backgroundColor: '#d2d2d2',
    justifyContent: 'center'
  },
  cadumlivro: {
    alignSelf: 'center',
    //borderWidth: 1,
    height: '55%',
    width: '45%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    //backgroundColor: '#2eb329'
  }
});

export default styles;
