const insertTeste = async () => {

  let teste = await firebase.database().ref('teste');
  let idteste = teste.push().key;

  teste.child(idteste).set({
    titulo: 'TESTANDO',
    usuario: {
      id: 'ID1234567USER',
    }
  })
}

Alterar:

Collection usuário

async function cadastrar() {

  await firebase.auth().createUserWithEmailAndPassword(props.email, props.senha)
    .then((value) => {
      firebase.database().ref('usuario').child(value.user.uid).set({
        nome: props.nome,
        termos: props.aceitetermos,
        livros_cadastrados: qtdlivros,
        publicacoes: qtdpublicacoes,
        contato: {
          telefone: props.telefone,
          email: props.email,
        }
      })

      firebase.auth().currentUser.sendEmailVerification().then(function () {
        console.log('Email enviado com sucesso');
      }, function (error) {
        console.log('Deu merda');
      });

      setIsVisible(!isVisible);  //Fecha o modal
      navigation.navigate('ConfirmCadastro');
    })
    .catch((error) => {
      alert('Algo deu errado!');
    })

}



Collection livro

const goToConfirmCadLivro = async () => {

  let valueAdd = 0;
  let livro = await firebase.database().ref('livro');
  let idlivro = livro.push().key;

  livro.child(idlivro).set({
    titulo: objetoLivro.title,
    autor: objetoLivro.author,
    editora: objetoLivro.publisher,
    linguagem: objetoLivro.language,
    imagem: objetoLivro.image,
    datapublicado: objetoLivro.datepublished,
    isbn: objetoLivro.isbn,
    usuario: {
      iduser: objetoLivro.iduser,
    },
    publicado: objetoLivro.publicado,
  })



  valueAdd = qtdLivros + 1;


  await firebase.database().ref('usuario/' + objetousuario.chave + '/livros_cadastrados').set(valueAdd);
  console.log(`Valor valueAdd: ${valueAdd}`);
  console.log(`Valor qtdLivros: ${qtdLivros}`);

  navigation.navigate('ConfirmCadastroLivro', {
    screen: 'ConfirmCadastroLivro',
  });

}

collection Publicações

const criarPublicacaoLivro = async () => {

  let valueAdd = 0;
  let livropublicado = true;

  let publicacao = await firebase.database().ref('publicacao');
  let idpublicacao = publicacao.push().key;

  publicacao.child(idpublicacao).set({
    usuario: {
      iduser: objetousuario.chave,
      nome: objetousuario.nome,
      telefone: objetousuario.telefone,
      email: objetousuario.email
    },
    livro: {
      idlivro: objetoLivro.key,
      imagem: objetoLivro.imagem,
      nome: objetoLivro.titulo,
      editora: objetoLivro.editora,
    },
    titulo: tituloPublicacao,
    descricao: descricao,
    status: statusPublicacao,
    datapublicacao: mostrarData,
  })


  valueAdd = qtdPublicacoes + 1;


  await firebase.database().ref('usuario/' + objetousuario.chave + '/publicacoes').set(valueAdd);
  await firebase.database().ref('livro/' + objetoLivro.key + '/publicado').set(livropublicado);

  setIsVisible(!isVisible);

  navigation.navigate('Biblioteca', {
    screen: 'Biblioteca',
  });

  Alert.alert("O Livro foi publicado com sucesso !");
}
