import React, { useState, useEffect } from 'react';
import { View, Text, KeyboardAvoidingView, Image, TouchableOpacity, SnapshotViewIOS, ScrollView } from 'react-native';
//importando o firebase
import firebase from '../../FirebaseConnection';

//Importando a Estilização StyleSheet
import styles from './styles';

//Importação da biblioteca de icons do react-native-vector-icons
import Icon from 'react-native-vector-icons/Entypo';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import ListarLivrosUser from '../../Components/ListarLivrosUser';


export default function TesteListRemoveLivros1() {

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  const [objetousuario, setMyObjetoUsuario] = useState('');
  const [livro, setLivro] = useState([]);
  const [publicacoes, setPublicacoes] = useState([]);


  const getDataObject = async () => {
    try {
      const pegarobjeto = await AsyncStorage.getItem('@objectuser')

      setMyObjetoUsuario(JSON.parse(pegarobjeto));

      if (objetousuario === null) {
        chaveusuario = ' DEU MERDA LEKAO '
      }

    } catch (e) {
      Alert.alert(e);
    }
  }

  const getDataPublicacao = async () => {
    await firebase.database().ref('publicacao').on('value', (snapshot) => {

      setPublicacoes([]);

      snapshot.forEach((chilItem) => {
        let publicacaodata = {
          key: chilItem.key,
          titulo: chilItem.val().titulo,
          descricao: chilItem.val().descricao,
          iduser: chilItem.val().iduser,
          idlivro: chilItem.val().idlivro,
          datapublicacao: chilItem.val().publicado,
          status: chilItem.val().status,
        };

        if (objetousuario.chave == publicacaodata.iduser) {
          setPublicacoes(oldArray => [...oldArray, publicacaodata]);
        }

      });
    })
  }

  const buscarLivro = async (chave) => {

    await firebase.database().ref(`livro/${chave}`).once('value', (snapshot) => {

      setArrayListLivro({
        key: snapshot.key,
        titulo: snapshot.val().titulo,
        autor: snapshot.val().autor,
        editora: snapshot.val().editora,
        imagem: snapshot.val().imagem,
        linguagem: snapshot.val().linguagem,
        datapublicado: snapshot.val().datapublicado,
        isbn: snapshot.val().isbn,
        publicado: snapshot.val().publicado,
      });

    });

    navigation.navigate('MostrarLivroBiblioteca', {
      screen: 'MostrarLivroBiblioteca',
    });
  }

  const renderPublicacoesBiblioteca = () => {

    return (

      <View style={{ flexDirection: 'row' }}>
        {publicacoes.map(datapublicacao => (

          <TouchableOpacity style={{
            width: wp('60%'),
            height: hp('30%'),
            borderRadius: 10,
            marginRight: wp('15%'),
            marginTop: hp('2%'),
            borderWidth: 1
            //alignSelf: 'center',

          }}>
            <Image style={{
              width: '100%',
              height: '100%',
              resizeMode: 'contain',
            }} source={{ uri: imgLivro }} />


            <Text numberOfLines={2} style={{ fontWeight: 'bold', textAlign: 'center', marginTop: '5%' }}>{datapublicacao.key}</Text>

          </TouchableOpacity>
        ))}
      </View>

    );


  }


  const pegarImgLivro = async (chavelivro) => {

    await firebase.database().ref(`livro/${chavelivro}/imagem`).once('value', (snapshot) => {

      setImgLivro(snapshot.val());
    });

  }

  useEffect(() => {

    getDataObject(); //pegando o objeto livro
    getDataPublicacao();


  }, [publicacoes.length]);

  {/*
  useEffect(() => {
    LivroStorage();
    //console.log(arrayListLivro);
  }, [arrayListLivro]);
  */}

  return (

    <View>
      {/* Box de Publicações de "Minhas Publicações" */}
      <ScrollView horizontal={true} style={styles.boxscrolview}>

        {renderPublicacoesBiblioteca()}

      </ScrollView>

    </View>
  );
}