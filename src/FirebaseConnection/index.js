import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

let firebaseConfig = {
    apiKey: "AIzaSyDleJ8zJYPPeBsCn3BpoIP6iEXUwN8I5UI",
    authDomain: "bookergift.firebaseapp.com",
    databaseURL: "https://bookergift.firebaseio.com",
    projectId: "bookergift",
    storageBucket: "bookergift.appspot.com",
    messagingSenderId: "477964160208",
    appId: "1:477964160208:web:40dc530c61daf17cdd22d5",
    measurementId: "G-XR44Q33CLJ"
  };

  if (!firebase.apps.length){
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }

export default firebase;