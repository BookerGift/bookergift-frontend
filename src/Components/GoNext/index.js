import React from 'react';
import {Alert, View, Text, TouchableOpacity, Button} from 'react-native';

//Importando navegação do React Navigation para navegação entre telas
import {NavigationContext, useNavigation} from '@react-navigation/native';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/FontAwesome5';

//Importando biblioteca de responsividade
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default function GoNext(props) {
  
  const navigation = useNavigation();

  //Função de ação para o button de "-->" da tela de cadastro
  //pois após preenchimento do telefone é necessária a realização
  //de um redirecionamento de tela para a confirmação do SMS
  const alertNotification = () => {
    let pegartelefone = props.telefone;
    
    if (pegartelefone.length < 11) {
      Alert.alert('Você precisa terminar de preencher o campo de telefone !');
    } else {
      if (pegartelefone.length >= 11) {
        navigation.navigate('Cadastro2', {phone:props.telefone});
      }
    }
  };

  //Esta função faz com que seja enviado um controlador
  //do estado "liga e desliga" do componente touchableOpacity
  //logo é possível habilita-lo e desabilita-lo quando necessário
  const controleTrueFalse = (phone) => {
    if (phone.length < 11) {
      return true;
    } else {
      if (phone.length >= 11) {
        return false;
      }
    }
  }

  return (
    <TouchableOpacity 
      disabled={controleTrueFalse(props.telefone)} 
      style={{
        width:wp('15%'), 
        height:hp('8%'), 
        borderRadius: 200 / 2,
        backgroundColor:props.colorcontrole,
        justifyContent:'center',
        alignItems:'center',
      }}
      
      onPress={() => alertNotification()}
    >        
      <Icon name="arrow-right" size={25} color={'#fff'}/>
    </TouchableOpacity>
  );
}
