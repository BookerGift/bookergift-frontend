import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, Alert, Modal, StyleSheet, TextInput } from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//Importando Axios para comunicação com a API do ISBNdb
import axios from 'axios';

import styles from './styles'

import AsyncStorage from '@react-native-async-storage/async-storage';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

//importando o firebase
import firebase from '../../FirebaseConnection';


export default function ConfirmCadastroLivro() {

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();


  const goToCadastroPublicacao = async () => {

    navigation.navigate('MostrarLivroPublicar', {
      screen: 'MostrarLivroPublicar',
    });
  }

  const goToLibrary = () => {
    navigation.navigate('Home', {
      screen: 'Biblioteca'
    });
  }

  return (
    <View style={styles.container}>

      {/* View vazia somente para alugar espaço do flex da screen */}
      <View style={styles.container1}></View>


      <View style={styles.container2}>

        {/*  */}
        <View style={styles.viewiconsucess}>
          <Icon name="checkmark-circle" color="#11bd2e" size={120} />
        </View>

        <View style={styles.viewtxtsucess}>
          <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#11bd2e' }}>Livro cadastrado com sucesso!</Text>

        </View>
      </View>

      <View style={styles.container3}>

        <View style={styles.viewtextpublicar}>
          <Text style={{ fontSize: 16, color: '#b4b4b4', fontWeight: 'bold' }}>Deseja publicar o livro cadastrado ?</Text>
        </View>

        <View style={styles.buttontextpublicar}>
          <TouchableOpacity style={styles.buttoncancel} onPress={() => goToLibrary()}>
            <Text style={{ color: 'white', fontWeight: 'bold' }}>Agora não !</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.buttonsucess} onPress={() => goToCadastroPublicacao()}>
            <Text style={{ color: 'white', fontWeight: 'bold' }}>Quero Publicar !</Text>
          </TouchableOpacity>
        </View>
      </View>

    </View>
  );
}
