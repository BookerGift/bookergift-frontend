import React, { useState } from 'react';
import { Alert, View, Text, TouchableOpacity, Button, Modal, StyleSheet } from 'react-native';

//Importando navegação do React Navigation para navegação entre telas
import { NavigationContext, useNavigation } from '@react-navigation/native';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/FontAwesome5';

//Importando biblioteca de responsividade
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//importando o firebase
import firebase from '../../FirebaseConnection';

export default function GoNextConfirmForgotPassword(props) {

  const navigation = useNavigation();

  //Constante de estado da visibilidade do Modal
  const [isVisible, setIsVisible] = useState(false);

  //Função de ação para o button de "-->" da tela de cadastro
  //pois após preenchimento do telefone é necessária a realização
  //de um redirecionamento de tela para a confirmação do SMS
  const finalizarrecovery = () => {
    setIsVisible(!isVisible);
    return navigation.navigate('Login');
  };

  async function resetpassword() {

    await firebase.auth().sendPasswordResetEmail(props.emailrecovery)
      .then((value) => {
        setIsVisible(true);
      })
      .catch((error) => {
        alert('O e-mail digitado está inválido ou não está cadastrado. Tente novamente !');
      })

  }

  return (
    <View>
      <TouchableOpacity
        disabled={props.controle}
        style={{
          width: wp('15%'),
          height: hp('8%'),
          borderRadius: 200 / 2,
          backgroundColor: props.colorcontrole,
          justifyContent: 'center',
          alignItems: 'center',
        }}

        onPress={() => resetpassword()}
      >

        <Icon name="arrow-right" size={25} color={'#fff'} />

      </TouchableOpacity>

      <Modal
        animationType="slide"
        transparent={true}
        visible={isVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
        style={{ width: '50%' }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Icon name="check" size={50} color={'#4ddb65'} />
            <Text style={styles.modalText}>Foi encaminhada uma notificação de recuperação para o seguinte e-mail:</Text>
            <Text style={{ fontSize: 16 }}>
              <Text style={{ fontSize: 18 }}> {props.emailrecovery} </Text>{'\n'}{'\n'}


            </Text>

            <View style={{ flexDirection: 'row' }}>

              <TouchableOpacity
                style={{ ...styles.openButton2, backgroundColor: "#4ddb65", }}
                onPress={() => {
                  finalizarrecovery() //ÁREA DE ENVIO DE EMAIL PARA O EMAIL DIGITADO.
                }}
              >
                <Text style={styles.textStyle}>Finalizar</Text>
              </TouchableOpacity>

            </View>

          </View>
        </View>
      </Modal>
    </View>


  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    width: wp('80%'),
    height: hp('45%'),
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  openButton2: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    fontSize: 16,
    marginBottom: 25,
    marginTop: 20,
    textAlign: "center",
    fontWeight: 'bold'
  }
});
