import {StyleSheet} from 'react-native';

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
      viewrodape:{
        flexDirection:'row',
        borderTopWidth:0.6,
        borderColor:'white',
        height:hp('10%'),
        //borderWidth:5,
      },
      viewpular:{
        //borderWidth:5,
        width:wp('25%'),
        alignItems:'center',
        justifyContent:'center',
        //borderWidth:5,
      },
      viewseta:{
        //borderWidth:5,
        width:wp('25%'),
        alignItems:'center',
        justifyContent:'center'
      },
      viewbolinha:{
        flexDirection:'row',
        //borderWidth:5,
        width:wp('50%'),
        paddingLeft:10,
        paddingRight:10,
        justifyContent:'space-between',
        alignSelf:'center'
      },
      txtpular:{
          fontWeight:'bold',
          fontSize:16,
          color:'white'
      },

});

export default styles;
