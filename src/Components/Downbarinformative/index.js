import React from 'react'
import {View, Text, TouchableOpacity} from 'react-native'

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/MaterialIcons';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

//Inportando estilização do componente Downbarinformative
import styles from './styles'

export default function Downbarinformative(props) {

    const navigation = useNavigation();

    return (

        //View Principal da estrutura do downbar
        <View style={styles.viewrodape}>
            
            {/* VIEW do Pular */}
            <View style={styles.viewpular}>
                <TouchableOpacity onPress={() => navigation.navigate('Login')} >
                    <Text style={styles.txtpular}>PULAR</Text>
                </TouchableOpacity>
            </View>

            {/* VIEW das bolinhas */}
            <View style={styles.viewbolinha}>
                <Icon onPress={() => navigation.navigate('Informative1')} name="fiber-manual-record" size={15} color={props.c1} />
                <Icon onPress={() => navigation.navigate('Informative2')} name="fiber-manual-record" size={15} color={props.c2} />
                <Icon onPress={() => navigation.navigate('Informative3')}name="fiber-manual-record" size={15} color={props.c3} />
                <Icon onPress={() => navigation.navigate('Informative4')} name="fiber-manual-record" size={15} color={props.c4}/>
            </View>

            {/* VIEW da setinha */}
            <View style={styles.viewseta}>
                <TouchableOpacity onPress={() => navigation.navigate(props.screenName)}>
                    <Icon name="keyboard-arrow-right" size={30} color="white" />
                </TouchableOpacity>
            </View>
        
        </View>
    );
}