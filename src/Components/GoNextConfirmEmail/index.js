import React, { useState } from 'react';
import {Alert, View, Text, TouchableOpacity, Button} from 'react-native';

//Importando navegação do React Navigation para navegação entre telas
import {NavigationContext, useNavigation} from '@react-navigation/native';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/FontAwesome5';

//Importando biblioteca de responsividade
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default function GoNextConfirmEmail(props) {
  
  const navigation = useNavigation();


  //Função de ação para o button de "-->" da tela de cadastro
  //pois após preenchimento do telefone é necessária a realização
  //de um redirecionamento de tela para a confirmação do SMS
  const alertNotification = () => {
    
    if (props.controle === false) {
      navigation.navigate('TermsServices', {phone:props.telefone, nome:props.nome, email:props.email, senha:props.senha});
      //navigation.navigate('Login', {phone:props.telefone});
    } else {
      Alert.alert('Você precisa terminar de preencher o campo de telefone !'); 
    }
  };

  return (
    
    <TouchableOpacity 
      disabled={props.controle} 
      style={{
        width:wp('15%'), 
        height:hp('8%'), 
        borderRadius: 200 / 2,
        backgroundColor:props.colorcontrole,
        justifyContent:'center',
        alignItems:'center',
      }}
      
      onPress={() => alertNotification()}
    >        
      <Icon name="arrow-right" size={25} color={'#fff'}/>
      {console.log(props.controle)}
    </TouchableOpacity>
  );
}
