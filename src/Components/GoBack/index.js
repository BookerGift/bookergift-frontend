import React from 'react';
import {Alert, View, Text, TouchableOpacity, Button} from 'react-native';

//Importando navegação do React Navigation para navegação entre telas
import {useNavigation} from '@react-navigation/native';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/FontAwesome5';

//Importando biblioteca de responsividade
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default function GoBack(props) {
  
  const navigation = useNavigation();

  //Função de ação para o button de "<--" goback da tela de cadastro
  //pois após preenchimento do telefone é necessária a realização
  //de um redirecionamento de tela para a confirmação do SMS
  const goToBack = () => {
    navigation.navigate(props.irParaTela);
  };

  return (
    <TouchableOpacity
      disabled={false}
      style={{
        width: wp('15%'),
        height: hp('8%'),
        justifyContent: 'center',
        alignItems: 'center',
      }}
      onPress={() => goToBack()}>
      <Icon name="arrow-left" size={30} color={props.colorback} />
    </TouchableOpacity>
  );
}
