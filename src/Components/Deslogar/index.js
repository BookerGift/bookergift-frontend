import React, { useEffect, useState } from 'react';
import { View, Text, Modal, TouchableOpacity } from 'react-native';

import styles from './styles';

import AsyncStorage from '@react-native-async-storage/async-storage';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

export default function Deslogar() {

  const [isVisible, setIsVisible] = useState(true);

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  const loggout = async () => {

  }

  return (

    <Modal
      animationType="slide"
      transparent={true}
      visible={true}
      onRequestClose={() => {
        setIsVisible(!isVisible);
      }}
      style={{ width: '50%' }}
    >
      <View style={styles.centeredView2}>
        <View style={styles.modalView2}>


          <Text style={{ fontWeight: 'bold' }}>Deseja realmente deslogar da aplicação ?</Text>

          <View style={{ flexDirection: 'row', marginTop: '10%' }}>
            <TouchableOpacity style={styles.buttonremover2} onPress={() => setIsVisible(!isVisible)}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Cancelar</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonpublicar2}>
              <Text style={{ color: 'white', fontWeight: 'bold' }} onPress={() => loggout()}>Sair</Text>
            </TouchableOpacity>

          </View>
        </View>
      </View>
    </Modal>

  );
}