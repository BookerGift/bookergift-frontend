import { StyleSheet } from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


const styles = StyleSheet.create({
  centeredView2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView2: {
    width: wp('70%'),
    height: hp('20%'),
    //margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    //borderWidth: 1,
    padding: 25,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  buttonremover2: {
    width: '50%',
    height: '100%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#f54842',
    justifyContent: 'center',
    alignItems: 'center',
    //right: wp('2%')
  },
  buttonpublicar2: {
    width: '50%',
    height: '100%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#2ed936',
    justifyContent: 'center',
    alignItems: 'center',
    left: wp('3%')
  },
});

export default styles;