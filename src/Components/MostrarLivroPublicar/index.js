import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, Alert, Modal, StyleSheet, TextInput } from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//Importando Axios para comunicação com a API do ISBNdb
import axios from 'axios';

import styles from './styles'

import AsyncStorage from '@react-native-async-storage/async-storage';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

//importando o firebase
import firebase from '../../FirebaseConnection';


export default function MostrarLivroPublicar() {

  const [objetoLivro, setMyObjetoLivro] = useState('');
  const [objetousuario, setMyObjetoUsuario] = useState('');
  const [tituloPublicacao, setTituloPublicacao] = useState('');
  const [descricao, setDescricao] = useState('');
  const [statusPublicacao, setStatusPublicacao] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [qtdPublicacoes, setMyQtdPublicacoes] = useState(0);

  const data = new Date();

  const dataAtual = {
    dia: data.getDate(),
    mes: data.getMonth(),
    ano: data.getFullYear(),
  }
  const mostrarData = `${dataAtual.dia}/${dataAtual.mes}/${dataAtual.ano}`;

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();


  //Função que pega no AsyncStorage o array de livros e array de usuário logado
  const getDataObject = async () => {
    try {

      const pegarobjetousuario = await AsyncStorage.getItem('@objectuser');
      const pegarobjetolivro = await AsyncStorage.getItem('@objetolivropublicar');

      setMyObjetoUsuario(JSON.parse(pegarobjetousuario));
      setMyObjetoLivro(JSON.parse(pegarobjetolivro));

      //console.log(objetousuario);
    } catch (e) {
      Alert.alert(e);
    }
  }

  const removerLivro = async (chave) => {
    await firebase.database().ref(`livro/${chave}`).remove();

  }

  const gotoPublicarLivro = () => {
    navigation.navigate('CadastroPublicacao', {
      screen: 'CadastroPublicacao',
    });
  }

  const gotoCadastroPublicacao = () => {
    navigation.navigate('CadastroPublicacao', {
      screen: 'CadastroPublicacao',
    })
  }

  const criarPublicacaoLivro = async () => {

    let valueAdd = 0;
    let livropublicado = true;

    let publicacao = await firebase.database().ref('publicacao');
    let idpublicacao = publicacao.push().key;

    publicacao.child(idpublicacao).set({
      usuario: {
        iduser: objetousuario.chave,
        nome: objetousuario.nome,
        telefone: objetousuario.telefone,
        email: objetousuario.email
      },
      livro: {
        idlivro: objetoLivro.key,
        imagem: objetoLivro.imagem,
        titulo: objetoLivro.titulo,
        editora: objetoLivro.editora,
        isbn: objetoLivro.isbn,
        autor: objetoLivro.autor
      },
      titulo: tituloPublicacao,
      descricao: descricao,
      status: statusPublicacao,
      datapublicacao: mostrarData,
    })

    await firebase.database().ref('livro/' + objetoLivro.key + '/publicacao/idpublicacao').set(idpublicacao);


    valueAdd = qtdPublicacoes + 1;


    await firebase.database().ref('usuario/' + objetousuario.chave + '/publicacoes').set(valueAdd);
    await firebase.database().ref('livro/' + objetoLivro.key + '/publicado').set(livropublicado);

    setIsVisible(!isVisible);

    navigation.navigate('Biblioteca', {
      screen: 'Biblioteca',
    });

    Alert.alert("O Livro foi publicado com sucesso !");
  }

  useEffect(() => {
    const getQtdLivros = async () => {
      await firebase.database().ref(`usuario/${objetousuario.chave}/publicacoes`).once('value', (snapshot) => {
        setMyQtdPublicacoes(snapshot.val());
      });
    }

    getDataObject();
    getQtdLivros();

    console.log(objetousuario);

  }, [objetoLivro.length, objetousuario.chave]);

  return (
    <View style={styles.containerprincipal}>

      {/* View topo onde ficará as informações do perfil do usuário */}
      <View style={styles.topo}>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity style={styles.viewphoto} onPress={() => navigation.goBack()}>
            <Icon name="arrow-back-outline" color="black" size={40} />
          </TouchableOpacity>

          <View style={styles.dadosperfil}>
            <View style={styles.dadosnome}>
              <Text style={{ fontSize: 17, fontWeight: 'bold' }}>PUBLICAR LIVRO</Text>
            </View>

          </View>
        </View>
      </View>

      <View style={styles.viewinfolivro}>

        <View style={styles.viewimg2}>
          <View style={{ width: '100%', height: '25%' }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold', marginBottom: 5 }}>Finalizar Publicação</Text>
            <Text>Para finalizar a <Text style={{ fontWeight: 'bold' }}>publicação do seu livro</Text>, digite um título e uma descrição para a mesma.</Text>
          </View>

          <View style={styles.viewboxtitulo}>
            <TextInput style={styles.inputtitulo}
              placeholderTextColor='gray'
              placeholder="Digite um título para a publicação !"
              onChangeText={text => setTituloPublicacao(text)}
              value={tituloPublicacao}
              maxLength={40}
            />
          </View>

          <View style={styles.viewboxdescricao}>
            <TextInput style={styles.inputdescricao}
              multiline={true}
              numberOfLines={4}
              placeholderTextColor='gray'
              placeholder="Digite uma descrição para sua publicação !"
              onChangeText={text => setDescricao(text)}
              value={descricao}
              maxLength={200}
            />

            <TouchableOpacity style={styles.bntpublicar} onPress={() => setIsVisible(!isVisible)}>
              <Text style={{ fontWeight: 'bold', color: '#fff' }}>PUBLICAR</Text>
            </TouchableOpacity>
          </View>
        </View>

      </View>

      <View style={{ width: wp('100%'), height: '5%', justifyContent: 'center', }}>
        <View style={{ width: '90%', alignSelf: 'center', }}>
          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Livro que será publicado: </Text>
        </View>
      </View>

      <View style={styles.viewimg}>
        <View style={styles.img}>
          <Image style={{
            width: '100%',
            height: '100%',
            resizeMode: 'cover',
            //position: 'relative',
            alignSelf: 'center',
          }} source={{ uri: objetoLivro.imagem }} />
        </View>

        <View style={styles.infobuttonslivro}>

          <View>
            <Text style={{ fontWeight: 'bold', fontSize: 16 }} >Título:</Text>
            <Text numberOfLines={1}>{objetoLivro.titulo}</Text>
          </View>

          <View style={{ marginTop: hp('1%') }}>
            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Autor:</Text>
            <Text numberOfLines={1}>{objetoLivro.autor}</Text>
          </View>

          <View style={{ marginTop: hp('1%') }}>
            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Editora:</Text>
            <Text numberOfLines={1}>{objetoLivro.editora}</Text>
          </View>

          <View style={{ marginTop: hp('1%') }}>
            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Ano de Publicação:</Text>
            <Text numberOfLines={1}>{objetoLivro.datapublicado}</Text>
          </View>

          <View style={{ marginTop: hp('1%') }}>
            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>ISBN:</Text>
            <Text numberOfLines={1}>{objetoLivro.isbn}</Text>
          </View>

        </View>


      </View>



      <Modal
        animationType="slide"
        transparent={true}
        visible={isVisible}
        onRequestClose={() => {
          setIsVisible2(!isVisible);
        }}
        style={{ width: '50%' }}
      >
        <View style={styles.centeredView2}>
          <View style={styles.modalView2}>

            <View style={{ height: '75%' }}>
              <Text style={{ fontWeight: 'bold' }}>Deseja realmente Publicar este livro?</Text>
              <View style={{ marginTop: 15 }}>
                <Text>
                  Ao publicar um livro, o mesmo <Text style={{ fontWeight: 'bold' }}>não poderá ser excluído.</Text>
                </Text>

                <Text style={{ marginTop: 15 }}>O Livro publicado só poderá ser excluído após que a publicação for excluída.</Text>
              </View>

            </View>



            <View style={{ flexDirection: 'row', height: '25%' }}>
              <View style={{ flexDirection: 'row', height: '100%' }}>
                <TouchableOpacity style={styles.buttonremover2} onPress={() => setIsVisible(!isVisible)}>
                  <Text style={{ color: 'white', fontWeight: 'bold' }}>Cancelar</Text>
                </TouchableOpacity>

                <View style={{ height: '100%', width: '10%' }}></View>

                <TouchableOpacity style={styles.buttonpublicar2}>
                  <Text style={{ color: 'white', fontWeight: 'bold' }} onPress={() => criarPublicacaoLivro()}>Publicar</Text>
                </TouchableOpacity>

              </View>


            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}