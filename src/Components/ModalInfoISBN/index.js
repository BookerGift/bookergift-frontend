import React, { useState } from 'react';
import { Alert, View, Text, TouchableOpacity, Button, Modal, StyleSheet, Image, ScrollView } from 'react-native';

//Importando navegação do React Navigation para navegação entre telas
import { NavigationContext, useNavigation } from '@react-navigation/native';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/FontAwesome5';

//Importando biblioteca de responsividade
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//importando o firebase
import firebase from '../../FirebaseConnection';

export default function ModalInfoISBN(props) {

  const navigation = useNavigation();

  //Constante de estado da visibilidade do Modal
  const [isVisible, setIsVisible] = useState(props.visible);

  //Função de ação para o button de "-->" da tela de cadastro
  //pois após preenchimento do telefone é necessária a realização
  //de um redirecionamento de tela para a confirmação do SMS
  const finalizar = () => {
    setIsVisible(!isVisible);
    navigation.navigate('CadastrarLivro1', {
      screen: 'CadastrarLivro1',
    });
  };

  const txtisbn1 = 'O International Standard Book Number, sendo chamado inicialmente de Standard Book Numbering, é um sistema internacional de identificação de livros e softwares que utiliza números para classificá-los por título, autor, país, editora e edição. '

  const txtisbn2 = 'Existem duas variações de ISBN, o ISBN10 e o ISBN13, o ISBN13 eixte o prefixo 978 na frente no código e o restante do cógdigo chegamos aos 13 dígitos, e o ISBN10 é o código sem o 978.'

  const txtisbn3 = 'O cadastro será feito apenas por código ISBN, pois atraveś dele obtemos todas as informações do livro, e o controle é feito de forma eficaz, tanto nos dados do livro quanto no estado de conservação do mesmo.'

  return (
    <View>

      <Modal
        animationType="slide"
        transparent={false}
        visible={isVisible}
        onRequestClose={() => {
          setIsVisible(!isVisible)
        }}
      >
        <ScrollView>

          <View style={styles.modalView}>

            <View style={styles.viewtxtisbn}>
              <Text style={{ fontSize: 26, fontWeight: 'bold', color: '#55bede' }}>
                Cadastro por ISBN
              </Text>
            </View>

            <View style={styles.viewtxtisbntitle}>
              <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#383838' }}>
                Você sabe o que é ISBN ?
              </Text>
            </View>

            <View style={styles.viewtxtifoisbn}>
              <Text style={{ fontSize: 14, color: '#383838', textAlign: 'justify', lineHeight: 25 }}>
                {txtisbn1}
              </Text>
            </View>

            <View style={styles.viewimgexampleisbn}>
              <Image
                source={require('../../assets/infoISBN.jpeg')}
                style={styles.imgprincipal}>
              </Image>
            </View>

            <View style={styles.viewimgexampleisbn2}>
              <Text style={{ color: '#55bede', fontSize: 14, textAlign: 'center', fontWeight: 'bold' }}>
                Representação da localização do ISBN
              </Text>
            </View>



            <View style={styles.viewtxtifoisbn}>
              <Text style={{ fontSize: 14, color: '#383838', textAlign: 'justify', lineHeight: 25 }}>
                {txtisbn2}
              </Text>
            </View>

            <View style={styles.viewtxtifoisbn3}>
              <Text style={{ fontSize: 14, color: '#383838', textAlign: 'justify', lineHeight: 25 }}>
                {txtisbn3}
              </Text>
            </View>

          </View>



        </ScrollView>

        {/* View de button "Entendi, Vamos começar" */}
        <View style={styles.buttonFinalizar}>

          <TouchableOpacity style={styles.openButton2} onPress={() => finalizar()}>
            <Text style={styles.textStyle}>Entendi, Vamos começar !</Text>
          </TouchableOpacity>

        </View>

      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    //justifyContent: "center",
    //alignItems: "center",
    //borderWidth: 1
  },
  modalView: {
    //width: wp('100%'),
    //height: hp('100%'),
    //margin: 20,
    backgroundColor: "white",
    //borderWidth: 1,
    //padding: 35,
    alignItems: "center",
    //flex: 2
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  openButton2: {
    flexDirection: 'column-reverse',
    width: '100%',
    height: '100%',
    backgroundColor: '#32a852',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    fontSize: 16,
    marginBottom: 25,
    marginTop: 20,
    textAlign: "center",
    fontWeight: 'bold'
  },
  buttonFinalizar: {
    width: wp('100%'),
    height: hp('10%'),
    //borderWidth: 1,
  },
  viewtxtisbn: {
    width: wp('85%'),
    //height: hp('10%'),
    //borderWidth: 1,
    marginTop: hp('5%')
  },
  viewtxtisbntitle: {
    width: wp('85%'),
    //height: hp('10%'),
    //borderWidth: 1,
    marginTop: hp('4%')
  },
  viewtxtifoisbn: {
    width: wp('85%'),
    //height: hp('10%'),
    //borderWidth: 1,
    marginTop: hp('3%'),
    marginBottom: hp('1%')
  },
  viewtxtifoisbn3: {
    width: wp('85%'),
    //height: hp('10%'),
    //borderWidth: 1,
    marginTop: hp('3%'),
    marginBottom: hp('5%')
  },
  viewimgexampleisbn: {
    width: wp('65%'),
    height: hp('34%'),
    borderRadius: 15,
    //borderWidth: 1,
    marginTop: hp('4%'),
    marginBottom: hp('2%'),
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    elevation: 5,
  },
  viewimgexampleisbn2: {
    width: wp('65%'),
    //height: hp('5%'),
    //borderWidth: 1,
    marginBottom: hp('2%'),
  },
  imgprincipal: {
    resizeMode: 'contain',
    width: '100%',
    height: '100%',
    borderRadius: 10,


  },
});
