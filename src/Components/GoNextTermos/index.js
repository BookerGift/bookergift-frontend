import React, { useState } from 'react';
import { Alert, View, Text, TouchableOpacity, Button, Modal, StyleSheet } from 'react-native';

//Importando navegação do React Navigation para navegação entre telas
import { NavigationContext, useNavigation } from '@react-navigation/native';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/FontAwesome5';

//Importando biblioteca de responsividade
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//importando o firebase
import firebase from '../../FirebaseConnection';

export default function GoNextTermos(props) {

  console.log('Aceite de Termos:', props.aceitetermos)
  const navigation = useNavigation();

  //Constante de estado da visibilidade do Modal
  const [isVisible, setIsVisible] = useState(false);

  const [qtdlivros, setMyQtdlivros] = useState(0);
  const [qtdpublicacoes, seMyQtdPublicacoes] = useState(0);

  //Função de ação para o button de "-->" da tela de cadastro
  //pois após preenchimento do telefone é necessária a realização
  //de um redirecionamento de tela para a confirmação do SMS

  const BUNDLE_ID = 'com.example.ios';

  async function cadastrar() {

    await firebase.auth().createUserWithEmailAndPassword(props.email, props.senha)
      .then((value) => {
        firebase.database().ref('usuario').child(value.user.uid).set({
          nome: props.nome,
          termos: props.aceitetermos,
          livros_cadastrados: qtdlivros,
          publicacoes: qtdpublicacoes,
          contato: {
            telefone: props.telefone,
            email: props.email,
          },
          saldo_adocao: 1,
          status: true,
        })

        firebase.auth().currentUser.sendEmailVerification().then(function () {
          console.log('Email enviado com sucesso');
        }, function (error) {
          console.log('Deu merda');
        });

        setIsVisible(!isVisible);  //Fecha o modal
        navigation.navigate('ConfirmCadastro', { email: props.email, senha: props.senha, nome: props.nome, telefone: props.telefone });
      })
      .catch((error) => {
        alert('Algo deu errado!');
      })

  }

  return (
    <View>
      <TouchableOpacity
        disabled={props.controle}
        style={{
          width: wp('15%'),
          height: hp('8%'),
          borderRadius: 200 / 2,
          backgroundColor: props.colorcontrole,
          justifyContent: 'center',
          alignItems: 'center',
        }}

        onPress={() => setIsVisible(true)}
      >
        <Icon name="arrow-right" size={25} color={'#fff'} />
        {console.log(props.controle)}
      </TouchableOpacity>

      <Modal
        animationType="slide"
        transparent={true}
        visible={isVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
        style={{ width: '50%' }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Icon name="exclamation-circle" size={50} color={'#edea34'} />
            <Text style={styles.modalText}>Seus dados estão corretos?</Text>
            <Text style={{ fontSize: 16 }}>
              <Text style={{ fontWeight: 'bold' }}>Nome:</Text> {props.nome}{'\n'}{'\n'}
              <Text style={{ fontWeight: 'bold' }}>Telefone:</Text> {props.telefone}{'\n'}{'\n'}
              <Text style={{ fontWeight: 'bold' }}>E-mail:</Text> {props.email}{'\n'}{'\n'}

            </Text>

            <View style={{ flexDirection: 'row' }}>

              <TouchableOpacity
                style={{ ...styles.openButton, backgroundColor: "#e8695a" }}
                onPress={() => {
                  setIsVisible(!isVisible);
                }}
              >
                <Text style={styles.textStyle}>Cancelar</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={{ ...styles.openButton2, backgroundColor: "#4ddb65", }}
                onPress={() => {
                  cadastrar()
                }}
              >
                <Text style={styles.textStyle}>Confirmar</Text>
              </TouchableOpacity>

            </View>

          </View>
        </View>
      </Modal>

    </View>

  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    width: wp('75%'),
    height: hp('50%'),
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  openButton2: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    fontSize: 16,
    marginBottom: 25,
    marginTop: 20,
    textAlign: "center",
    fontWeight: 'bold'
  }
});