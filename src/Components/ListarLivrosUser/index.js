import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, Alert, Modal, StyleSheet } from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//Importando Axios para comunicação com a API do ISBNdb
import axios from 'axios';

import AsyncStorage from '@react-native-async-storage/async-storage';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';


//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

//importando o firebase
import firebase from '../../FirebaseConnection';

export default function ListarLivrosUser() {

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  //Declaração de Estados da tela Biblioteca
  const [objetousuario, setMyObjetoUsuario] = useState('');
  const [objetoLivro, setMyObjetoLivro] = useState('');
  const [livro, setLivro] = useState([]);
  const [isVisible, setIsVisible] = useState(false);
  const [isVisible2, setIsVisible2] = useState(false);
  const [arrayListLivro, setArrayListLivro] = useState('');

  /* 
  const [arryListLivro2, setArrayListLivro2] = useState('');

  const arrayListLivroJSON = {
    titulo: '',
    editora: '',
    autor: '',
    linguagem: '',
    imagem: '',
    datapuclicacao: '',
    isbn: '',
    publicado:'',
  }
  */

  //Função que pega no AsyncStorage o array de livros e array de usuário logado
  const getDataObject = async () => {
    try {
      const pegarobjetouser = await AsyncStorage.getItem('@objectuser')
      const pegarobjetolivro = await AsyncStorage.getItem('@objectlivro')

      setMyObjetoUsuario(JSON.parse(pegarobjetouser));
      setMyObjetoLivro(JSON.parse(pegarobjetolivro));

      //console.log(objetousuario);
    } catch (e) {
      Alert.alert(e);
    }
  }

  //Função assíncrona que buscar um livro de uma determinada chave passada
  //por parâmetro e populando um vetor state, além de habilitar o modal que
  //renderiza esse vetor state com o map
  const buscarLivro = async (chave) => {

    let controller = true;

    await firebase.database().ref(`livro/${chave}`).once('value', (snapshot) => {

      setArrayListLivro({
        key: snapshot.key,
        titulo: snapshot.val().titulo,
        autor: snapshot.val().autor,
        editora: snapshot.val().editora,
        imagem: snapshot.val().imagem,
        linguagem: snapshot.val().linguagem,
        datapublicado: snapshot.val().datapublicado,
        isbn: snapshot.val().isbn,
        publicado: snapshot.val().publicado,
      });

    });

    navigation.navigate('MostrarLivroBiblioteca', {
      screen: 'MostrarLivroBiblioteca',
    });

    //setIsVisible(!isVisible);
  }

  const RenderLivroNaoPublicado = (boleano, chave) => {

    if (boleano) {
      return (
        <View>
          <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: '5%' }}>
            <Icon name="checkmark-circle" color="#11bd2e" size={25} />
            <View style={{ justifyContent: 'center' }}>
              <Text style={{ color: "#11bd2e", fontWeight: 'bold' }}>Livro Já Publicado !</Text>
            </View>
          </View>
          <TouchableOpacity style={{ alignSelf: 'center', marginTop: '5%', }}>
            <Text style={{ color: '#55bede', fontWeight: 'bold' }}>Ver publicação</Text>
          </TouchableOpacity>
        </View>

      );
    } else {
      return (

        <View>

          <View>
            <Text style={{ textAlign: 'center', color: '#55bede' }}>Este livro ainda não foi publicado.</Text>
            <Text style={{ textAlign: 'center', color: '#55bede' }}> O que deseja fazer?</Text>
          </View>

          <View style={styles.viewremover}>

            <TouchableOpacity style={styles.buttonremover} onPress={() => setIsVisible2(!isVisible2)}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Excluir</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonpublicar}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Publicar</Text>
            </TouchableOpacity>

          </View>

        </View>



      )
    }
  }

  const removerLivro = async (chave) => {
    await firebase.database().ref(`livro/${chave}`).remove();

    setIsVisible2(!isVisible2);
    setIsVisible(!isVisible);

  }

  const mostrarLivroStorage = async () => {

    //Try catch para guardar objeto com dados do livro
    //no async storage
    try {

      await AsyncStorage.setItem('@objectmostrarlivro', JSON.stringify(arrayListLivro));

    } catch (e) {

      alert(e);

    }

  }

  //Renderizar "Livro Já Publicado" na tela de BIBLIOTECA quando na listagem
  //dos livros tenha algum livro que já esteja publicado
  const renderPublicado = (boleano) => {
    if (boleano) {
      return (
        <View style={{ flexDirection: 'row' }}>
          <Icon name="checkmark-circle" color="#11bd2e" size={25} />
          <View style={{ justifyContent: 'center' }}>
            <Text style={{ color: "#11bd2e", fontWeight: 'bold' }}>Livro já Publicado !</Text>
          </View>
        </View>
      )
    }
  }

  const getDataLivros = async () => {
    await firebase.database().ref('livro').on('value', (snapshot) => {

      setLivro([]);

      snapshot.forEach((chilItem) => {
        let livrodata = {
          key: chilItem.key,
          autor: chilItem.val().autor,
          imagem: chilItem.val().imagem,
          titulo: chilItem.val().titulo,
          iduser: chilItem.val().iduser,
          publicado: chilItem.val().publicado,
        };

        if (objetousuario.chave === livrodata.iduser) {
          setLivro(oldArray => [...oldArray, livrodata]);
        }

      });
    })


  }

  useEffect(() => {

    getDataObject();
    getDataLivros();

    console.log(livro);

  }, [livro.length]);

  useEffect(() => {
    mostrarLivroStorage();
    console.log(arrayListLivro);

  }, [arrayListLivro]);

  return (
    <View style={{ flexDirection: 'row' }}>

      {livro.map(datalivro => (

        <TouchableOpacity style={{
          width: wp('40%'),
          height: hp('30%'),
          borderRadius: 10,

          marginRight: wp('15%')
        }} onPress={() => buscarLivro(datalivro.key)}>

          <Image style={{
            width: '100%',
            height: '100%',
            resizeMode: 'contain',
            position: 'relative',
            alignSelf: 'center',
          }} source={{ uri: datalivro.imagem }} />


          <Text numberOfLines={2} style={{ fontWeight: 'bold', textAlign: 'center', marginTop: '5%' }}>{datalivro.titulo}</Text>


          {renderPublicado(datalivro.publicado)}




        </TouchableOpacity>
      ))}

    </View >
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  centeredView2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  imgdatalivro: {
    width: '90%',
    height: '100%',
    resizeMode: 'cover',
    position: 'relative',
    //alignSelf: 'center',
    borderRadius: 10,
  },
  viewremover: {
    width: '100%',
    //height: '100%',
    //borderWidth: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: hp('2%')
  },
  viewpublicado: {
    width: '50%',
    height: '100%',
    //borderWidth: 1,
    justifyContent: 'center',
    //paddingLeft: '5%'
  },
  buttonremover: {
    width: '45%',
    height: '95%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#f54842',
    justifyContent: 'center',
    alignItems: 'center',
    right: wp('3%')
  },
  buttonpublicar: {
    width: '45%',
    height: '95%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#2ed936',
    justifyContent: 'center',
    alignItems: 'center',
    left: wp('3%')
  },
  buttonremover2: {
    width: '50%',
    height: '90%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#f54842',
    justifyContent: 'center',
    alignItems: 'center',
    //right: wp('2%')
  },
  buttonpublicar2: {
    width: '50%',
    height: '90%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#2ed936',
    justifyContent: 'center',
    alignItems: 'center',
    left: wp('3%')
  },
  viewacoeslivro: {
    width: '100%',
    height: hp('14%'),
    //borderWidth: 1,
    //flexDirection: 'row',
    marginTop: '5%',
  },
  modalView: {
    width: wp('80%'),
    height: hp('60%'),
    //margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    //borderWidth: 1,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  modalView2: {
    width: wp('70%'),
    height: hp('20%'),
    //margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    //borderWidth: 1,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  openButton2: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    fontSize: 16,
    marginBottom: 25,
    marginTop: 20,
    textAlign: "center",
    fontWeight: 'bold'
  }
});