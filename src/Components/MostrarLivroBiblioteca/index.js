import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, Alert, Modal, StyleSheet } from 'react-native';

//Importando Axios para comunicação com a API do ISBNdb
import axios from 'axios';

import AsyncStorage from '@react-native-async-storage/async-storage';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

//Importando a Estilização StyleSheet
import styles from './styles';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

//importando o firebase
import firebase from '../../FirebaseConnection';

export default function MostrarLivroBiblioteca() {

  //Criando State objeto para guardar o objeto mostrar livro que vem do async Storage
  const [objetoMostrarLivro, setMyObjetoMostrarLivro] = useState('');
  const [isVisible, setIsVisible] = useState(false);
  const [qtdLivros, setMyQtdLivros] = useState(0);
  const [objetousuario, setMyObjetoUsuario] = useState('');
  const [arrayListLivro, setArrayListLivro] = useState('');
  const [arrayListPublicacao, setArrayListPublicacao] = useState('');

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();


  //Função que que pegar objeto mostrar livro dentro do async storage e guarda dentro
  //da state objeto objetoMostrarLivro
  const getDataObject = async () => {
    try {
      const pegarobjetouser = await AsyncStorage.getItem('@objectuser')
      const pegarobjetomostrarlivro = await AsyncStorage.getItem('@objectmostrarlivro')

      setMyObjetoMostrarLivro(JSON.parse(pegarobjetomostrarlivro));
      setMyObjetoUsuario(JSON.parse(pegarobjetouser));

    } catch (e) {
      Alert.alert(e);
    }
  }

  const removerLivro = async (chave) => {

    let valueAdd = 0;
    valueAdd = qtdLivros - 1;

    await firebase.database().ref(`livro/${chave}`).remove();
    setIsVisible(!isVisible);

    await firebase.database().ref('usuario/' + objetousuario.chave + '/livros_cadastrados').set(valueAdd);
    console.log(`Valor qtdLivros: ${qtdLivros}`);

    navigation.navigate('ConfirmRemoveLivro', {
      screen: 'ConfirmRemoveLivro',
    });

  }

  const renderLivroNaoPublicado = (boleano, chavelivro, chavepublicacao) => {

    if (boleano) {
      return (
        <View>
          <View style={{ alignSelf: 'center', marginTop: '5%', alignItems: 'center' }}>
            <Icon name="checkmark-circle" color="#11bd2e" size={70} />
            <View style={{ justifyContent: 'center' }}>
              <Text style={{ color: "#11bd2e", fontWeight: 'bold', fontSize: 16 }}>Livro Já Publicado !</Text>
            </View>
          </View>
          <TouchableOpacity style={{
            alignSelf: 'center',
            marginTop: '30%',
            //borderWidth: 1,
            width: '85%',
            height: '25%',
            justifyContent: 'center',
            borderRadius: 5,
            backgroundColor: '#55bede'
          }} onPress={() => buscarPublicacao(chavepublicacao)}>
            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16, textAlign: 'center' }}>Ver publicação</Text>
          </TouchableOpacity>
        </View>

      );
    } else {
      return (

        <View>

          <View>
            <Text style={{ color: 'black', textAlign: 'center', fontSize: 16, fontWeight: 'bold' }}>Este livro ainda não foi publicado.</Text>
            <Text style={{ textAlign: 'center', color: 'black', marginTop: '10%' }}> O que deseja fazer?</Text>
          </View>

          <View style={styles.viewremover}>

            <TouchableOpacity style={styles.buttonremover} onPress={() => setIsVisible(!isVisible)}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Excluir Livro</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonpublicar} onPress={() => buscarLivro(objetoMostrarLivro.key)}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Publicar Livro</Text>
            </TouchableOpacity>

          </View>

        </View>



      )
    }
  }

  const gotoBiblioteca = () => {
    navigation.navigate('Biblioteca', {
      screen: 'Biblioteca',
    });
  }

  const gotoMostrarLivroPublicar = () => {
    navigation.navigate('MostrarLivroPublicar', {
      screen: 'MostrarLivroPublicar',
    })
  }

  const getQtdLivros = async () => {
    await firebase.database().ref(`usuario/${objetousuario.chave}/livros_cadastrados`).on('value', (snapshot) => {
      setMyQtdLivros(snapshot.val());
    });
  }



  const buscarPublicacao = async (chavepublicacao) => {


    await firebase.database().ref(`publicacao/${chavepublicacao}`).once('value', (snapshot) => {

      setArrayListPublicacao({
        key: snapshot.key,
        titulo: snapshot.val().titulo,
        descricao: snapshot.val().descricao,
        datapublicacao: snapshot.val().datapublicacao,
        status: snapshot.val().status,
        iduser: snapshot.val().usuario.iduser,
        nomeusuario: snapshot.val().usuario.nome,
        emailusuario: snapshot.val().usuario.email,
        telefoneusuario: snapshot.val().usuario.telefone,
        idlivro: snapshot.val().livro.idlivro,
        titulolivro: snapshot.val().livro.titulo,
        editoralivro: snapshot.val().livro.editora,
        imagem: snapshot.val().livro.imagem,

      });

    });


    console.log(chavepublicacao);


    navigation.navigate('EditarPublicacao', {
      screen: 'EditarPublicacao',
    });

  }

  const buscarLivro = async (chave) => {

    await firebase.database().ref(`livro/${chave}`).once('value', (snapshot) => {

      if (snapshot.val().publicado == true) {
        setArrayListLivro({
          key: snapshot.key,
          titulo: snapshot.val().titulo,
          autor: snapshot.val().autor,
          editora: snapshot.val().editora,
          imagem: snapshot.val().imagem,
          linguagem: snapshot.val().linguagem,
          datapublicado: snapshot.val().datapublicado,
          isbn: snapshot.val().isbn,
          publicado: snapshot.val().publicado,
          idpublicacao: snapshot.val().publicacao.idpublicacao,
        });

      } else {
        setArrayListLivro({
          key: snapshot.key,
          titulo: snapshot.val().titulo,
          autor: snapshot.val().autor,
          editora: snapshot.val().editora,
          imagem: snapshot.val().imagem,
          linguagem: snapshot.val().linguagem,
          datapublicado: snapshot.val().datapublicado,
          isbn: snapshot.val().isbn,
          publicado: snapshot.val().publicado,
        });
      }

    });

    navigation.navigate('MostrarLivroPublicar', {
      screen: 'MostrarLivroPublicar',
    });
  }


  const publicacaoStorage = async () => {

    //Try catch para guardar objeto com dados do livro
    //no async storage
    try {

      await AsyncStorage.setItem('@objectpublicacao', JSON.stringify(arrayListPublicacao));

    } catch (e) {

      alert(e);

    }

  }

  const LivroStorage = async () => {

    //Try catch para guardar objeto com dados do livro
    //no async storage
    try {

      await AsyncStorage.setItem('@objetolivropublicar', JSON.stringify(arrayListLivro));

    } catch (e) {

      alert(e);

    }

  }


  useEffect(() => {

    getDataObject();
    getQtdLivros();

    console.log(qtdLivros);

    if (objetoMostrarLivro.idpublicacao) {
      console.log(objetoMostrarLivro.idpublicacao)
    } else {
      console.log('não existe publicacao')
    }

  }, [objetousuario.chave, objetoMostrarLivro.key, qtdLivros]);

  useEffect(() => {
    LivroStorage();

  }, [arrayListLivro]);

  useEffect(() => {
    publicacaoStorage();
  }, [arrayListPublicacao]);

  return (
    <View style={styles.containerprincipal}>

      <View style={styles.topo}>
        <TouchableOpacity style={styles.viewphoto} onPress={() => gotoBiblioteca()}>
          <Icon name="ios-arrow-back-outline" color="black" size={40} />
        </TouchableOpacity>
      </View>

      <View style={styles.viewimg}>
        <View style={styles.img}>
          <Image style={{
            width: '100%',
            height: '100%',
            resizeMode: 'contain',
            //position: 'relative',
            alignSelf: 'center',
          }} source={{ uri: objetoMostrarLivro.imagem }} />
        </View>

        <View style={styles.infobuttonslivro}>
          {renderLivroNaoPublicado(objetoMostrarLivro.publicado, objetoMostrarLivro.key, objetoMostrarLivro.idpublicacao)}
        </View>
      </View>

      <View style={styles.viewinfolivro}>

        <View style={{ height: '15%' }}>
          <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Informações do Livro</Text>
        </View>

        <View style={{ height: '15%', width: '95%', borderBottomWidth: 1, borderColor: '#d1d1d1' }}>
          <Text style={{ fontWeight: 'bold', fontSize: 18 }} >Título:</Text>
          <Text numberOfLines={1}>{objetoMostrarLivro.titulo}</Text>
        </View>

        <View style={{ height: '15%', width: '95%', borderBottomWidth: 1, borderColor: '#d1d1d1', marginTop: '3%' }}>
          <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Autor:</Text>
          <Text numberOfLines={1}>{objetoMostrarLivro.autor}</Text>
        </View>

        <View style={{ height: '15%', width: '95%', borderBottomWidth: 1, borderColor: '#d1d1d1', marginTop: '3%' }}>
          <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Editora:</Text>
          <Text numberOfLines={1}>{objetoMostrarLivro.editora}</Text>
        </View>

        <View style={{ height: '15%', width: '95%', borderBottomWidth: 1, borderColor: '#d1d1d1', marginTop: '3%', flexDirection: 'row' }}>
          <View style={{ width: '60%' }}>
            <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Ano de Publicação:</Text>
            <Text numberOfLines={1}>{objetoMostrarLivro.datapublicado}</Text>
          </View>

          <View>
            <Text style={{ fontWeight: 'bold', fontSize: 18 }}>ISBN:</Text>
            <Text numberOfLines={1}>{objetoMostrarLivro.isbn}</Text>
          </View>
        </View>

      </View>

      <Modal
        animationType="slide"
        transparent={true}
        visible={isVisible}
        onRequestClose={() => {
          setIsVisible(!isVisible);
        }}
        style={{ width: '50%' }}
      >
        <View style={styles.centeredView2}>
          <View style={styles.modalView2}>


            <Text style={{ fontWeight: 'bold' }}>Deseja realmente excluir/remover este livro?</Text>

            <View style={{ flexDirection: 'row', marginTop: '10%' }}>
              <TouchableOpacity style={styles.buttonremover2} onPress={() => setIsVisible(!isVisible)}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>Não, Cancelar !</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.buttonpublicar2}>
                <Text style={{ color: 'white', fontWeight: 'bold' }} onPress={() => removerLivro(objetoMostrarLivro.key)}>Sim, Excluir !</Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>

    </View>
  );
}