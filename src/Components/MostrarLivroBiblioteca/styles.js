import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  containerprincipal: {
    flex: 1,
    backgroundColor: '#f2f2f2'
    //paddingTop: wp('4%')
  },
  viewimg: {
    width: wp('90%'),
    height: hp('35%'),
    //borderWidth: 1,
    justifyContent: 'center',
    padding: wp('4%'),
    flexDirection: 'row'
    , alignSelf: 'center',
    borderRadius: 10,
    backgroundColor: '#fff',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  centeredView2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView2: {
    width: wp('70%'),
    height: hp('20%'),
    //margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    //borderWidth: 1,
    padding: 25,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  buttonremover2: {
    width: '50%',
    height: '100%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#f54842',
    justifyContent: 'center',
    alignItems: 'center',
    //right: wp('2%')
  },
  buttonpublicar2: {
    width: '50%',
    height: '100%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#2ed936',
    justifyContent: 'center',
    alignItems: 'center',
    left: wp('3%')
  },
  topo: {
    width: wp('100%'),
    height: hp('10%'),
    //backgroundColor: '#fff',
    //padding: wp('5%'),
    justifyContent: 'center',
    paddingLeft: wp('8%'),
  },
  img: {
    width: '50%',
    height: '100%',
    //borderWidth: 1,
    //left: wp('-20%'),
  },
  buttonremover: {
    width: '80%',
    height: '31%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#f54842',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: hp('3%'),
    alignSelf: 'center'
  },
  buttonpublicar: {
    width: '80%',
    height: '31%',
    //borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#2ed936',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: hp('2%'),
    alignSelf: 'center'
    //left: wp('3%')
  },
  infobuttonslivro: {
    width: '50%',
    height: '100%',
    //borderWidth: 1,
    //left: wp('-20%'),
  },
  viewinfolivro: {
    paddingLeft: wp('5%'),
    marginTop: wp('8%'),
    width: '100%',
    height: hp('50%'),
    //borderWidth: 1,
  }

});

export default styles;