import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

//Importando Estilização externa da Screen
import styles from './styles';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

import AsyncStorage from '@react-native-async-storage/async-storage';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

export default function ErrorCadastroLivro() {

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  const goToLibrary = () => {
    navigation.navigate('Home', {
      screen: 'Biblioteca',
    });
  }

  return (
    <View style={styles.container}>

      {/* View vazia somente para alugar espaço do flex da screen */}
      <View style={styles.container1}></View>


      <View style={styles.container2}>

        {/*  */}
        <View style={styles.viewiconsucess}>
          <Icon name="md-sad" color="#d44c4c" size={120} />
        </View>

        <View style={styles.viewtxtsucess}>
          <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#454545', textAlign: 'center' }}>OPS, algo deu errado ao cadastrar o Livro.</Text>

        </View>
      </View>

      <View style={styles.container3}>

        <View style={styles.viewtextpublicar}>
          <Text style={{ fontSize: 16, color: '#b4b4b4', fontWeight: 'bold' }}>Tente novamente mais tarde.</Text>
        </View>

        <View style={styles.buttontextpublicar}>
          <TouchableOpacity style={styles.buttoncancel} onPress={() => goToLibrary()}>
            <Text style={{ color: 'white', fontWeight: 'bold' }}>Voltar</Text>
          </TouchableOpacity>
        </View>
      </View>

    </View>
  );
}