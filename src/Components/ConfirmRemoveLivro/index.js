import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Alert } from 'react-native';

//Importando Estilização externa da Screen
import styles from './styles';

//Importando navegação do React Navigation para navegação entre telas
import { useNavigation } from '@react-navigation/native';

//importando o firebase
import firebase from '../../FirebaseConnection';

import AsyncStorage from '@react-native-async-storage/async-storage';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

export default function ConfirmRemoveLivro() {

  //Atribuindo a navegação para a constante navigantion, assim
  //a mesma tem acesso à nevegação entre as telas da aplicação
  const navigation = useNavigation();

  const [qtdLivro, setMyQtdLivros] = useState(0);
  const [objetousuario, setMyObjetoUsuario] = useState('');

  const goToLibrary = async () => {
    {/*let valueadd = 0;

    if (qtdLivro) {
      valueadd = qtdLivro + 1;
    }

    await firebase.database().ref('usuario/' + objetousuario.chave + '/livros_cadastrados').set(valueadd);*/}

    navigation.navigate('Home', {
      screen: 'Biblioteca',
    });
  }

  const getDataObject = async () => {
    try {
      const pegarobjetouser = await AsyncStorage.getItem('@objectuser');
      setMyObjetoUsuario(JSON.parse(pegarobjetouser));
    } catch (e) {
      Alert.alert(e);
    }
  }

  useEffect(() => {
    {/*const getQtdLivros = async () => {
      await firebase.database().ref(`usuario/${objetousuario.chave}/livros_cadastrados`).once('value', (snapshot) => {
        setMyQtdLivros(snapshot.val());
      });
    }*/}

    getDataObject();
    //getQtdLivros();

  });


  return (
    <View style={styles.container}>

      {/* View vazia somente para alugar espaço do flex da screen */}
      <View style={styles.container1}></View>


      <View style={styles.container2}>

        {/*  */}
        <View style={styles.viewiconsucess}>
          <Icon name="checkmark-circle" color="#11bd2e" size={120} />
        </View>

        <View style={styles.viewtxtsucess}>
          <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#11bd2e' }}>Livro Excluído com sucesso!</Text>

        </View>
      </View>

      <View style={styles.container3}>

        <View style={styles.viewtextpublicar}>
          <Text style={{ fontSize: 16, color: '#b4b4b4', fontWeight: 'bold' }}>Vamos cadastrar mais um livro !?</Text>
        </View>

        <View style={styles.buttontextpublicar}>
          <TouchableOpacity style={styles.buttoncancel} onPress={() => goToLibrary()}>
            <Text style={{ color: 'white', fontWeight: 'bold' }}>Voltar</Text>
          </TouchableOpacity>
        </View>
      </View>

    </View>
  );
}
