import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container1: {
    flex: 1 / 1.5,
    //borderWidth: 1,
  },
  container2: {
    flex: 1,
    //borderWidth: 1,
  },
  container3: {
    flex: 1,
    //borderWidth: 1,
  },
  viewiconsucess: {
    //width: '100%',
    //borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewtxtsucess: {
    //borderWidth: 1,
    //width: '100%',
    alignItems: 'center'
  },
  viewtextpublicar: {
    //borderWidth: 1,
    height: '30%',
    alignSelf: 'center'
  },
  buttontextpublicar: {
    //borderWidth: 1,
    height: '70%',
    flexDirection: 'row',
    //alignItems: 'center',
    justifyContent: 'center'
  },
  buttonsucess: {
    //borderWidth: 1,
    width: wp('40%'),
    height: hp('7%'),
    marginLeft: wp('3%'),
    borderRadius: 10,
    backgroundColor: '#11bd2e',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttoncancel: {
    //borderWidth: 1,
    width: wp('40%'),
    //width: '35%',
    borderRadius: 10,
    height: hp('7%'),
    backgroundColor: '#d44c4c',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default styles;