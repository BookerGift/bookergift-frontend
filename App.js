import { View, Text } from 'react-native'
import React from 'react'

//Importando o React Navigation
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

//Importação do react-native-vector-icons
import Icon from 'react-native-vector-icons/Ionicons';

//Criando o Stack Navigator e o Tab Navigator
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

//importando as telas da pulha de navegação antes do login
import Informative1 from './src/pages/Informative1'
import Informative2 from './src/pages/Informative2'
import Informative3 from './src/pages/Informative3'
import Informative4 from './src/pages/Informative4'
import Login from './src/pages/Login'
import Cadastro from './src/pages/Cadastro'
import Cadastro2 from './src/pages/Cadastro2'
import TermsServices from './src/pages/TermsServices'
import ConfirmCadastro from './src/pages/ConfirmCadastro'
import ForgotPassword from './src/pages/ForgotPassword'
import PerfilInfoNome from './src/pages/PerfilInfoNome'
import PerfilInfoTelefone from './src/pages/PerfilInfoTelefone'
import CadastrarLivro1 from './src/pages/CadastrarLivro1'
import ConfirmLivro from './src/pages/ConfirmLivro'
import ModalInfoISBN from './src/Components/ModalInfoISBN'
import ConfirmCadastroLivro from './src/Components/ConfirmCadastroLivro'
import ErrorCadastroLivro from './src/Components/ErrorCadastroLivro'
import ListarLivrosUser from './src/Components/ListarLivrosUser'
import MostrarLivroBiblioteca from './src/Components/MostrarLivroBiblioteca'
import ConfirmRemoveLivro from './src/Components/ConfirmRemoveLivro'
import CadastroPublicacao from './src/pages/CadastroPublicacao'
import MostrarLivroPublicar from './src/Components/MostrarLivroPublicar'
import Testes from './src/Tests/Testes'
import EditarPublicacao from './src/pages/EditarPublicacao'
import CancelDoacao from './src/pages/CancelDoacao'
import ConfirmDoacao from './src/pages/ConfirmDoacao'

//importando as telas da pilha de navegação após usuário logado
import Home from './src/pages/Home';
import Biblioteca from './src/pages/Biblioteca';
import Perfil from './src/pages/Perfil';
import Configuracoes from './src/pages/Configuracoes';

const icons = {
  Home: {
    name: 'home-sharp'
  },
  Biblioteca: {
    name: 'library'
  },
  Perfil: {
    name: 'person'
  },
  Configuracoes: {
    name: 'settings-sharp'
  }
}

function Tabs() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, size }) => {
          const { name } = icons[route.name];
          return <Icon name={name} color={color} size={size} />
        }
      })}
    >
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Biblioteca" component={Biblioteca} />
      <Tab.Screen name="Perfil" component={Perfil} />
      <Tab.Screen name="Configuracoes" component={Configuracoes} />
    </Tab.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Informative1" screenOptions={{ headerShown: false }}>

        <Stack.Screen name="Informative1" component={Informative1} />
        <Stack.Screen name="Informative2" component={Informative2} />
        <Stack.Screen name="Informative3" component={Informative3} />
        <Stack.Screen name="Informative4" component={Informative4} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Cadastro" component={Cadastro} />
        <Stack.Screen name="Cadastro2" component={Cadastro2} />
        <Stack.Screen name="TermsServices" component={TermsServices} />
        <Stack.Screen name="ConfirmCadastro" component={ConfirmCadastro} />
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        <Stack.Screen name="PerfilInfoNome" component={PerfilInfoNome} />
        <Stack.Screen name="PerfilInfoTelefone" component={PerfilInfoTelefone} />
        <Stack.Screen name="CadastrarLivro1" component={CadastrarLivro1} />
        <Stack.Screen name="Home" component={Tabs} />
        <Stack.Screen name="ConfirmLivro" component={ConfirmLivro} />
        <Stack.Screen name="ModalInfoISBN" component={ModalInfoISBN} />
        <Stack.Screen name="ConfirmCadastroLivro" component={ConfirmCadastroLivro} />
        <Stack.Screen name="ErrorCadastroLivro" component={ErrorCadastroLivro} />
        <Stack.Screen name="ListarLivrosUser" component={ListarLivrosUser} />
        <Stack.Screen name="MostrarLivroBiblioteca" component={MostrarLivroBiblioteca} />
        <Stack.Screen name="ConfirmRemoveLivro" component={ConfirmRemoveLivro} />
        <Stack.Screen name="MostrarLivroPublicar" component={MostrarLivroPublicar} />
        <Stack.Screen name="CadastroPublicacao" component={CadastroPublicacao} />
        <Stack.Screen name="Testes" component={Testes} />
        <Stack.Screen name="EditarPublicacao" component={EditarPublicacao} />
        <Stack.Screen name="ConfirmDoacao" component={ConfirmDoacao} />
        <Stack.Screen name="CancelDoacao" component={CancelDoacao} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}


// Prop screenOptions={{headerShown: false}} = Esconde o header do stack navigator
// Prop initialRoutename = Define qual é tela primária. 